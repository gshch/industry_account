<?php


require_once(dirname(__FILE__).'/include/common.php');

if(empty($_GET['id']) || intval($_GET['id'])<1){
	LYG::ShowMsg('参数错误');
}
$id=intval($_GET['id']);


if(empty($_GET['class'])){
	LYG::ShowMsg('参数错误');
}
$class=trim($_GET['class']);

//判断分类下是否有
$ddcount = $con->rowscount("select count(*) from #__money where {$class}=$id");
if($ddcount>0){
	LYG::ShowMsg('有数据，暂不能删除');
}

//判断分类下是否有
$ddcount = $con->rowscount("select count(*) from #__money_pay where {$class}=$id");
if($ddcount>0){
	LYG::ShowMsg('有数据，暂不能删除');
}

$sql="delete from #__{$class} where id=$id limit 1";
$data =$con->Excute($sql);
if($data){
	lyg::jump('class_list.php?class='.$class.'');
}else{
	LYG::ShowMsg('删除失败');
}	
