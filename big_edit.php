<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 

if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$data_id = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money_bigclass where id=$data_id");
if(empty($info)){lyg::showmsg('参数错误');}





if(!empty($_POST)){
	//参数校验
	extract($_POST);

	if(empty($bigclass) || trim($bigclass)==''){
		LYG::ShowMsg('一级分类名不能为空');
	}
	$bigclass= trim($bigclass);
	
	$ex = $con->rowscount("select count(*) from #__money_bigclass where  bigclass=? and id<>?",array(
		$bigclass,$data_id
	));
	if($ex>0){
		lyg::showmsg("同名一级分类已存在");
	}
	
	$data = array(
		$bigclass,
		$data_id,
	);
	
	$eok = $con->Update("update #__money_bigclass set bigclass=? where id=? limit 1",$data);

	if($eok!==false){
		LYG::ShowMsg('操作成功','big_list.php?type=');
	}else{
		LYG::ShowMsg('操作失败，请重试');
	}
	
	die();
}


	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑一级分类</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<input type='hidden' name='id' value='<?php echo $data_id;?>'>
	<table cellpadding="3" cellspacing="0" class="table-add">

		<tr>
			<td align="right" width='100' height='36'>一级分类名称：</td>
			<td align="left" width='*'>
				<input type='text' class='inp' name='bigclass' value='<?php echo $info['bigclass'];?>'/>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>备注：</td>
			<td>
				<font color="#ff0000"><?php if($info['isok']==1){ echo $c_isok["{$info['type']}"];}?></font>
			</td>
		</tr>
		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='edit' type='submit' value='修改'/></td>
		</tr>

	</table>
</form>

</body>
</html>