<?php
require_once(dirname(__FILE__)."/common.php");
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}
	
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>详情</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body style="background:#F4F5F6;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>详情</h1>
</header>

<div class="hui-wrap">
    <div style="padding:10px 10px 10px 10px;float:right;">
<a href="money_edit.php?id=<?php echo $info['id'];?>">
<?php if (c_bigclassisok($info['id_bigclass'])==1){echo "";}else{echo "<button type='button' class='hui-button hui-button-small hui-primary hui-fl'>编辑</button>";}?>
</a>
<a onclick="return confirm('删除后不可恢复，确定删除吗？');"  href="money_del.php?<?php echo 'id='.$info['id'];?>">
<?php if ($check_enable && $info['isok']==0){echo "";}else{echo "<button type='button' class='hui-button hui-button-small hui-danger hui-fl' style='margin:0px 10px;'>删除</button>";}?>
</a>
    </div>

    <div class="hui-center-title" style="margin-top:5px;"><h1 style="border:0px;"><?php echo $info['moneyID'];?></h1></div>

    <div style="margin:10px 10px; margin-bottom:5px;" class="hui-form" id="form1">
        <div class="hui-form-items">
        	<div class="hui-form-items-title">类型：</div>
            [<?php echo $c_type["{$info['type']}"];?>]<?php echo c_bigclassname($info['id_bigclass']);?>-><?php echo c_smallclassname($info['id_smallclass']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">金额：</div>
            <?php echo $info['price'];?> 元
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">时间：</div>
            <?php echo substr($info['selldate'],0,10) ?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">资金账户：</div>
            <?php echo c_zhangname($info['zhanghu']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_wanglai'];?>：</div>
            <?php echo c_classname("wanglai",$info['wanglai']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?>：</div>
            <?php echo c_classname("yuangong",$info['yuangong']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_xiangmu'];?>：</div>
            <?php echo c_classname("xiangmu",$info['xiangmu']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">备注：</div>
            <?php echo $info['beizhu'];?>
        </div>
        
        <div class="hui-form-items">
        	<div class="hui-form-items-title">记账人：</div>
            <?php echo c_adminname($info['id_login']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">添加时间：</div>
            <?php echo $info['addtime'];?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">修改人：</div>
            <?php echo $info['LastLogin'];?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">修改时间：</div>
            <?php echo $info['LastTime'];?>
        </div>
<?php if($check_enable){ ?>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">审核状态：</div>
            <?php if ($info['isok']==0){echo '已审';}?><?php if ($info['isok']==1){echo '未审';}?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">审核人：</div>
            <?php echo $info['shenhe'];?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">审核时间：</div>
            <?php echo $info['shenhetime'];?>
        </div>
<?php } ?>
    </div>
<?php if($info['img']){ ?>
    <div style="padding:10px; padding-top:10px;">
                   <?php 
//附件显示开始
if ($info['img']!=""){
$count = count(explode(',', $info['img']));
$kk=0;
foreach( explode(',', $info['img']) as $k1=>$v1)
{$kk=$kk+1;
if($count<>$kk){echo '<a href="'.$v1.'"><img src="'.$v1.'" width=33%></a>';}
}		
			}
//附件显示结束			
			?>
    </div>
<?php } ?>
</div>


<script src="js/hui.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>