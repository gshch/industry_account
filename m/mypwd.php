<?php
require_once(dirname(__FILE__)."/common.php");

$debug = lyg::readArr("debug");
if($debug['debug']){
	header('Content-Type:text/html;charset=utf-8');
	echo '演示系统，不能修改密码'; exit;
}

if(!empty($_POST)){		

	$id	= $_SESSION['eptime_id'];
	
	$data = array(
		'UserName' => empty($_POST['username']) ? '' : trim($_POST['username']),
	);
	if(!preg_match('/^[a-zA-Z]{1,}[a-zA-Z0-9_]{1,}$/',$data['UserName'])){
		lyg::showmsg('用户名格式错误');
	}
	if(!empty($_POST['pwd1']) || !empty($_POST['pwd2'])){
		
		if($_POST['pwd1'] != $_POST['pwd2']){
			lyg::showmsg('密码输入不一致');
		}
		$pwd = trim($_POST['pwd1']);
		if(strlen($pwd) < 6){
			lyg::showmsg('密码长度应不少于6位');
		}
		$data['Password'] = c_adminpwd($pwd);
	}
	
	$result	= $con->savemin('admin',$data,'ID='.$id);
	if($result!==false){
		$_SESSION['eptime_username'] = $data['UserName'];
		LYG::ShowMsg('修改成功','exit.php');
	}else{
		LYG::ShowMsg('修改失败');
	}
		
	die();
}

$id	= $_SESSION['eptime_id'];

//查询数据
$sql = 'select * from #__admin where ID='.$id;
$data = $con->find($sql);
if(!$data || count($data)<1){
	LYG::ShowMsg('数据读取失败');
}
	
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>修改密码</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body style="background:#F4F5F6;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>修改密码</h1>
</header>
<div class="hui-wrap">
<form action='' method='post' onsubmit='return ckf();'>
    <div style="margin:20px 10px; margin-bottom:15px;" class="hui-form" id="form1">
        <div class="hui-form-items">
        	<div class="hui-form-items-title">用户名</div>
            <input type="text" name='username' value='<?php echo $data['UserName'];?>' readonly class="hui-input hui-input-clear" />
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">登录密码</div>
            <input type="password" name='pwd1' class="hui-input hui-pwd-eye" placeholder="登录密码" checkType="string" id="pwd" checkData="6,20" checkMsg="密码应为6-20个字符" />
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">确认密码</div>
            <input type="password"  name='pwd2' class="hui-input hui-pwd-eye" placeholder="确认密码" checkType="sameWithId" checkData="pwd" checkMsg="两次密码不一致"  />
        </div>
    </div>
    <div style="padding:10px; padding-top:10px;">
        <button type="submit" class="hui-button hui-button-large hui-primary" id="submit">修改</button>
    </div>
	</form>
</div>
<?php include 'footer.php';?>
<script type="text/javascript" src="js/hui-form.js" charset="utf-8"></script>
<script type="text/javascript">
hui('#submit').click(function(){
    //验证
    var res = huiFormCheck('#form1');
    //提交
    if(res){hui.iconToast('验证通过！');}
});
</script>
</body>
</html>