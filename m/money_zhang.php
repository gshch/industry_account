<?php
require_once(dirname(__FILE__)."/common.php");
if (strpos($_SESSION['eptime_flag'], 'zhangfig') === false) {LYG::ShowMsg('您没有权限！');} 
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title></title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body style="background:#FCFCFC;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>资金管理</h1>
</header>
<div class="hui-wrap">


<?php
$sql = "select #__zhanghu.*,#__zhanghu_class.zhanghuclass from #__zhanghu left join #__zhanghu_class on #__zhanghu_class.id = #__zhanghu.type order by  #__zhanghu.id desc";
$data = $con->select($sql,$_v);
?>
<div class="hui-wrap" style="padding-top:5px;">
    <div class="hui-center-title" style="margin-top:5px;"><h1>资金账户(点击转账)</h1></div>
</div>
	<style type="text/css">
		.bgGreen{background:#009900 !important;}
	.bgRed{background:#EE4B47 !important;}
	.bgBlue{background:#1BC5BB !important;}
	.demo{width:100%; height:40px; text-align:left; text-indent: 10px;color:#FFF; line-height:40px; font-size:14px; margin:5px; background:#3388FF;}
	.demo2{height:50px; text-align:left; text-indent: 10px;color:#FFF; line-height:50px; font-size:15px; margin:5px; background:#3388FF;}
	</style>
	<div>

	<?php $amount=0; foreach($data as $k=>$v){?>
<div class="demo2 bgBlue">[<?php echo $v['zhanghuclass'];?>]<?php echo $v['name'];?>
<a href="zhuan.php?id=<?php echo $v['id'];?>" onClick="return confirm('确定从该账户转账吗?');">
<div class="hui-list-info2"><?php echo round($v['amount'],2);?>元
<span class="hui-icons hui-icons-right"></span>
</div>	
</a>
</div>
	<?php $amount=$amount+$v['amount'];}?>


		
		<div class="demo2 bgRed">合计：<?php echo round($amount,2);?>元</div>
	</div>


</div>
<?php include 'footer.php';?>
</body>
</html>