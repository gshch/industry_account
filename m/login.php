<?php

session_start();
require_once(dirname(dirname(__FILE__))."/include/init.php");
require_once(dirname(dirname(__FILE__))."/include/mysql.php");
require_once(dirname(dirname(__FILE__))."/include/function.php");

$webconfig = lyg::readArr("web");
if(!empty($_POST)){
	
	$post = $_POST;
	if(empty($post['username']) || empty($post['userpwd'])){
		LYG::ShowMsg('请输入账号密码');
	}
	if($webconfig['login_code_open']){
		if(empty($_SESSION['eptime_login_code'])){
			LYG::ShowMsg('非法操作');
		}
		if(empty($post['code'])){
			LYG::ShowMsg('请输入验证码');
		}
		if(strtolower($post['code']) != $_SESSION['eptime_login_code']){
			LYG::ShowMsg('验证码错误');
		}
		$_SESSION['eptime_login_code'] = null;
		unset($_SESSION['eptime_login_code']);
	}
	
	$username	=$post['username'];
	$userpwd	=c_adminpwd($post['userpwd']);
	$data	=$con->find('select * from #__admin where Working=1 and username=? and password=?',array($username,$userpwd));

	if(!empty($data)){

   if($webconfig['login_wx_open']){//微信开始
if($data['openid']==''){
    	$_SESSION['eptime_id']=$data['ID'];
		$_SESSION['eptime_username']=$data['UserName'];
		$_SESSION['eptime_adminPower']=$data['AdminPower'];
		$_SESSION['eptime_l_zhanghu']=$data['zhanghu'];
		$_SESSION['eptime_l_xiangmu']=$data['xiangmu'];
		$_SESSION['eptime_l_wanglai']=$data['wanglai'];
		$_SESSION['eptime_l_yuangong']=$data['yuangong'];
		$_SESSION['eptime_flag']=$data['flag'];
		$_SESSION['eptime_img']=$_SESSION['headimgurl'];
		$aa_id=$data['ID'];
    $data1 = array(
        'openid'		=>$_SESSION['openid'],
        'img'		=>$_SESSION['headimgurl'],
    );
	$result	= $con->savemin("admin",$data1,"ID={$aa_id}");		
	    LYG::writeLog("[".$_SESSION['eptime_username']."]绑定[".$_SESSION['openid']."]成功！");
		lyg::jump("index.php");    
}else{		
LYG::ShowMsg('该账号已经绑定微信，不能重复绑定！');    
}
   }else{//正常登陆
   
 		$_SESSION['eptime_id']=$data['ID'];
		$_SESSION['eptime_username']=$data['UserName'];
		$_SESSION['eptime_adminPower']=$data['AdminPower'];
		$_SESSION['eptime_l_zhanghu']=$data['zhanghu'];
		$_SESSION['eptime_l_xiangmu']=$data['xiangmu'];
		$_SESSION['eptime_l_wanglai']=$data['wanglai'];
		$_SESSION['eptime_l_yuangong']=$data['yuangong'];
		$_SESSION['eptime_flag']=$data['flag'];
		$_SESSION['eptime_img']=$data['img'];
		LYG::writeLog("[".$_SESSION['eptime_username']."]登陆后台成功！");
		lyg::jump("index.php");  
   }   
    
        
	}else{
		LYG::ShowMsg('账号密码错误');
	}
	exit();
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php echo $webconfig['system_title'];?>|<?php if($webconfig['login_wx_open']){echo '绑定微信';}else{echo '用户登陆';}?></title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<style>
.hui-start-banner{width:100%; height:100%; position:fixed; z-index:9998; left:0px; top:0px; background:#FFFFFF; display:none;}
#hui-start-banner-close{width:50px; height:50px; position:fixed; z-index:9999; top:50px; right:20px; background:#F5F6F7; display:none; text-align:center; border-radius:50px; line-height:50px; font-size:14px; color:#666;}
.hui-start-banner .hui-swipe-indicator{width:96%; position:absolute; z-index:3; left:2%; bottom:20px; display:none;}
</style>
</head>
<body style="background:#F4F5F6;">
<header class="hui-header">
 
    <h1><?php if($webconfig['login_wx_open']){echo '绑定微信';}else{echo '用户登陆';}?></h1>
</header>
<div class="hui-wrap">
    <div class="hui-center-title" style="margin-top:35px;"><h1 style="border:0px;"><?php echo $webconfig['system_title'];?></h1></div>
    <div style="margin:20px 10px; margin-bottom:15px;" class="hui-form" id="form1">
	   <form method="post">
        <div class="hui-form-items">
        	<div class="hui-form-items-title">登录用户</div>
            <input type="text" name="username" class="hui-input hui-input-clear"  placeholder="请输入登录用户名称" checkType="string" checkData="5,20" checkMsg="用户名应为5-20个字符" />
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">登录密码</div>
            <input type="password"  name="userpwd" class="hui-input hui-pwd-eye"  placeholder="请输入登录用户密码" checkType="string" id="pwd" checkData="5,20" checkMsg="密码应为5-20个字符" />
        </div>
				<?php
				if($webconfig['login_code_open']){
				?>
        <div class="hui-form-items">
            <div class="hui-form-items-title">验证码</div>
            <input type="text" class="hui-input" name="code" value="" checkType="string" checkData="3,4" checkMsg="验证码应该为3-4个字符" />
            <div style="width:120px;">
            	<img src="../code/" id="code-img" title="点击更换验证码"  width="100%">
            </div>
        </div>
		         <?php } ?>
    </div>
    <div style="padding:10px; padding-top:10px;">
        <button type="submit" class="hui-button hui-button-large hui-primary" id="submit"><?php if($webconfig['login_wx_open']){echo '确定绑定微信';}else{echo '立即登录';}?></button>
    </div>
	</form>	
</div>

<?php
if($_COOKIE['visit_flag']==""){
?>
 
<?php
}
setcookie("visit_flag",1);
?>


<script type="text/javascript" src="js/hui.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/hui-form.js" charset="utf-8"></script>
<script type="text/javascript">
hui('#submit').click(function(){
    //验证
    var res = huiFormCheck('#form1');
    //提交
    if(res){hui.iconToast('验证通过！');}
});
</script>
<script type="text/javascript" src="js/hui-swipe.js" charset="UTF-8"></script>
<script type="text/javascript">
var wininfo = hui.winInfo();
hui('#startBanner').find('.hui-swipe-item').css({height:wininfo.height+'px'});
hui('#startBanner').show();
hui('#hui-start-banner-close').show();
var swipe      = new huiSwpie('#startBanner');
swipe.autoPlay = false;
swipe.loop     = false;
swipe.run();
function huiStartBannerClose(){
    hui('#startBanner').remove();
    hui('#hui-start-banner-close').remove();
}
</script>
</body>
</html>