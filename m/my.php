<?php
require_once(dirname(__FILE__)."/common.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>用户中心</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body>
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>用户中心</h1>
</header>
<div class="hui-wrap">
    <div class="hui-list" style="background:#FFFFFF; margin-top:28px;">
        <a href="" style="height:auto; height:80px; padding-bottom:8px;">
    		<div class="hui-list-icons" style="width:110px; height:80px;">
    			<img src="<?php echo $_SESSION['eptime_img'];?>" style="width:66px; margin:0px; border-radius:50%;" />
    		</div>
    		<div class="hui-list-text" style="height:79px; line-height:79px;">
    			<div class="hui-list-text-content">
    			<?php echo $_SESSION['nickname'];?>	（<?php echo $_SESSION['eptime_username'];?>）
    			</div>
    			<div class="hui-list-info">
    				<span class="hui-icons hui-icons-right"></span>
    			</div>
    		</div>
    	</a>
    	<a href="">
    		<div class="hui-list-text">
    			姓名:<?php echo c_adminname($_SESSION['eptime_id']);?> | 级别:<?php echo $c_adminpower["{$_SESSION['eptime_adminPower']}"];?>
    		</div>
    	</a>
    </div>
    <div class="hui-list" style="background:#FFFFFF; margin-top:28px;">
        <ul>
            <li>
            	<a href="mypwd.php">
            		<div class="hui-list-icons">
		    			<img src="images/list/order.png" />
		    		</div>
		    		<div class="hui-list-text">
		    			修改密码
		    			<div class="hui-list-info">
		    				<span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
            	</a>
           	</li>
       
         
       
        </ul>
    </div>
    <div style="background:#FFFFFF; margin-top:28px;">
	<a href="exit.php">
        <button type="button" class="hui-button hui-button-large">
        	<span class="hui-icons hui-icons-logoff"></span><?php if($webconfig['login_wx_open']){echo '解绑微信';}else{echo '退出系统';}?>
       	</button>
	</a>
    </div>
</div>
<?php include 'footer.php';?>

</body>
</html>