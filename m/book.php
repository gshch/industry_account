<?php
require_once(dirname(__FILE__)."/common.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>意见反馈</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body style="background:#FCFCFC;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>意见反馈</h1>
</header>


<div class="hui-wrap">
    <form action="http://www.eptime.cn/e/enews/index.php" method="post" name="form1" id="form1" style="padding:28px 10px;" class="hui-form">
        <div class="hui-form-items">
        	<div class="hui-form-items-title">联系邮箱</div>
            <input type="email" class="hui-input" placeholder="如：eptime@eptime.cn" name="email" id="email" checkType="email" checkMsg="邮箱格式错误" />
        </div>
        <div class="hui-form-items">
            <div class="hui-form-items-title">留言内容</div>
            <div class="hui-form-textarea">
                <textarea placeholder="请填写留言内容..." name="lytext" id="lytext"></textarea>
            </div>
        </div>
        <div style="padding:15px 8px;">
		<input name="name" type="hidden" id="name" value="<?php echo c_adminname($_SESSION['eptime_id']);?>" />
		<input name="enews" type="hidden" id="enews" value="AddGbook" />
            <input type="submit" name="Submit3" value="提交数据"  class="hui-button hui-primary hui-fr" id="submitBtn">
        </div>
    </form>
</div>
<?php include 'footer.php';?>
<script type="text/javascript" src="js/hui-form.js"></script>
<script type="text/javascript">
hui.formInit();
//表单验证
var loves;
hui('#submitBtn').click(function(){
    var res = huiFormCheck('#form1');
    //提交
    if(res){
    	hui.iconToast('验证通过！');
    	var data = hui.getFormData('#form1');
    	data.loves = loves;
    	console.log(JSON.stringify(data));
    }
});
//附加验证函数用于单选多选等特殊检查项目
</script>
</body>
</html>