<?php
require_once(dirname(__FILE__)."/common.php");
$webconfig = lyg::readArr("web");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>图形报表</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body>
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>图形报表</h1>
</header>
<div class="hui-wrap">

    <div class="hui-list" style="margin-top:22px;">
    	<ul>


    		<li>
    			<a href="chart.php?list=Month">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			月收支曲线图
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>

    		<li>
    			<a href="chart.php?list=year">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			年度收支柱状图
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>

    		<li>
    			<a href="chart.php?list=wanglai">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			<?php echo $webconfig['system_wanglai'];?>收支柱状图
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>
    		<li>
    			<a href="chart.php?list=yuangong">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			<?php echo $webconfig['system_yuangong'];?>收支柱状图
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>
    		<li>
    			<a href="chart.php?list=xiangmu">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			<?php echo $webconfig['system_xiangmu'];?>收支柱状图
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>

    		<li>
    			<a href="chart.php?list=shouzhi">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			收入支出构成图表
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>
    		<li>
    			<a href="chart.php?list=total">
		    		<div class="hui-list-text">
		    			<div class="hui-list-text-content">
		    			收支汇总饼状图
		    			</div>
		    			<div class="hui-list-info">
		    				查看 <span class="hui-icons hui-icons-right"></span>
		    			</div>
		    		</div>
		    	</a>
    		</li>
    	</ul>
    </div>


</div>
<?php include 'footer.php';?>
</body>
</html>