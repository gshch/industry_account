<?php
require_once(dirname(__FILE__)."/common.php");
if (strpos($_SESSION['eptime_flag'], 'paymfig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money_pay where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}

if(!empty($_POST)){
	
	extract($_POST);

$moneyID=intval($moneyID);
$price=trim($price);
$selldate=trim($selldate);
$Lastdate=trim($Lastdate);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$beizhu= trim($beizhu);
	$img = trim($img);
$LastLogin = $_SESSION['eptime_username'];
$LastTime = date("Y-m-d H:i:s",time());

	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}

    $data = array(
        'price'		=>$price,
        'selldate'	=>$selldate,
		'Lastdate'	=>$Lastdate,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'img'		=>$img,
        'LastLogin'	=>$LastLogin,
		'LastTime'	=>$LastTime,
    );
	$result	= $con->savemin("money_pay",$data,"id={$dataid}");
	if($result!==false){
LYG::writeLog("[".$_SESSION['eptime_username']."]修改单号[".$moneyID."]");
		LYG::ShowMsg('修改成功','money_pay.php');
	}else{
		LYG::ShowMsg('流水没有更改');
	}
	
	die();
	
}


	
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>编辑</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../s/common.js"></script>

		<link rel="stylesheet" href="../kindeditor/themes/default/default.css" />
		<script src="../kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="../kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>
<body style="background:#FCFCFC;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>编辑</h1>
</header>
<div class="hui-wrap">

<form action='' method='post'>
<input type='hidden' name='id' value="<?php echo $dataid;?>" />
<input type='hidden' name='moneyID' value="<?php echo $info['moneyID'];?>" />
        <div class="hui-form-items">
            <div class="hui-form-items-title">单号：</div>
            <div class="hui-form-select">
<?php echo $info['moneyID'];?>
            </div>
        </div>
        <div class="hui-form-items">
            <div class="hui-form-items-title">类型：</div>
            <div class="hui-form-select">
[<?php echo $c_type1["{$info['type']}"];?>]<?php echo c_bigclassname($info['id_bigclass']);?>-><?php echo c_smallclassname($info['id_smallclass']);?>	
            </div>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">金额：</div>
            <input class='inp' type='number' name='price' placeholder="0.00" step="0.01" value="<?php echo $info['price'];?>"/>
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">时间：</div>
            <input type='text' class='inp' name='selldate' value='<?php echo substr($info['selldate'],0,10) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">期限：</div>
            <input type='text' class='inp' name='Lastdate' value='<?php echo substr($info['Lastdate'],0,10) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
        </div>

        <div class="hui-form-items">
            <div class="hui-form-items-title"><?php echo $webconfig['system_wanglai'];?>：</div>
            <div class="hui-form-select">
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
        		if(intval($info['wanglai'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
            </div>
        </div>

        <div class="hui-form-items">
            <div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?>：</div>
            <div class="hui-form-select">
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
        		if(intval($info['yuangong'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
            </div>
        </div>


        <div class="hui-form-items">
            <div class="hui-form-items-title"><?php echo $webconfig['system_xiangmu'];?>：</div>
            <div class="hui-form-select">
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
        		if(intval($info['xiangmu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
            </div>
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">备注</div>
            <input type="text" class="hui-input hui-input-clear" name="beizhu" value="<?php echo $info['beizhu'];?>" />
        </div>

        <div class="hui-form-items">
            <div class="hui-form-items-title">附件：</div>
            <input type='text' class='inp' name='img' placeholder='' id="url3" value="<?php echo $info['img'];?>" />&nbsp;&nbsp;<input type="button" value="上传" id="open" class="layui-btn">
        </div>
     <div class="hui-form-items">
            <div class="hui-form-items-title"></div>     
            <div class="alert alert-info" style="width:92%" id="demo2"></div>
            <div class="item col-xs-12"><div class="f-fl item-ifo">
<link rel="stylesheet" href="../layui/css/layui.css" media="all">
<script type="text/javascript" src="../layui/layui.js"></script>
<script type="text/javascript">
layui.use('upload', function (){
  var upload = layui.upload,
    $ = layui.jquery;
var uploadInst = upload.render({
  elem : '#open',
  accept : 'images',//指定允许上传时校验的文件类型，可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
  multiple : 'true',
  url : '../up.php',
  data: {},
  before: function(obj){
      obj.preview(function(index, file, result){
        $('#demo2').append('<img src="'+ result +'" alt="'+ file.name +'" style="width:33%;">')
      });
    },
  done : function(res){
if(res['code']==1){
//alert(res['src']);
var imgpath=res['src'];
//alert(imgpath);
layer.msg('图片上传成功');
imgpath = imgpath + ','+$("#url3").val();
$("#url3").val(imgpath);
}else{
layer.msg('图片上传失败');
}
  },
  error : function(){   
    //请求异常
  }
});
});

</script>
            </div></div>
        </div>
        <div style="padding:15px 8px;">
            <input type='submit' class="hui-button hui-button-large hui-primary" id="submitBtn" value='修改'/>
        </div>
</form>

</div>
<?php include 'footer.php';?>
</body>
</html>