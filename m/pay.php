<?php
require_once(dirname(__FILE__)."/common.php");
if (strpos($_SESSION['eptime_flag'], 'pay2fig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money_pay where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}

if(!empty($_POST)){
	
	extract($_POST);

$moneyID=trim($moneyID);
$price=trim($price);
$selldate=trim($selldate);
$price0=trim($price0);
    $zhanghu = intval($zhanghu);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$beizhu= trim($beizhu);
	$img = trim($img);
$LastLogin = $_SESSION['eptime_username'];
$LastTime = date("Y-m-d H:i:s",time());

	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}
	if($price0-$price<0){
		LYG::ShowMsg('收/还款金额不能大于原金额');
	}

$beizhu1= $beizhu." 收/还款 ￥".$price."元 时间[".$selldate."]；";
$price1=$price0-$price;
    $data = array(
        'price'		=>$price1,
		'beizhu'	=>$beizhu1,
    );
	$result	= $con->savemin("money_pay",$data,"id={$dataid}");
	if($result!==false){
LYG::writeLog("[".$_SESSION['eptime_username']."]收/还款单号[".$moneyID."]");

$beizhu="[收/还款".$moneyID."] ￥".$price."元";
$moneyID1 = c_newOrderNo("D");
$id_login = $_SESSION['eptime_id'];
$login = $_SESSION['eptime_username'];
$addtime = date("Y-m-d H:i:s",time());
if($check_enable){
	$isok = 1;
		}
		else{$isok = 0;}

	$data = array(
		'type'		=>$type,
  'id_bigclass'		=>$id_bigclass,
 'id_smallclass'	=>$id_smallclass,
        'price'		=>$price,
        'selldate'	=>$selldate,
		'zhanghu'	=>$zhanghu,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'moneyID'	=>$moneyID1,
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);

$aok = $con->add("money",$data);
if ($type==0){$eok = $con->Update("update #__zhanghu set amount=amount+{$price} where id={$zhanghu}");}
elseif($type==1){$eok = $con->Update("update #__zhanghu set amount=amount-{$price} where id={$zhanghu}");}
LYG::writeLog("[".$_SESSION['eptime_username']."]收还款成功，同时产生[".$moneyID."]");
		LYG::ShowMsg('收还款成功','money_pay.php');
	}else{
		LYG::ShowMsg('流水没有更改');
	}
	
	die();
	
}

	
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php if($info['type']==0){ echo "收款";}elseif($info['type']==1){ echo "还款";}?></title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body style="background:#F4F5F6;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1><?php if($info['type']==0){ echo "收款";}elseif($info['type']==1){ echo "还款";}?></h1>
</header>
<div class="hui-wrap">
  <div style="margin:20px 10px; margin-bottom:15px;" class="hui-form" id="form1">
<form action='' method='post'>
<input type='hidden' name='id' value="<?php echo $dataid;?>" />
		<input type='hidden' name='type' value="<?php echo $info['type'];?>" />
		<input type='hidden' name='moneyID' value="<?php echo $info['moneyID'];?>" />
		<input type="hidden" name="id_bigclass" value="<?php echo $info['id_bigclass'];?>">
		 <input type="hidden" name="id_smallclass" value="<?php echo $info['id_smallclass'];?>">
		 <input type="hidden" name="price0" value="<?php echo $info['price'];?>">
        <div class="hui-form-items">
        	<div class="hui-form-items-title">类型：</div>
            <?php echo c_bigclassname($info['id_bigclass']);?>-><?php echo c_smallclassname($info['id_smallclass']);?>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">金额：</div>
<input class="hui-input hui-input-clear" type='number' name='price' placeholder="0.00" step="0.01" value="<?php echo $info['price'];?>"/>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">时间：</div>
            <input type="text" class="hui-input hui-input-clear" name='selldate' value='<?php echo date("Y-m-d",time()) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">资金账户：</div>
				<select name="zhanghu" class="select">
				<?php
				foreach(c_classinfo("zhanghu") as $k=>$v){
        		if(intval($info['zhanghu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_wanglai'];?>：</div>
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
        		if(intval($info['wanglai'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?>：</div>
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
        		if(intval($info['yuangong'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_xiangmu'];?>：</div>
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
        		if(intval($info['xiangmu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">备注：</div>
            <input type="text" class="hui-input hui-input-clear" name='beizhu' placeholder='' value="<?php echo $info['beizhu'];?>"/>
        </div>
   </div>

    <div style="padding:10px; padding-top:10px;">
        <input type="submit" class="hui-button hui-button-large hui-primary" id="submit" value='提交'>
    </div>
</form>
   
</div>
<?php include 'footer.php';?>
</body>
</html>