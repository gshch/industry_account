<?php
require_once(dirname(__FILE__)."/common.php");
if (strpos($_SESSION['eptime_flag'], 'payfig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;
if(!empty($_POST)){
	//参数校验
	extract($_POST);

$type=intval($type);
$id_bigclass=intval($id_bigclass);
$id_smallclass=intval($id_smallclass);
$price=trim($price);
$selldate=trim($selldate);
$Lastdate=trim($Lastdate);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$beizhu= trim($beizhu);
	$img = trim($img);

$moneyID = c_newOrderNo("Y");
$id_login = $_SESSION['eptime_id'];
$login = $_SESSION['eptime_username'];
if($check_enable){
	$isok = 1;
		}
		else{$isok = 0;}
$addtime = date("Y-m-d H:i:s",time());


	if(trim($type)==''){
		LYG::ShowMsg('类型不能为空');
	}
	if(empty($id_bigclass) || trim($id_bigclass)==''){
		LYG::ShowMsg('一级分类不能为空');
	}
	if(empty($id_smallclass) || trim($id_smallclass)==''){
		LYG::ShowMsg('二级分类不能为空');
	}
	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}
	$ex = $con->rowscount("select count(*) from #__money_pay where moneyID=?",array($moneyID));
	if($ex>0){
		lyg::showmsg("单据号出错，请重新提交！");
	}
	
	$data = array(
		'type'		=>$type,
  'id_bigclass'		=>$id_bigclass,
 'id_smallclass'	=>$id_smallclass,
        'price'		=>$price,
		'price1'	=>$price,
        'selldate'	=>$selldate,
		'Lastdate'	=>$Lastdate,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'img'		=>$img,
		'moneyID'	=>$moneyID,
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);
	
	$aok = $con->add("money_pay",$data);

	if($aok){
LYG::writeLog("[".$_SESSION['eptime_username']."]添加单号[".$moneyID."]");
		LYG::ShowMsg('添加成功','money_pay.php');
	}else{
		LYG::ShowMsg('添加失败，请重试');
	}
	
	die();
}




?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>记一笔</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="j../s/common.js"></script>
<script type="text/javascript">
$(function(){
    $("#type").change(function(){
        var id = parseInt($(this).val());

            $.ajax({
                type:'get',
                url:'../json.php?act=getbigclass&type='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>请选择</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].bigclass+"</option>";
                    }
                    $("#id_bigclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });

    $("#id_bigclass").change(function(){
        var id = parseInt($(this).val());
            $.ajax({
                type:'get',
                url:'../json.php?act=getsmallclass&id_bigclass='+id,
                dataType:'json',
                success:function(data){
                    var html="";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].smallclass+"</option>";
                    }
                    $("#id_smallclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });
	
});
function setmethod(tag){
	$("#actionmethod").val(tag);
	if(tag=="output"){
		$("#searchform").attr("target","_blank");
	}else{
		$("#searchform").removeAttr("target");
	}
}
</script>


		<link rel="stylesheet" href="../kindeditor/themes/default/default.css" />
		<script src="../kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="../kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>
<body style="background:#FCFCFC;">
<SCRIPT language=javascript>
function check()
{
	
if (document.form3.type.value=="")
{
alert("类型必须选择！");
return false;
}
if (document.form3.id_bigclass.value=="")
{
alert("一级分类必须填写，如没有，请先添加！");
return false;
}
if (document.form3.id_smallclass.value=="")
{
alert("二级分类必须填写，如没有，请先添加！");
return false;
}
if (document.form3.price.value=="")
{
alert("金额必须填写！");
return false;
}

if (document.form3.wanglai.value=="")
{
alert("<?php echo $webconfig['system_wanglai'];?>必须填写，请选择！如没有，请先添加！");
return false;
}
if (document.form3.yuangong.value=="")
{
alert("<?php echo $webconfig['system_yuangong'];?>必须填写，请选择！如没有，请先添加！");
return false;
}
if (document.form3.xiangmu.value=="")
{
alert("<?php echo $webconfig['system_xiangmu'];?>必须填写，请选择！如没有，请先添加！");
return false;
}

}
</script>
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>记一笔</h1>
</header>
<div class="hui-wrap">
	<div style="padding:5px;">
		<div class="hui-segment" id="cate">
			<a href="money_add.php">收支记账</a>
			<a href="money_pay_add.php" class="hui-segment-active">应收应付记账</a>
		</div>
	</div>

<form action='' method='post' name="form3">
        <div class="hui-form-items">
            <div class="hui-form-items-title">类型：</div>
            <div class="hui-form-select">
			<select name="type" class="select" id="type">
			<option value=''>请选择</option>
			<?php 
			foreach($c_type1 as $k=>$v){
						echo "<option value='{$k}'>{$v}</option>";
			}?>
			</select>	
            </div>
        </div>

        <div class="hui-form-items">
            <div class="hui-form-items-title">一级分类：</div>
            <div class="hui-form-select">
				<select name="id_bigclass" class="select" id="id_bigclass">
				</select>
            </div>
        </div>


        <div class="hui-form-items">
            <div class="hui-form-items-title">二级分类：</div>
            <div class="hui-form-select">
				<select name="id_smallclass" class="select" id="id_smallclass">
				</select>
            </div>
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">金额：</div>
            <input class='inp' type='number' name='price' placeholder="0.00" step="0.01" />
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">时间：</div>
            <input type='text' class='inp' name='selldate' value='<?php echo date("Y-m-d",time()) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">期限：</div>
            <input type='text' class='inp' name='Lastdate' value='<?php echo date("Y-m-d",strtotime('+7 days')) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
        </div>

        <div class="hui-form-items">
            <div class="hui-form-items-title"><?php echo $webconfig['system_wanglai'];?>：</div>
            <div class="hui-form-select">
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
if(intval($_SESSION['eptime_l_wanglai'])===intval($v['id'])){echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";}
else{echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
            </div>
        </div>



        <div class="hui-form-items">
            <div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?>：</div>
            <div class="hui-form-select">
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
if(intval($_SESSION['eptime_l_yuangong'])===intval($v['id'])){echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";}
else{echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
            </div>
        </div>


        <div class="hui-form-items">
            <div class="hui-form-items-title"><?php echo $webconfig['system_xiangmu'];?>：</div>
            <div class="hui-form-select">
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
if(intval($_SESSION['eptime_l_xiangmu'])===intval($v['id'])){echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";}
else{echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
            </div>
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">备注</div>
            <input type="text" class="hui-input hui-input-clear" name="beizhu"/>
        </div>

        <div class="hui-form-items">
            <div class="hui-form-items-title">附件：</div>
            <input type='text' class='inp' name='img' placeholder='' id="url3" readonly/>&nbsp;&nbsp;<input type="button" value="上传" id="open" class="layui-btn">
        </div>
     <div class="hui-form-items">
            <div class="hui-form-items-title"></div>     
            <div class="alert alert-info" style="width:92%" id="demo2"></div>
            <div class="item col-xs-12"><div class="f-fl item-ifo">
<link rel="stylesheet" href="../layui/css/layui.css" media="all">
<script type="text/javascript" src="../layui/layui.js"></script>
<script type="text/javascript">
layui.use('upload', function (){
  var upload = layui.upload,
    $ = layui.jquery;
var uploadInst = upload.render({
  elem : '#open',
  accept : 'images',//指定允许上传时校验的文件类型，可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
  multiple : 'true',
  url : '../up.php',
  data: {},
  before: function(obj){
      obj.preview(function(index, file, result){
        $('#demo2').append('<img src="'+ result +'" alt="'+ file.name +'" style="width:33%;">')
      });
    },
  done : function(res){
if(res['code']==1){
//alert(res['src']);
var imgpath=res['src'];
//alert(imgpath);
layer.msg('图片上传成功');
imgpath = imgpath + ','+$("#url3").val();
$("#url3").val(imgpath);
}else{
layer.msg('图片上传失败');
}
  },
  error : function(){   
    //请求异常
  }
});
});

</script>
            </div></div>
        </div>
        <div style="padding:15px 8px;">
            <input type='submit' class="hui-button hui-button-large hui-primary" id="submitBtn" value='记一笔' onClick="return check()"/>
        </div>
</form>

</div>
<?php include 'footer.php';?>

</body>
</html>