<?php
require_once(dirname(__FILE__)."/common.php");
if (strpos($_SESSION['eptime_flag'], 'zhangzfig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__zhanghu where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}

if(!empty($_POST)){
	
	extract($_POST);
$type=2;
$moneyID=c_newOrderNo("Z");
$price=trim($price);
$price11=trim($price11);
$selldate=trim($selldate);
    $zhanghu = intval($zhanghu);
	$zhanghu1= intval($zhanghu1);
	$beizhu= trim($beizhu);
$id_login = $_SESSION['eptime_id'];
$login = $_SESSION['eptime_username'];
$addtime = date("Y-m-d H:i:s",time());

	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}
	if($price11-$price<0){
		LYG::ShowMsg('转出账户余额不足，不能进行操作!');
	}

if($check_enable){$isok = 1;}else{$isok = 0;}

	$data = array(
		'type'		=>$type,
        'price'		=>$price,
        'selldate'	=>$selldate,
		'zhanghu'	=>$zhanghu,
        'zhanghu1'	=>$zhanghu1,
		'beizhu'	=>$beizhu,
		'moneyID'	=>$moneyID,
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);

$result = $con->add("money_pay",$data);






$beizhu="[转出".$moneyID."] ￥".$price."元";
$beizhu1="[转入".$moneyID."] ￥".$price."元";
$moneyID1 = c_newOrderNo("Z");
$id_login = $_SESSION['eptime_id'];
$login = $_SESSION['eptime_username'];
$addtime = date("Y-m-d H:i:s",time());
if($check_enable){
	$isok = 1;
		}
		else{$isok = 0;}

	$data1 = array(
		'type'		=>'0',
  'id_bigclass'		=>'11',
 'id_smallclass'	=>'16',
        'price'		=>$price,
        'selldate'	=>$selldate,
		'zhanghu'	=>$zhanghu1,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'moneyID'	=>$moneyID1,
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);

	$data2 = array(
		'type'		=>'1',
  'id_bigclass'		=>'10',
 'id_smallclass'	=>'17',
        'price'		=>$price,
        'selldate'	=>$selldate,
		'zhanghu'	=>$zhanghu,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu1,
		'moneyID'	=>$moneyID1."2",
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);

$aok = $con->add("money",$data1);
$aok = $con->add("money",$data2);












	if($result!==false){

$eok = $con->Update("update #__zhanghu set amount=amount-{$price} where id={$zhanghu}");
$eok = $con->Update("update #__zhanghu set amount=amount+{$price} where id={$zhanghu1}");

LYG::writeLog("[".$_SESSION['eptime_username']."]转账成功！");
		LYG::ShowMsg('转账成功','money_zhang.php');
	}else{
		LYG::ShowMsg('没有更改');
	}
	
	die();
	
}

$classes = $con->select("select * from #__zhanghu where id!=$dataid");
	
?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title></title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
</head>
<body style="background:#FCFCFC;">
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>账户转账</h1>
</header>
<div class="hui-wrap">
<form action='' method='post'>
<input type='hidden' name='id' value="<?php echo $dataid;?>" />

        <div class="hui-form-items">
        	<div class="hui-form-items-title">转出账户：</div>
<?php echo $info['name'];?><input type='hidden' name='zhanghu' value="<?php echo $dataid;?>" /><input type='hidden' name='price11' value="<?php echo $info['amount'];?>" />
        </div>
        <div class="hui-form-items">
            <div class="hui-form-items-title">转入账户：</div>
            <div class="hui-form-select">
				<select name="zhanghu1" class="select">
				<?php
				foreach($classes as $k=>$v){
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";
				}
				?>
				</select>
            </div>
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">金额：</div>
            <input type="number" class="hui-input" step="0.01" name="price"  />
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">时间：</div>
            <input type="text" class="hui-input hui-input-clear" name="selldate" value='<?php echo date("Y-m-d",time()) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
        </div>
        <div class="hui-form-items">
            <div class="hui-form-items-title">备注：</div>
            <div class="hui-form-textarea">
                <textarea placeholder="备注..." name="beizhu"></textarea>
            </div>
        </div>
        <div style="padding:15px 8px;">
            <input type="submit" class="hui-button hui-primary hui-fr" id="submitBtn">
        </div>
    </form>


</div>
<?php include 'footer.php';?>
</body>
</html>