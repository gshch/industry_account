<?php
require_once(dirname(__FILE__)."/common.php");
$webconfig = lyg::readArr("web");

?>
<?php
$_week1 =strtotime(date("Y-m-d 00:00:00",strtotime("last Sunday +1 day")));
$_month1 =strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
$_year1 =strtotime(date("Y-01-01 00:00:00")); 
$_all1 =strtotime(date("1000-01-01 00:00:00")); 
function _total($pay,$type,$time1){
	global $con;
	if($pay==1){
		//统计收
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} ";}
	
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return (int)round($sl['sl'],2);
	}else if($pay ==2){
		//支应收应付

    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as s2 from #__money_pay where type={$type} and UNIX_TIMESTAMP(selldate)>={$time1} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as s2 from #__money_pay where type={$type} and UNIX_TIMESTAMP(selldate)>={$time1} ";}
		
		$s2 = $con->find($sql);
		if(empty($s2['s2'])){ $s2['s2'] = 0;}
		return (int)round($s2['s2'],2);
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>首页</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
</head>
<body>
<div class="hui-wrap">
	<div style="height:30px;">
	     
	</div>
	<style type="text/css">
	.bgGreen{background:#009900 !important;}
	.bgRed{background:#EE4B47 !important;}
	.bgBlue{background:#1BC5BB !important;}
	.demo{width:100%; height:40px; text-align:left; text-indent: 10px;color:#FFF; line-height:40px; font-size:14px; margin:5px; background:#3388FF;}
.demo1{width:100%; height:90px; text-align:center; color:#000; font-size:12px; margin:5px; background:FFF;}
	</style>

	<div class="hui-flex">
		<div class="demo1"><a href="money_add.php"><img src="images/s1.png"><br>记一笔</a></div>
		<div class="demo1"><a href="money.php"><img src="images/s2.png"><br>记账流水</a></div>
		<div class="demo1"><a href="money_zhang.php"><img src="images/s3.png"><br>资金管理</a></div>
		<div class="demo1"><a href="tongji.php"><img src="images/s4.png"><br>图形报表</a></div>
	</div>



	<div class="hui-common-title" style="margin-top:5px;">
		<div class="hui-common-title-line"></div>
		<div class="hui-common-title-txt">本周收支情况</div>
		<div class="hui-common-title-line"></div>
	</div>
	<div class="hui-flex">
		<div class="demo bgRed">收入:<?php echo _total("1","0",$_week1);?>元</div>
		<div class="demo bgGreen">支出:<?php echo _total('1','1',$_week1);?>元</div>
		<div class="demo bgBlue">盈亏:<?php echo _total("1","0",$_week1)-_total('1','1',$_week1);?>元</div>
	</div>
	<div class="hui-flex">
		<div class="demo ">应收:<?php echo _total("2","0",$_week1);?>元</div>
		<div class="demo bgBlue">应付:<?php echo _total('2','1',$_week1);?>元</div>
		<div class="demo bgBlue">净余:<?php echo _total("2","0",$_week1)-_total('2','1',$_week1);?>元</div>
	</div>


	<div class="hui-common-title" style="margin-top:5px;">
		<div class="hui-common-title-line"></div>
		<div class="hui-common-title-txt">本月收支情况</div>
		<div class="hui-common-title-line"></div>
	</div>
	<div class="hui-flex">
		<div class="demo bgRed">收入:<?php echo _total('1','0',$_month1);?>元</div>
		<div class="demo bgGreen">支出:<?php echo _total('1','1',$_month1);?>元</div>
		<div class="demo bgBlue">盈亏:<?php echo _total('1','0',$_month1)-_total('1','1',$_month1);?>元</div>
	</div>
	<div class="hui-flex">
		<div class="demo ">应收:<?php echo _total('2','0',$_month1);?>元</div>
		<div class="demo bgBlue">应付:<?php echo _total('2','1',$_month1);?>元</div>
		<div class="demo bgBlue">净余:<?php echo _total('2','0',$_month1)-_total('2','1',$_month1);?>元</div>
	</div>



 

<?php
$sql = "select #__zhanghu.*,#__zhanghu_class.zhanghuclass from #__zhanghu left join #__zhanghu_class on #__zhanghu_class.id = #__zhanghu.type order by  #__zhanghu.id desc";
$data = $con->select($sql,$_v);

if (strpos($_SESSION['eptime_flag'], 'zhangfig') === false) {}
else{
?>
   <div class="hui-common-title" style="margin-top:15px;">
		<div class="hui-common-title-line"></div>
		<div class="hui-common-title-txt">资金账户信息</div>
		<div class="hui-common-title-line"></div>
	</div>
	<style type="text/css">
	.demo2{height:50px; text-align:left; text-indent: 10px;color:#FFF; line-height:50px; font-size:15px; margin:5px; background:#3388FF;}
	</style>

	<div>
	<?php $amount=0; foreach($data as $k=>$v){?>
<div class="demo2">[<?php echo $v['zhanghuclass'];?>]<?php echo $v['name'];?>：<?php echo round($v['amount'],2);?>元</div>
	<?php $amount=$amount+$v['amount'];}?>
<div class="demo2 bgRed">合计：<?php echo round($amount,2);?>元</div>
	</div>
<?php }
if($_SESSION['eptime_adminPower']<>2){
?>

 

<?php }?>

</div>



	<div class="hui-common-title" style="margin-top:5px;">
		<div class="hui-common-title-line"></div>
		<div class="hui-common-title-txt">全部收支情况</div>
		<div class="hui-common-title-line"></div>
	</div>
	<div class="hui-flex">
		<div class="demo bgRed">收入:<?php echo _total('1','0',$_all1);?>元</div>
		<div class="demo bgGreen">支出:<?php echo _total('1','1',$_all1);?>元</div>
		<div class="demo bgBlue">盈亏:<?php echo _total('1','0',$_all1)-_total('1','1',$_all1);?>元</div>
	</div>
	<div class="hui-flex">
		<div class="demo ">应收:<?php echo _total('2','0',$_all1);?>元</div>
		<div class="demo bgBlue">应付:<?php echo _total('2','1',$_all1);?>元</div>
		<div class="demo bgBlue">净余:<?php echo _total('2','0',$_all1)-_total('2','1',$_all1);?>元</div>
	</div>
	<div class="hui-flex">
		<div class="demo bgRed">总收:<?php echo _total('1','0',$_all1)+_total('2','0',$_all1);?>元</div>
		<div class="demo bgGreen">总支:<?php echo _total('1','1',$_all1)+_total('2','1',$_all1);?>元</div>
		<div class="demo bgBlue">合计:<?php echo _total('1','0',$_all1)-_total('1','1',$_all1)+_total('2','0',$_all1)-_total('2','1',$_all1);?>元</div>
	</div>	

<?php include 'footer.php';?>
<script src="js/hui-swipe.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
var swipe = new huiSwpie('#swipe');
swipe.autoPlay = false;
swipe.run();
</script>
</body>
</html>
