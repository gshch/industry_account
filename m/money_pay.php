<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>记账流水</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<style type="text/css">
.hui-tab-item{border:0px;}
</style>
</head>
<body>
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>记账流水</h1>
	<a href="index.php"><div id="hui-header-menu1"></div></a>
</header>


<div class="hui-wrap">

	<div style="padding:5px;">
		<div class="hui-segment" id="cate">
			<a href="money.php">收支流水</a>
			<a href="money_pay.php"  class="hui-segment-active">应收应付流水</a>
		</div>
	</div>

    <div id="refreshContainer" class="hui-refresh">
        <div class="hui-refresh-icon"></div>
        <div class="hui-refresh-content hui-list">
            <ul id="list"></ul>
        </div>
    </div>
</div>


<script src="js/hui.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/hui-refresh-load-more.js"></script>
<script type="text/javascript">
var page = 1;
hui.refresh('#refreshContainer', refresh);
hui.loadMore(getMore);
//加载更多
function getMore(){
    hui.get(
        'list.php?m=2&p='+page,
        function(res){
            //判断加载完毕
            if(res == 'null'){
                hui.endLoadMore(true, '已经到头了...');
                return false;
            }
            var data = res.split('--hcSplitor--');
            for(var i = 0; i < data.length; i++){
                var li = document.createElement('li');
                li.innerHTML = '<div class="hui-list-text">'+data[i]+'</div>';
                hui(li).appendTo('#list');
            }
            page++;
            hui.endLoadMore();
        }
    );
}

//下拉刷新
function refresh(){
    hui.loading('加载中...');
    hui.get(
        'list.php?m=2&p=1',
        function(res){
            hui.closeLoading();
            var data = res.split('--hcSplitor--');
            var html = '';
            for(var i = 0; i < data.length; i++){
                html += '<div class="hui-list-text">'+data[i]+'</div>';
            }
            page = 2;
            hui('#list').html(html);
            //结束刷新
            hui.endRefresh();
            //重置加载更多状态
            hui.resetLoadMore();
        },
        function(){
            hui.closeLoading();
            hui.upToast('连接服务器失败！');
            hui.endRefresh();
        }
    );
}
</script>
</body>
</html>