<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>关于我们</title>
<link rel="stylesheet" type="text/css" href="css/hui.css" />
<style type="text/css">
body{background:#FFF;}
.line{height:20px;}
</style>
</head>
<body>
<header class="hui-header">
    <div id="hui-back"></div>
    <h1>关于我们</h1>
</header>
<div class="hui-wrap">
	<div class="line"></div>
	<div class="hui-center-title">
		<h1>公司简介</h1>
	</div>
	<div class="line"></div>
	<div class="hui-content" style="padding:10px;">
		<h2>山西先启科技有限公司</h2>
<p>山西先启科技有限公司（简称“先启科技”）成立于2011年5月，是一家集软硬件的研发、生产、销售、服务于一体的高科技企业，也是山西省内最有影响力的高科技企业之一。公司位于山西省太原市，注册资金110万元，公司秉承“理念为先，启迪未来”的企业理念，以市场需求为导向，致力于互联网、物联网、云计算等技术应用产品的研发及服务，切实满足客户对互联网、物联网、云计算等技术应用于企业信息化的需求，为客户提供最优的解决方案。</p>
<p>目前公司主要为中小企业提供综合信息化服务，包括网络基础服务(域名、网络空间、企业基础网络等)、网络营销服务(微信、微博、网站等自媒体的社会化营销及会员制、网络广告)、企业信息化服务（财务系统、业务管理系统等）及信息系统集成软硬件产品。可以提高企业工作效率、经营管理和财务控制的能力，降低营销成本、增加销售额等。</p>

<h2>联系方式</h2>
                                    <p>地址：太原市小店区平阳路东巷60号文教中心316室</p>
                                    <p>电话：400-878-0621  0351-2780621</p>
                                    <p>邮箱：info@xqkj.com.cn</p>
                                    <p>网址：www.xqkj.com.cn</p>

	</div>
</div>
<?php include 'footer.php';?>
</body>
</html>