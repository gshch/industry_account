<?php
require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");

	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>网站设置</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
</style>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	$("input.inp").blur(function(){
		var v = $(this).val().replace(/ /g,'');
		if(v==''){ alert('设置不能为空'); return;}
		var def = this.defaultValue;
		if(def==v){ return; }
		var d = $(this).attr('data-name');
		$(this).attr("disabled","disabled");
		var obj = $(this);
		
		$.post("json.php",{"act":"setwebconfig","key":d,"val":v},function(e){
			if(e.flag=="1"){
				$(obj).val(e.info);
				$(obj).removeAttr("disabled");
			}else{
				alert(e.info);
			}
		},"Json");
	});
	$("#system_check").click(function(){
		var open = "0";
		if($(this).is(":checked")){
			open = "1";
			$(this).parent().addClass("lightgreen");
		}else{
			$(this).parent().removeClass("lightgreen");
		}
		$.post("json.php",{"act":"setwebconfig","key":"system_check","val":open},function(e){
			if(e.flag=="1"){
			}else{
				alert(e.info);
			}
		},"Json");
	});
	$("#login_code_open").click(function(){
		var open = "0";
		if($(this).is(":checked")){
			open = "1";
			$(this).parent().addClass("lightgreen");
		}else{
			$(this).parent().removeClass("lightgreen");
		}
		$.post("json.php",{"act":"setwebconfig","key":"login_code_open","val":open},function(e){
			if(e.flag=="1"){
			}else{
				alert(e.info);
			}
		},"Json");
	});
	$("#login_sms_open").click(function(){
		var open = "0";
		if($(this).is(":checked")){
			open = "1";
			$(this).parent().addClass("lightgreen");
		}else{
			$(this).parent().removeClass("lightgreen");
		}
		$.post("json.php",{"act":"setwebconfig","key":"login_sms_open","val":open},function(e){
			if(e.flag=="1"){
			}else{
				alert(e.info);
			}
		},"Json");
	});
		$("#login_wx_open").click(function(){
		var open = "0";
		if($(this).is(":checked")){
			open = "1";
			$(this).parent().addClass("lightgreen");
		}else{
			$(this).parent().removeClass("lightgreen");
		}
		$.post("json.php",{"act":"setwebconfig","key":"login_wx_open","val":open},function(e){
			if(e.flag=="1"){
			}else{
				alert(e.info);
			}
		},"Json");
	});
	$("#modi_sms_open").click(function(){
		var open = "0";
		if($(this).is(":checked")){
			open = "1";
			$(this).parent().addClass("lightgreen");
		}else{
			$(this).parent().removeClass("lightgreen");
		}
		$.post("json.php",{"act":"setwebconfig","key":"modi_sms_open","val":open},function(e){
			if(e.flag=="1"){
			}else{
				alert(e.info);
			}
		},"Json");
	});

});
</script>
		<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
		<script src="kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image2').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url2').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url2').val(url);
								editor.hideDialog();
							}
						});
					});
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>


<body class="content">


<form action='' method='post'>

<table cellpadding="3" cellspacing="0" class="table-add">

	<tr>
		<td align="right" width='150' height='36'>&nbsp;</td>
		<td align="left"  width="400" style="color:#fb3506">
			文本信息修改完后鼠标点击页面空白区即可完成修改
		</td>
		<td align="left"></td>
	</tr>
	<tr>
		<td align="right"  height='36'>系统名称：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="system_title" placeholder="系统名称" value="<?php echo $webconfig['system_title'];?>">
		</td>
		<td></td>
	</tr>
		<tr>
		<td align="right"  height='36'>[客户]：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="system_wanglai" placeholder="客户" value="<?php echo $webconfig['system_wanglai'];?>">
		</td>
		<td>[客户]在系统中的显示，请慎重更改，如可以显示为[往来人]等</td>
	</tr>
	<tr>
		<td align="right"  height='36'>[员工]：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="system_yuangong" placeholder="员工" value="<?php echo $webconfig['system_yuangong'];?>">
		</td>
		<td>[员工]在系统中的显示，请慎重更改，如可以显示为[经办人]、[经手人]等</td>
	</tr>
	<tr>
		<td align="right"  height='36'>[项目]：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="system_xiangmu" placeholder="项目" value="<?php echo $webconfig['system_xiangmu'];?>">
		</td>
		<td>[项目]在系统中的显示，请慎重更改，如可以显示为[分公司]、[办事处]等</td>
	</tr>
	<tr>
		<td align="right"  height='36'>审核：</td>
		<td align="left">
			<div class="kaiguan">
				<section class="sevcheckbox <?php if($webconfig['system_check']){ echo " lightgreen";}?>">
					<input type="checkbox" id="system_check" <?php if($webconfig['system_check']){ echo " checked='checked'";}?>>
					<label for="system_check"></label>
				</section>
			</div>
		</td>
		<td>是否开启审核</td>
	</tr>
	<tr>
		<td align="right"  height='36'>登陆验证码：</td>
		<td align="left">
			<div class="kaiguan">
				<section class="sevcheckbox <?php if($webconfig['login_code_open']){ echo " lightgreen";}?>">
					<input type="checkbox" id="login_code_open" <?php if($webconfig['login_code_open']){ echo " checked='checked'";}?>>
					<label for="login_code_open"></label>
				</section>
			</div>
		</td>
		<td>为了您的数据安全，强烈建议开启</td>
	</tr>
	<tr>
		<td align="right"  height='36'>流水每页显示条数：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="eptime_pagesize" placeholder="20" value="<?php echo $webconfig['eptime_pagesize'];?>">
		</td>
		<td>如每页显示20条 </td>
	</tr>


	<tr>
		<td align="right"  height='36'>手机短信登录：</td>
		<td align="left">
			<div class="kaiguan">
				<section class="sevcheckbox <?php if($webconfig['login_sms_open']){ echo " lightgreen";}?>">
					<input type="checkbox" id="login_sms_open" <?php if($webconfig['login_sms_open']){ echo " checked='checked'";}?>>
					<label for="login_sms_open"></label>
				</section>
			</div>
		</td>
		<td>需要配置短信平台</td>
	</tr>
	<tr>
		<td align="right"  height='36'>修改删除短信验证：</td>
		<td align="left">
			<div class="kaiguan">
				<section class="sevcheckbox <?php if($webconfig['modi_sms_open']){ echo " lightgreen";}?>">
					<input type="checkbox" id="modi_sms_open" <?php if($webconfig['modi_sms_open']){ echo " checked='checked'";}?>>
					<label for="modi_sms_open"></label>
				</section>
			</div>
		</td>
		<td>需要配置短信平台</td>
	</tr>

	<tr>
		<td align="right"  height='36'>收支产生的应收付时间差：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="order_interval" placeholder="10" value="<?php echo $webconfig['order_interval'];?>">
		</td>
		<td>单位：秒，如设置为七天收付款，为3600*24*7秒 </td>
	</tr>
	<tr>
		<td align="right"  height='36'>数据备份文件后缀名：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="backrestore_ext" placeholder="如：.sql" value="<?php echo $webconfig['backrestore_ext'];?>">
		</td>
		<td>常规后缀“.sql”，强烈推荐更换，格式：.数字或字母，不允许为“.php”</td>
	</tr>

	<tr>
		<td align="right"  height='36'>首页背景图片：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="index_pic" value="<?php echo $webconfig['index_pic'];?>" id="url2">
		</td>
		<td><input type="button" id="image2" value="选择图片" /> 建议尺寸为1980*1080</td>
	</tr>

	<tr>
		<td align="right"  height='36'>手机端首页banner图片：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="mobile_pic" value="<?php echo $webconfig['mobile_pic'];?>" id="url3">
		</td>
		<td><input type="button" id="image3" value="选择图片" /> 建议尺寸为640*320，多图用"|"隔开</td>
	</tr>
	<tr>
		<td align="right"  height='36'>只允许微信使用：</td>
		<td align="left">
			<div class="kaiguan">
				<section class="sevcheckbox <?php if($webconfig['login_wx_open']){ echo " lightgreen";}?>">
					<input type="checkbox" id="login_wx_open" <?php if($webconfig['login_wx_open']){ echo " checked='checked'";}?>>
					<label for="login_wx_open"></label>
				</section>
			</div>
		</td>
		<td>微信访问地址：<?php echo $_SERVER['HTTP_HOST']; ?>/m</td>
	</tr>
	<tr>
		<td align="right" width='150' height='36'>&nbsp;</td>
		<td align="left"  width="400" style="color:#009900">
			微信公众号平台接入配置信息
		</td>
		<td align="left"></td>
	</tr>

	<tr>
		<td align="right"  height='36'>微信公众号APPID：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="wx_appid" placeholder="微信公众号APPID" value="<?php echo $webconfig['wx_appid'];?>">
		</td>
		<td></td>
	</tr>
	<tr>
		<td align="right"  height='36'>微信公众号密钥：</td>
		<td align="left">
		<input class="inp text" type="password" data-name="wx_secret" placeholder="微信公众号密钥" value="<?php echo $webconfig['wx_secret'];?>">
		</td>
		<td></td>
	</tr>
	<tr>
		<td align="right"  height='36'>微信访问地址：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="wx_url" placeholder="微信访问地址" value="<?php echo $webconfig['wx_url'];?>">
		</td>
		<td>一定要加http://或者https://</td>
	</tr>
	
	<tr>
		<td align="right" width='150' height='36'>&nbsp;</td>
		<td align="left"  width="400" style="color:#009900">
			短信平台配置信息
		</td>
		<td align="left"></td>
	</tr>

	<tr>
		<td align="right"  height='36'>接口API地址：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="sms_api" placeholder="API地址" value="<?php echo $webconfig['sms_api'];?>">
		</td>
		<td>如，http://sms.eptime.cn/sms.php</td>
	</tr>
	<tr>
		<td align="right"  height='36'>接口用户名：</td>
		<td align="left">
			<input class="inp text" type="text" data-name="sms_eptimename" placeholder="用户名" value="<?php echo $webconfig['sms_eptimename'];?>">
		</td>
		<td></td>
	</tr>
	<tr>
		<td align="right"  height='36'>接口认证码：</td>
		<td align="left">
			<input class="inp text" type="password" data-name="sms_eptimecode" placeholder="认证码" value="<?php echo $webconfig['sms_eptimecode'];?>">
		</td>
		<td></td>
	</tr>	
</table>
</form>

</body>
</html>