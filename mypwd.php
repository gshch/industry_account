<?php
require_once(dirname(__FILE__).'/include/common.php');

$debug = lyg::readArr("debug");
if($debug['debug']){
	header('Content-Type:text/html;charset=utf-8');
	echo '演示系统，不能修改密码'; exit;
}

if(!empty($_POST)){		

	$id	= $_SESSION['eptime_id'];
	
	$data = array(
		'UserName' => empty($_POST['username']) ? '' : trim($_POST['username']),
	);
	if(!preg_match('/^[a-zA-Z]{1,}[a-zA-Z0-9_]{1,}$/',$data['UserName'])){
		lyg::showmsg('用户名格式错误');
	}
	if(!empty($_POST['pwd1']) || !empty($_POST['pwd2'])){
		
		if($_POST['pwd1'] != $_POST['pwd2']){
			lyg::showmsg('密码输入不一致');
		}
		$pwd = trim($_POST['pwd1']);
		if(strlen($pwd) < 6){
			lyg::showmsg('密码长度应不少于6位');
		}
		$data['Password'] = c_adminpwd($pwd);
	}
	
	$result	= $con->savemin('admin',$data,'ID='.$id);
	if($result!==false){
		$_SESSION['eptime_username'] = $data['UserName'];
		LYG::ShowMsg('修改成功');
	}else{
		LYG::ShowMsg('修改失败');
	}
		
	die();
}

$id	= $_SESSION['eptime_id'];

//查询数据
$sql = 'select * from #__admin where ID='.$id;
$data = $con->find($sql);
if(!$data || count($data)<1){
	LYG::ShowMsg('数据读取失败');
}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改密码</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
</head>
<script type='text/javascript'>
function ckf(){
	return confirm('确定修改吗？');
}
</script>
<body class="content">

<form action='' method='post' onsubmit='return ckf();'>
<table cellpadding="3" cellspacing="0" class="table-add">
	<tr>
		<td align="right" width='100' height='36'>用户名：</td>
		<td align="left" width='*'>
			<input class='inp' type='text' name='username' value='<?php echo $data['UserName'];?>' readonly/>
			<span>强烈建议修改默认用户名(限：字母数字下划线，且字母开头)</span>
		</td>
	</tr>
	<tr>
		<td align="right" height='36'>新密码：</td>
		<td align="left">
			<input class='inp' type='password' name='pwd1' placeholder=''/>
			<span>密码长度不应小于6位</span>
		</td>
	</tr>
	<tr>
		<td align="right" height='36'>确认密码：</td>
		<td align="left"><input class='inp' type='password' name='pwd2' /></td>
	</tr>
	<tr>
		<td align="right" height='50'>　</td>
		<td align="left"><input class='sub' type='submit' value='修改'/></td>
	</tr>
</table>
</form>

</body>
</html>