<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$pagesize = 20;

//总记录数
$datacount=$con->RowsCount("select count(*) from #__admin");
//总页数
$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(isset($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;
//查询数据
$sql='select * from #__admin order by ID desc limit '.$start_id.','.$pagesize;
$data	=$con->select($sql,'');

//得到分页HTML
$fenye=LYG::getPageHtml($page,$datacount,$pagesize);

$classes = $con->select("select * from #__class");
if(trim($_GET['setpass'])=="ok"){$eok = $con->Update("update #__admin set Password='08c478afaaa5c47119d55da5fdf6dbee' where ID={$_GET['id']}");LYG::ShowMsg('重置成功，新密码为eptime，请使用新密码登陆','user_list.php');}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户管理</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
function setEnable(obj,id){
    $.post("json.php",{"act":"setadminenable","id":id,"enable":obj.checked?"1":"0"},function(e){
        location.reload();
    },"Json");
}
</script>
</head>

<body class="content">


<div class="list-menu">
	<ul>
		<li><a href="user_add.php">添加</a></li>
	</ul>
</div>


<table cellpadding="3" cellspacing="0">
	<thead>
    	<tr>
            <th>ID</th>
            <th>用户名</th>
            <th>真实名</th>
			<th>手机号</th>
            <th>默认资金账户</th>
<th>默认<?php echo $webconfig['system_wanglai'];?></th>
<th>默认<?php echo $webconfig['system_yuangong'];?></th>
<th>默认<?php echo $webconfig['system_xiangmu'];?></th>
<th>重置密码</th>
            <th>级别</th>
            <th>状态</th>
            <th>-</th>
        </tr>


    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>

    	<tr class='list' data-id="<?php echo $v['ID'];?>">
        	<td align="center"><?php echo $v['ID'];?></td>
        	<td align="center"><?php echo $v['UserName'];?></td>
        	<td align="center"><?php echo $v['Realname'];?></td>
			<td align="center"><?php echo $v['mobile'];?></td>
<td align="center"><?php echo c_classname("zhanghu",$v['zhanghu']);?></td>
<td align="center"><?php echo c_classname("wanglai",$v['wanglai']);?></td>
<td align="center"><?php echo c_classname("yuangong",$v['yuangong']);?></td>
<td align="center"><?php echo c_classname("xiangmu",$v['xiangmu']);?></td>
			<td align="center">
<?php if($v['AdminPower']<>0){?>
<a onclick="return confirm('确定重置吗？');" href="?setpass=ok&id=<?php echo $v['ID'];?>" title="重置为eptime"><font color=red>点击重置</font></a>
<?php }?>  

			</td>
<td align="center"><?php echo $c_adminpower["{$v['AdminPower']}"];?></td>

<td align="center"><input type="checkbox" class="enable" <?php if($_SESSION['eptime_id']!=$v['ID']){ echo "onclick=\"setEnable(this,{$v['ID']});\"";} echo $v['Working']=="1"?" checked='checked'":"";?>/></td>
            <td align="center">
				<a class="edit" href="user_edit.php?id=<?php echo $v['ID'];?>"><i class="fa fa-pencil-square-o"></i><span>编辑</span></a>
				<a onclick="return confirm('确定删除吗？');" class="del" href="user_del.php?id=<?php echo $v['ID'];?>"><i class="fa fa-close"></i><span>删除</span></a>
			</td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="12" style="padding-left:30px;">
			<?php echo $fenye ;?>
			</td>
        </tr>
    </tfoot>
</table>


</body>
</html>