<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 
$_k = array();
$_v = array();

$_c = array();//分页条件
$_s = array();//搜索条件

if(!empty($_GET['type']) && intval($_GET['type'])>0){
    $_k[]="#__zhanghu.type=?";
    $_v[]=intval($_GET['type']);
    $_c[]="type=".intval($_GET['type']);
    $_s['type'] = intval($_GET['type']);
}

$_k = implode(' and ',$_k);
if($_k!=''){
    $_k = " where ".$_k;
}



$pagesize = 20;

//总记录数
$datacount=$con->RowsCount("select count(*) from #__zhanghu {$_k}",$_v);
//总页数
$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(isset($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;
//查询数据
$sql = "select #__zhanghu.*,#__zhanghu_class.zhanghuclass from #__zhanghu left join #__zhanghu_class on #__zhanghu_class.id = #__zhanghu.type {$_k} order by  #__zhanghu.id desc limit $start_id,$pagesize";
$data = $con->select($sql,$_v);

//得到分页HTML
$fenye=LYG::getPageHtml($page,$datacount,$pagesize);

$classes = $con->select("select * from #__zhanghu_class");

if(trim($_GET['shenhe'])=="ok"){$eok = $con->Update("update #__zhanghu set sy=1 where id={$_GET['id']}");echo "<script>location.href='zhanghu_list.php';</script>";}
if(trim($_GET['shenhe'])=="kk"){$eok = $con->Update("update #__zhanghu set sy=0 where id={$_GET['id']}");echo "<script>location.href='zhanghu_list.php';</script>";}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>账户管理</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
function search(obj){
	document.searchform.submit();
}
$(function(){
	$("input.sort").blur(function(){
		
		var sort = parseInt($(this).val());
		if(isNaN(sort)){
			sort = 100;
		}
		if(sort == parseInt(this.defaultValue)){
			return;
		}
		
		var id = $(this).parent().parent().attr("data-id");
		
		$.post("json.php",{"act":"zhanghusort","id":id,"sort":sort},function(e){
			location.reload();
		},"json");
	});
});
</script>
</head>

<body class="content">


<div class="searchform">
    <form method="get" name="searchform">
	<table>
		<tr>
			<td width="80" align="left">账户类型</td>
			<td width="200">
			<select name="type" class="select bai" onchange="search(this);">
				<option value='0'>不限</option>
			<?php
			foreach ($classes as $k => $v) {
				if(array_key_exists('type', $_s) && intval($_s['type'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['zhanghuclass']}</option>";
				}else{
					echo "<option value='{$v['id']}'>{$v['zhanghuclass']}</option>";    
				}                    
			}
			?></select>
			</td>
			<td width="100" align="center"><li><a href="zhanghuclass.php">账户类型管理</a></li></td>
			<td width="*"></td>
		</tr>
	</table>
    </form>
</div>

<div class="list-menu">
	<ul>
		<li><a href="zhanghu_add.php">添加</a></li>
	</ul>
</div>


<table cellpadding="3" cellspacing="0">
	<thead>
    	<tr>
            <th>ID</th>
            <th>账户名称</th>
            <th>当前余额</th>
            <th>期初（创建时）金额	</th>
            <th>账户类型</th>
			<th>是否首页显示</th>
            <th>备注</th>
            <th>-</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list' data-id="<?php echo $v['id'];?>">
        	<td align="center"><?php echo $v['id'];?></td>
        	<td align="center"><?php echo $v['name'];?></td>
        	<td align="center"><?php echo round($v['amount'],2);?></td>
        	<td align="center"><?php echo round($v['amount0'],2);?></td>
        	<td align="center"><?php echo $v['zhanghuclass'];?></td>
			<td align="center">
<?php if($v['sy']==1){echo "<a href=?shenhe=kk&id={$v['id']}><font color=red>是</font></a>";}else{echo "<a href=?shenhe=ok&id={$v['id']}><font color=grey>否</a></a>";}?>
			</td>
			<td align="center"><?php echo $v['beizhu'];?></td>
            <td align="center">
				<a class="edit" href="zhanghu_edit.php?id=<?php echo $v['id'];?>"><i class="fa fa-pencil-square-o"></i><span>编辑</span></a>
				<a onclick="return confirm('确定删除吗？');" class="del" href="zhanghu_del.php?id=<?php echo $v['id'];?>"><i class="fa fa-close"></i><span>删除</span></a>
			</td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="8" style="padding-left:30px;">
			<?php echo $fenye ;?>
			</td>
        </tr>
    </tfoot>
</table>


</body>
</html>