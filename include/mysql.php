﻿<?php

class mySQLHelper{
	private $host	= MYSQL_HOST;
	private $dbname	= MYSQL_DATABASE;
	private $uid	= MYSQL_USER;
	private $pwd	= MYSQL_PWD;
	private $port	= MYSQL_PORT;
	private $prefix	= MYSQL_TABLE_PRE;
	private	$con;
	function __construct(){
		try{
			$this->con = new PDO(
				"mysql:host={$this->host};port={$this->port};dbname={$this->dbname}",
				$this->uid,
				$this->pwd,
				array(PDO::MYSQL_ATTR_INIT_COMMAND => "set names 'utf8';")
			);
		}catch(Exception $e){
			exit('MySQL error');
		}
	}
	public function begin(){
		$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if(!$this->con->beginTransaction()){
			exit('Error 001');
		}
	}
	public function back(){
		$this->con->rollBack();
	}
	public function done(){
		$this->con->commit();
	}
	//返回结果集合，失败返回null(select)
	private function MyQuery($sql){
		$sql = str_replace('#__',$this->prefix,$sql);
		$data=$this->con->query($sql);
		if($data)
			return $data->fetchAll(PDO::FETCH_ASSOC);
		else
			return null;
	}
	public function select($sql,$arr=array()){
		$sql = str_replace('#__',$this->prefix,$sql);
		$dp=$this->con->prepare($sql);		
		if(is_array($arr)){
			for($i=1;$i<=count($arr);$i++){
				$dp->bindParam($i,$arr[$i-1]);
			}
		}
		$dp->execute();
		if($dp){
			return $dp->fetchAll(PDO::FETCH_ASSOC);			
		}else{
			return null;
		}
	}
	public function find($sql,$arr=array()){
		$items = $this->select($sql,$arr);
		if($items && count($items)>0){
			return $items[0];
		}else{
			return null;
		}
	}

	//返回最新ID的参数化方法(如：insert)
	public function Insert($sql,$arr=array()){
		$sql = str_replace('#__',$this->prefix,$sql);
		$dp=$this->con->prepare($sql);
		if(is_array($arr)){
			for($i=1;$i<=count($arr);$i++){
				$dp->bindParam($i,$arr[$i-1]);
			}
		}
		$dp->execute();
		return $this->con->lastInsertId();
	}
	public function add($tablename,$data=array()){
		$fields = array();
		$wen = "";//问号  ?
		$zhi = array();
		foreach($data as $k=>$v){
			$fields[] = $k;
			$zhi [] = $v;
			$wen.=$wen==''?'?':',?';
		}
		$fields = implode(',',$fields);
		$sql = "insert into #__{$tablename}({$fields}) values({$wen})";
		return $this->insert($sql,$zhi);
	}
	//小型update操作，where条件全为 and =
	public function savemin($table,$newdata=array(),$where,$limit="1"){
		$_k = array();
		$_v = array();
		foreach($newdata as $a=>$b){
			$_k[] = "$a=?";
			$_v[] = $b;
		}
		$_k = implode(",",$_k);
		
		if(is_array($where)){
			$_x = array();
			foreach($where as $a=>$b){
				$_x[] = "$a=?";
				$_v[] = $b;
			}
			$_x = implode(" and ",$_x);
		}else{
			$_x = $where;
		}		
		
		$sql = "update #__{$table} set {$_k} where {$_x} limit {$limit}";
		return $this->update($sql,$_v);	
	}
	//
	public function Update($sql,$arr=array()){
		$sql = str_replace('#__',$this->prefix,$sql);
		$dp=$this->con->prepare($sql);
		if(is_array($arr)){
			for($i=1;$i<=count($arr);$i++){
				$dp->bindParam($i,$arr[$i-1]);
			}
		}
		if($dp->execute()){
			return $dp->rowCount();
		}
		return false;
	}
	
	//返回受影响行数(insert,delete,update)
	public function Excute($sql){
		$sql = str_replace('#__',$this->prefix,$sql);
		$rows=$this->con->exec($sql);
		return $rows;
	}
		
	//满足条件数量统计(select count(*) ... )
    public function RowsCount($sql,$arr=array()){
		$sql = str_replace('#__',$this->prefix,$sql);
        try{
            $dp=$this->con->prepare($sql);       
            if(is_array($arr)){
                for($i=1;$i<=count($arr);$i++){
                    $dp->bindParam($i,$arr[$i-1]);
                }
            }
            $dp->execute();
            if($dp){
                $x = $dp->fetchColumn();
                return intval($x);
            }
            return 0;
        }catch(Exception $e){
            die("Error");
        }
    }
}

$con = new mySQLHelper;
