<?php

if(!extension_loaded('pdo_mysql')){ die('pdo_mysql needed'); }

//载入数据库安装配置
$install_success = init_read_install_config();
if(empty($install_success['host'])){ exit('config error'); }


//------以下内容不允许修改
define("APP_NAME","eptime management system");

$_http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';

define("HOST_URL",$_http_type.$_SERVER['HTTP_HOST']);//根目录网址
define("APP_PATH",dirname(dirname(__FILE__)));//应用目录

$_app_url = str_replace(realpath($_SERVER['DOCUMENT_ROOT']),"",__FILE__);
$_app_url = str_replace("\init.php","",$_app_url);
$_app_url = str_replace("/init.php","",$_app_url);
$_app_url = HOST_URL.str_replace("\\","/",$_app_url);

define("APP_URL",$_app_url);//应用网址



define("ADMIN_DIR",$install_success['houtai']);//后台文件夹名字，开头和结尾均不含斜杠
//--数据库配置，请根据实际进行修改
define("MYSQL_HOST",$install_success['host']);//数据库连接地址，常用：localhost
define("MYSQL_PORT",$install_success['port']);//数据库连接端口，常用：3306
define("MYSQL_USER",$install_success['user']);//数据库连接用户名
define("MYSQL_PWD", $install_success['pwd']);//数据库连接用户密码
define("MYSQL_DATABASE",$install_success['database']);//数据库名
define("MYSQL_TABLE_PRE","eptime_");//数据库表前缀，无需修改

define("ADMIN_PATH",dirname(__FILE__));//应用目录
define("ADMIN_URL",APP_URL."/".ADMIN_DIR);//应用后台网址
date_default_timezone_set('PRC');


function init_read_install_config(){
	return include dirname(__FILE__)."/database.php";
}

