<?php

require_once(dirname(__FILE__).'/include/common.php');
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>详情</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
label{margin-right:10px;}
</style>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
		<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
		<script src="kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>


<body class="content">

<table cellpadding="3" cellspacing="0" class="table-add">
		<tr>
			<td align="right" height='36'>单号：</td>
			<td>
			<?php echo $info['moneyID'];?>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>类型：</td>
			<td>
			[<?php echo $c_type["{$info['type']}"];?>]<?php echo c_bigclassname($info['id_bigclass']);?>-><?php echo c_smallclassname($info['id_smallclass']);?>
			
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>金额：</td>
			<td>
			<?php echo $info['price'];?>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>时间：</td>
			<td>
			<?php echo substr($info['selldate'],0,10) ?>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>资金账户：</td>
			<td align="left" width='*'>
<?php echo c_zhangname($info['zhanghu']);?>
			</td>
		</tr>



		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_wanglai'];?>：</td>
			<td align="left" width='*'>
<?php echo c_classname("wanglai",$info['wanglai']);?>

			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_yuangong'];?>：</td>
			<td align="left" width='*'>
<?php echo c_classname("yuangong",$info['yuangong']);?>

			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_xiangmu'];?>：</td>
			<td align="left" width='*'>
<?php echo c_classname("xiangmu",$info['xiangmu']);?>
			</td>
		</tr>

	<tr>
		<td align="right" height='36'>备注：</td>
		<td align="left"><?php echo $info['beizhu'];?></td>
	</tr>
	<tr>
		<td align="right" height='36'>附件：</td>
		<td align="left"><?php 
//附件显示开始
if ($info['img']!=""){
$count = count(explode(',', $info['img']));
$kk=0;
foreach( explode(',', $info['img']) as $k1=>$v1)
{$kk=$kk+1;
if($count<>$kk){echo '<a href="'.$v1.'" target="blank"><img src="'.$v1.'" width=33%></a>';}
}		
			}
//附件显示结束			
			?>
		</td>
	</tr>  



      <tr>
        <td align="right" height="30">记账人：</td>
<td align="left"><?php echo c_adminname($info['id_login']);?></td>
      </tr>

	  	  <tr>
        <td align="right" height="30">添加时间：</td>
<td align="left"><?php echo $info['addtime'];?></td>
      </tr>	
	  	  	  <tr>
        <td align="right" height="30">最后一次修改人：</td>
<td align="left"><?php echo $info['LastLogin'];?></td>
      </tr>	
	  	  	  <tr>
        <td align="right" height="30">最后一次修改时间：</td>
<td align="left"><?php echo $info['LastTime'];?></td>
      </tr>	
<?php if($check_enable){ ?>
	  <tr>
        <td align="right" height="30">审核状态：</td>
<td align="left"><?php if ($info['isok']==0){echo '已审';}?><?php if ($info['isok']==1){echo '未审';}?></td>
	  	 <tr>
        <td align="right" height="30">审核人：</td>
<td align="left"><?php echo $info['shenhe'];?></td>
      </tr>	
	  	  <tr>
        <td align="right" height="30">审核时间：</td>
<td align="left"><?php echo $info['shenhetime'];?></td>
      </tr>	
<?php } ?>

</table>
</body>
</html>