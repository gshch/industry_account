<?php

require_once(dirname(__FILE__).'/include/common.php');

$data = $con->select("select * from #__zhanghu_class");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>账户类型</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(function(){
	$(".new-express-add").click(function(){
		var name = $(".new-express-name").val();
		if(name==''){
			alert("请输入新账户类型名称"); return;
		}
		$.post("json.php",{"act":"addexpress","name":name},function(e){
			if(e.flag=="1"){
				location.reload();
			}else{
				alert(e.info);
			}
		},"json");
	});
	$(".express-name").blur(function(){
		var newname = $(this).val();
		if(newname==this.defaultValue){
			reuturn;
		}
		var id = $(this).parent().parent().attr("data-id");
		$.post("json.php",{"act":"editexpress","id":id,"name":newname},function(e){
			if(e.flag=="1"){
				location.reload();
			}else{
				alert(e.info);
			}
		},"json");
	});
	$("a.del").click(function(){
		if(!confirm("删除后不可恢复，是否确认删除？")){
			return;
		}
		var id = $(this).parent().parent().attr("data-id");
		$.post("json.php",{"act":"expresshasorder","id":id},function(e){
			if(e.flag=="1"){
				if(!confirm("使用了该账户类型，是否确定要删除？")){
					return;
				}
			}
			$.post("json.php",{"act":"delexpress","id":id},function(e){
				location.reload();
			},"json");
		},"json");
	});
	$("#express-enable").change(function(){
		var cked = this.checked?"1":"0";
		$.post("json.php",{"act":"enableexpress","enable":cked},function(e){
			location.reload();
		},"json");
	});
});
</script>
</head>

<body class="content">

<style type="text/css">
a.del{ padding-left: 0 }
th{ text-align: left; }
td{ text-align: left; }
.express-name,.new-express-name{ height: 30px; line-height: 30px; border: 1px solid #ccc; width: 100%; }
.new-express-add{ display: inline-block; line-height: 1em;padding:6px 10px; background: green; color:#fff; border-radius:4px; }
</style>

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<table cellpadding="3" cellspacing="0" style="width:600px;">
	<thead>
    	<tr>
            <th width="100">ID</th>
            <th>账户类型</th>
            <th width="100">-</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list' data-id="<?php  echo $v['id'];?>">
        	<td><?php  echo $v['id'];?></td>
        	<td><input type="text" class="express-name" value="<?php echo $v['zhanghuclass'];?>"></td>
            <td>
				<a class="del" style="cursor:default;"><i class="fa fa-close"></i><span>删除</span></a>
			</td>
        </tr>

	<?php }?>
		<tr>
        	<td>账户类型</td>
        	<td><input type="text" class="new-express-name" placeholder="输入新的账户类型名称"></td>
        	<td><a class="submit new-express-add">添加</a></td>
        </tr>
    </tbody>
</table>
<p style="padding:10px; font-size:13px;">修改时输入新的内容鼠标点击页面空白区域即可完成保存！</p>



</body>
</html>