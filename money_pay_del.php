<?php

require_once(dirname(__FILE__).'/include/common.php');
if (strpos($_SESSION['eptime_flag'], 'paydfig') === false) {LYG::ShowMsg('您没有权限！');} 

if(empty($_REQUEST['id'])){ lyg::showmsg('参数错误'); }

$arr = explode(",", $_REQUEST['id']);

$ids = array();
foreach($arr as $a){
	$b = intval($a);
	if(!in_array($b,$ids)){
		$ids[] = $b;
	}
}
if(count($ids)<1){
	lyg::showmsg('参数错误');
}

$ids = implode(" or id=",$ids);

$rowscount = $con->Excute("delete from #__money_pay where id={$ids}");
LYG::writeLog("[".$_SESSION['eptime_username']."]删除 {$rowscount} 条应收应付记录");
LYG::ShowMsg("成功删除 {$rowscount} 条记录","money_pay_list.php");
