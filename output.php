<?php

set_time_limit(0);
if(!isset($data)){ exit(); }
$totalcount = count($data);

$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);


//$objPHPExcel->getActiveSheet()->getStyle('C')->getNumberFormat()->setFormatCode("@");
	

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'ID')
			->setCellValue('B1', '单号')
			->setCellValue('C1', '时间')
			->setCellValue('D1', '[类型]大类->小类')
			->setCellValue('E1', '流入')
			->setCellValue('F1', '流出')
			->setCellValue('G1', '所属帐户')
			->setCellValue('H1', ''.$webconfig['system_wanglai'].'')
			->setCellValue('I1', ''.$webconfig['system_yuangong'].'')
			->setCellValue('J1', ''.$webconfig['system_xiangmu'].'')
			->setCellValue('K1', '备注')
			->setCellValue('L1', '审核状态/附件')
			->setCellValue('M1', '记录时间');
$index = 2;
$nowprice1=0;
$nowprice2=0;

foreach($data as $k=>$v){
	$t = intval($v['addtime'])<1?'':date('Y-m-d H:i:s',$v['addtime']);
	$s = $c_status["{$v['status']}"][0];
	$g = sprintf('%.2f',floatval($v['price'])*intval($v['shuliang']));
$nowprice11=0;
$nowprice22=0;
if($v['type']==0){$nowprice1=$nowprice1+$v['price'];$nowprice11=$v['price'];}
if($v['type']==1){$nowprice2=$nowprice2+$v['price'];$nowprice22=$v['price'];}	
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$index, $v['id'])
		->setCellValue('B'.$index, $v['moneyID'])
		->setCellValue('C'.$index, $v['selldate'])
		->setCellValue('D'.$index, $c_type["{$v['type']}"].'->'.c_bigclassname($v['id_bigclass']).'->'.c_smallclassname($v['id_smallclass']))
		->setCellValue('E'.$index, $nowprice11)
		->setCellValue('F'.$index, $nowprice22)
		->setCellValue('G'.$index, $v['name'])
		->setCellValue('H'.$index, c_classname("wanglai",$v['wanglai']))
		->setCellValue('I'.$index, c_classname("yuangong",$v['yuangong']))
		->setCellValue('J'.$index, c_classname("xiangmu",$v['xiangmu']))
		->setCellValue('K'.$index, $v['beizhu'])
		->setCellValue('L'.$index, $v['shenhe'])
		->setCellValue('M'.$index, $v['addtime']);
	$index++;
}
  $index1 = $index+1;
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$index, '合计')
		->setCellValue('B'.$index, '')
		->setCellValue('C'.$index, '')
		->setCellValue('D'.$index, '')
		->setCellValue('E'.$index, $nowprice1)
		->setCellValue('F'.$index, $nowprice2)
		->setCellValue('G'.$index, '')
		->setCellValue('H'.$index, '')
		->setCellValue('I'.$index, '')
		->setCellValue('J'.$index, '')
		->setCellValue('K'.$index, '')
		->setCellValue('L'.$index, '')
		->setCellValue('M'.$index, '');


$objPHPExcel->getActiveSheet()->setTitle('数据');

$objPHPExcel->setActiveSheetIndex(0);
ob_end_clean();
header("Pragma: public");
header("Expires: 0");
header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
header("Content-Type:application/force-download");
header("Content-Type:application/vnd.ms-execl");
header("Content-Type:application/octet-stream");
header("Content-Type:application/download");;
header('Content-Disposition:attachment;filename="E'.date("Ymd",time()).rand(10,99).'.xls"');
header("Content-Transfer-Encoding:binary");
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');