﻿<?php
require_once(dirname(__FILE__).'/include/common.php');
$webconfig = lyg::readArr("web");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>左侧</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
</head>
	<style>
		*{ margin: 0; padding: 0; overflow:hidden;}
		img{border:0;}
        ul,li{list-style-type:none;}
		a {color:#00007F;text-decoration:none;}
		a:hover {color:#bd0a01;text-decoration:underline;}
		.treebox{ width: 200px; margin: 0 auto; background-color:#F4F4F4; }
		.menu{ overflow: hidden; border-color: #ddd; border-style: solid ; border-width: 0 1px 1px ; }
		/*第一层*/
		.menu li.level1>a{ 
			display:block;
			height: 40px;
			line-height: 40px;
			color: #8A8A8A;
			padding-left: 50px;
			border-bottom: 1px solid #FFF; 
			font-size: 15px;
			position: relative;
		 }
		 .menu li.level1 a:hover{ text-decoration: none;background-color:#00ADC4;color:#FFF;   }
		 .menu li.level1 a.current{ background: #E0E0E2; }

		/*============修饰图标*/
		 .ico{ width: 20px; height: 20px; display:block;   position: absolute; left: 20px; top: 10px; background-repeat: no-repeat; background-image: url(style/images/img/ico1.png); }

		 /*============小箭头*/
		 .level1 i{ width: 20px; height: 10px; background-image:url(style/images/img/arrow.png); background-repeat: no-repeat; display: block; position: absolute; right: 20px; top: 15px; }
		.level1 i.down{ background-position: 0 -10px; }

		 .ico1{ background-position: 0 0; }
		 .ico2{ background-position: 0 -20px; }
		 .ico3{ background-position: 0 -40px; }
		 .ico4{ background-position: 0 -60px; }
         .ico5{ background-position: 0 -80px; }

		 /*第二层*/
		 .menu li ul{ overflow: hidden; }
		 .menu li ul.level2{ display: none;background: #E0E0E2;  }
		 .menu li ul.level2 li a{
		 	display: block;
			height: 40px;
			line-height: 40px;
			color: #8A8A8A;
			text-indent: 50px;
			/*border-bottom: 1px solid #ddd; */
			font-size: 14px;
		 }

	</style>
<body>

	<div class="treebox">
		<ul class="menu">
			<li class="level1">
				<a href="#none"><em class="ico ico5"></em>日常记账<i class="down"></i></a>
				<ul class="level2">
					<li><a href="money_add.php" target="right">收支记账</a></li>
					<li><a href="money_list.php" target="right">收支流水</a></li>
					<li><a href="money_pay_add.php" target="right">应收应付记账</a></li>
					<li><a href="money_pay_list.php" target="right">应收应付流水</a></li>
					<li><a href="money_pay_list.php?type=0" target="right">借出收款</a></li>
					<li><a href="money_pay_list.php?type=1" target="right">借入归还</a></li>
				</ul>
			</li>
			<li class="level1">
				<a href="#none"><em class="ico ico2"></em>资金管理<i></i></a>
				<ul class="level2">
					<li><a href="money_pay_list_ok.php?type=0" target="right">已收款</a></li>
					<li><a href="money_pay_list_ok.php?type=1" target="right">已付款</a></li>
					<li><a href="money_zhang.php" target="right">资金账户管理</a></li>
					<li><a href="money_zhang_list.php" target="right">转账流水</a></li>
					
				</ul>
			</li>


<?php if ($_SESSION['eptime_adminPower']==0) {?>
			<li class="level1">
				<a href="#none"><em class="ico ico1"></em>系统设置<i></i></a>
				<ul class="level2">
<li><a href="zhanghu_list.php" target="right">账户管理</a></li>
<li><a href="big_list.php" target="right">一级分类管理</a></li>	
<li><a href="small_list.php" target="right">二级分类管理</a></li>
<li><a href="class_list.php?class=wanglai" target="right"><?php echo $webconfig['system_wanglai'];?>管理</a></li>
<li><a href="class_list.php?class=yuangong" target="right"><?php echo $webconfig['system_yuangong'];?>管理</a></li>
<li><a href="class_list.php?class=xiangmu" target="right"><?php echo $webconfig['system_xiangmu'];?>管理</a></li>
<li><a href="user_list.php" target="right">用户管理</a></li>
<li><a href="config_system_base.php" target="right">基础设置</a></li>	
<li><a href="config_system_backrestore.php" target="right">数据库维护</a></li>
<li><a href="log_list.php" target="right">操作日志</a></li>
				</ul>
			</li>

          
<?php }?>
		</ul>
	</div>
	<!-- 引入 jQuery -->
<script src="style/js/jquery1.8.3.min.js" type="text/javascript"></script>
<script src="style/js/easing.js"></script>
<script>
//等待dom元素加载完毕.
	$(function(){
		$(".treebox .level1>a").click(function(){
			$(this).addClass('current')   //给当前元素添加"current"样式
			.find('i').addClass('down')   //小箭头向下样式
			.parent().next().slideDown('slow','easeOutQuad')  //下一个元素显示
			.parent().siblings().children('a').removeClass('current')//父元素的兄弟元素的子元素去除"current"样式
			.find('i').removeClass('down').parent().next().slideUp('slow','easeOutQuad');//隐藏
			 return false; //阻止默认时间
		});
	})
</script>
</body>
</html>
