<?php

if(!file_exists("install/install_lock.txt")){
	header("location:install/");
}

session_start();
require_once(dirname(__FILE__)."/include/init.php");
require_once(dirname(__FILE__)."/include/mysql.php");
require_once(dirname(__FILE__)."/include/function.php");



$webconfig = lyg::readArr("web");
if(!empty($_POST)){
	
	$post = $_POST;
	if(empty($post['username']) || empty($post['userpwd'])){
		LYG::ShowMsg('请输入账号密码');
	}
	if($webconfig['login_code_open']){
		if(empty($_SESSION['eptime_login_code'])){
			LYG::ShowMsg('非法操作');
		}
		if(empty($post['code'])){
			LYG::ShowMsg('请输入验证码');
		}
		if(strtolower($post['code']) != $_SESSION['eptime_login_code']){
			LYG::ShowMsg('验证码错误');
		}
		$_SESSION['eptime_login_code'] = null;
		unset($_SESSION['eptime_login_code']);
	}
	
	$username	=$post['username'];
	$userpwd	=c_adminpwd($post['userpwd']);
	$data	=$con->find('select * from #__admin where Working=1 and username=? and password=?',array($username,$userpwd));

	if(!empty($data)){
		$_SESSION['eptime_id']=$data['ID'];
		$_SESSION['eptime_username']=$data['UserName'];
		$_SESSION['eptime_mobile']=$data['mobile'];
		$_SESSION['eptime_adminPower']=$data['AdminPower'];
		$_SESSION['eptime_l_zhanghu']=$data['zhanghu'];
		$_SESSION['eptime_l_xiangmu']=$data['xiangmu'];
		$_SESSION['eptime_l_wanglai']=$data['wanglai'];
		$_SESSION['eptime_l_yuangong']=$data['yuangong'];
		$_SESSION['eptime_flag']=$data['flag'];
		$_SESSION['eptime_img']=$data['img'];
		LYG::writeLog("[".$_SESSION['eptime_username']."]登陆后台成功！");
		lyg::jump("admin.php"); 
        
	}else{
		LYG::ShowMsg('账号密码错误');
	}
	exit();
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $webconfig['system_title'];?></title>
<link href="style/css/login.css" type="text/css" rel="stylesheet" />
<style type="text/css">
<?php if($webconfig['login_code_open']){
	echo ".loginbox{ height:364px;}";
}?>
body{
background:url(<?php echo $webconfig['index_pic'];?>) no-repeat center top; background-size:cover; position:relative;
}
</style>
</head>

<body>

	<div class="loginbox">
		<div class="logintitle">					
		<span>账号登录</span>
		<?php if($webconfig['login_sms_open']){echo "<span><a href=index1.php>快捷登录</a></span>";}?>
        </div>
		<div class="logintitle-min"></div>
		<div class="loginbody">
			<form method="post">
				<div class="oneline">
					<input type="text" name="username" autocomplete="off" placeholder="请输入账号" value="">
				</div>
				<div class="oneline">
					<input type="password" name="userpwd" placeholder="请输入密码" value="">
				</div>
				<?php
				if($webconfig['login_code_open']){
				?>
				<div class="oneline">
					<div class="code-text"><input type="text" name="code" autocomplete="off" placeholder="请输入验证码"></div>
					<div class="code-img"><img src="code/" id="code-img" title="点击更换验证码"></div>
				</div>
				<?php } ?>
				<div class="loginbtn">
				 <input type="submit" value="登录">
				</div>
			</form>		
		</div>
	</div>
</body>
<script type="text/javascript">
window.onload = function(){
	document.getElementById("code-img").onclick = function(){
		this.src = "code/?r="+Math.random();
	}
}
</script>
</html>