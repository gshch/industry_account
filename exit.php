<?php
session_start();
require_once(dirname(__FILE__)."/include/init.php");
require_once(dirname(__FILE__)."/include/mysql.php");
require_once(dirname(__FILE__)."/include/function.php");

LYG::writeLog("[".$_SESSION['eptime_username']."]退出后台成功！");

if(isset($_SESSION['eptime_id'])){
	$_SESSION['eptime_id'] = null;
	unset($_SESSION['eptime_id']);
}
if(isset($_SESSION['eptime_username'])){
	$_SESSION['eptime_username'] = null;
	unset($_SESSION['eptime_username']);
}
if(isset($_SESSION['eptime_mobile'])){
	$_SESSION['eptime_mobile'] = null;
	unset($_SESSION['eptime_mobile']);
}
if(isset($_SESSION['eptime_adminPower'])){
	$_SESSION['eptime_adminPower'] = null;
	unset($_SESSION['eptime_adminPower']);
}
if(isset($_SESSION['eptime_l_zhanghu'])){
	$_SESSION['eptime_l_zhanghu'] = null;
	unset($_SESSION['eptime_l_zhanghu']);
}
if(isset($_SESSION['eptime_l_xiangmu'])){
	$_SESSION['eptime_l_xiangmu'] = null;
	unset($_SESSION['eptime_l_xiangmu']);
}
if(isset($_SESSION['eptime_l_wanglai'])){
	$_SESSION['eptime_l_wanglai'] = null;
	unset($_SESSION['eptime_l_wanglai']);
}
if(isset($_SESSION['eptime_l_yuangong'])){
	$_SESSION['eptime_l_yuangong'] = null;
	unset($_SESSION['eptime_l_yuangong']);
}
if(isset($_SESSION['eptime_flag'])){
	$_SESSION['eptime_flag'] = null;
	unset($_SESSION['eptime_flag']);
}
if(isset($_SESSION['eptime_img'])){
	$_SESSION['eptime_img'] = null;
	unset($_SESSION['eptime_img']);
}
echo "<script type='text/javascript'>window.open('./','_top');</script>";
exit();