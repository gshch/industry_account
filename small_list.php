<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 
$_k = array();
$_v = array();

$_c = array();//分页条件
$_s = array();//搜索条件

if(!empty($_GET['id_bigclass']) && intval($_GET['id_bigclass'])>0){
    $_k[]="#__money_smallclass.id_bigclass=?";
    $_v[]=intval($_GET['id_bigclass']);
    $_c[]="id_bigclass=".intval($_GET['id_bigclass']);
    $_s['id_bigclass'] = intval($_GET['id_bigclass']);
}

$_k = implode(' and ',$_k);
if($_k!=''){
    $_k = " where ".$_k;
}



$pagesize = 20;

//总记录数
$datacount=$con->RowsCount("select count(*) from #__money_smallclass {$_k}",$_v);
//总页数
$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(isset($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;
//查询数据
$sql = "select * from #__money_smallclass {$_k} order by  id desc limit $start_id,$pagesize";
$data = $con->select($sql,$_v);

//得到分页HTML
$fenye=LYG::getPageHtml($page,$datacount,$pagesize);


$classes = $con->select("select * from #__money_bigclass");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>二级分类管理</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
function search(obj){
	document.searchform.submit();
}
$(function(){
	$("input.sort").blur(function(){
		
		var sort = parseInt($(this).val());
		if(isNaN(sort)){
			sort = 100;
		}
		if(sort == parseInt(this.defaultValue)){
			return;
		}
		
		var id = $(this).parent().parent().attr("data-id");
		
		$.post("json.php",{"act":"zhanghusort","id":id,"sort":sort},function(e){
			location.reload();
		},"json");
	});
});
</script>
</head>

<body class="content">


<div class="searchform">
    <form method="get" name="searchform">
	<table>
		<tr>
			<td width="80" align="left">类型</td>
			<td width="200">
			<select name="id_bigclass" class="select bai" onchange="search(this);">
				<option value='0'>不限</option>
			<?php
			foreach ($classes as $k => $v) {
				if(array_key_exists('id_bigclass', $_s) && intval($_s['id_bigclass'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>[{$c_type[$v['type']]}]{$v['bigclass']}</option>";
				}else{
					echo "<option value='{$v['id']}'>[{$c_type[$v['type']]}]{$v['bigclass']}</option>";    
				}                    
			}
			?></select>



			</td>
			<td width="*"></td>
		</tr>
	</table>
    </form>
</div>

<div class="list-menu">
	<ul>
		<li><a href="small_add.php">添加</a></li>
	</ul>
</div>


<table cellpadding="3" cellspacing="0">
	<thead>
    	<tr>
            <th>ID</th>
			<th>类型/一级分类名称</th>
            <th>二级分类名称</th>
            <th>说明</th>
            <th>-</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list' data-id="<?php echo $v['id'];?>">
        	<td align="center"><?php echo $v['id'];?></td>
<td align="center">[<?php echo $c_type[c_bigclasstype($v['id_bigclass'])];?>]<?php echo c_bigclassname($v['id_bigclass']);?></td>
<td align="center"><?php echo $v['smallclass'];?></td>
<td align="center"><font color="#ff0000"><?php if(c_bigclassisok($v['id_bigclass'])==1){ echo $c_isok[c_bigclassisok($v['id_bigclass'])];}?></font></td>
            <td align="center">
				<a class="edit" href="small_edit.php?id=<?php echo $v['id'];?>"><i class="fa fa-pencil-square-o"></i><span>编辑</span></a>
				<a onclick="return confirm('确定删除吗？');" class="del" href="small_del.php?id=<?php echo $v['id'];?>"><i class="fa fa-close"></i><span>删除</span></a>
			</td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="5" style="padding-left:30px;">
			<?php echo $fenye ;?>
			</td>
        </tr>
    </tfoot>
</table>


</body>
</html>