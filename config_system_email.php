<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 
$email = lyg::readArr("mail");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>授权信息</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
</head>


<script type='text/javascript' src='js/jquery.min.js'></script>
<script type='text/javascript'>
var _type = "<?php echo $email['type'];?>";
$(function(){
	$("input[name='type']").click(function(){
		var v = $("input[name='type']:checked").val();
		if(v!=_type){
			$.post("json.php",{"act":"setemailtype","type":v},function(e){
				location.reload();
			},"Json");
		}
	});
	$("#saveaction").click(function(){
		 if(_type=='server'){
			var server = {
				"server_mail":$("#server_mail").val(),
				"server_key":$("#server_key").val(),
				"server_url":$("#server_url").val(),
				"server_ver":$("#server_ver").val(),
				
			}
			server.act="setservermail";
			$.post("json.php",server,function(e){
				alert(e.info);
			},"Json");
		}
	});
});
</script>
<body class="content">


<?php
if($email['type']=='server'){
?>

<table cellpadding="3" cellspacing="0" class="table-add">
	<tr>
		<td align="right" width='100'>　</td>
		<td align="left" width='*'>授权信息</td>
	</tr>




	<tr>
		<td align="right">授权邮箱：</td>
		<td align="left">
			<input class='inp' type='text' id='server_mail' placeholder="如：xxxxx@qq.com" value="<?php echo $email['server_mail'];?>"/>
		</td>
	</tr>
	<tr>
		<td align="right">授权码：</td>
		<td align="left">
			<input class='inp' type='password' id='server_key' placeholder="授权码key" value="<?php echo $email['server_key'];?>"/>
		</td>
	</tr>
	
	<tr>
		<td align="right">授权域名：</td>
		<td align="left">
			<input class='inp' type='text' id='server_url' placeholder="授权域名" value="<?php echo $email['server_url'];?>"/>
		   不加http://
		</td>
	</tr>
	<tr>
		<td align="right">版本号：</td>
		<td align="left">
			<input class='inp' type='text' id='server_ver' placeholder="授权版本号" value="<?php echo $email['server_ver'];?>" readonly/>
			不允许修改
		</td>
	</tr>

	<tr>
		<td align="right" height='50'>　</td>
		<td align="left">
			<input class='sub' type='button' id='saveaction' value='保存'/>
		</td>
	</tr>

</table>
<?php } ?>

</body>
</html>