<?php

require_once(dirname(__FILE__).'/include/common.php');
if (strpos($_SESSION['eptime_flag'], 'pay2fig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money_pay where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}

if(!empty($_POST)){
	
	extract($_POST);

$moneyID=trim($moneyID);
$price=trim($price);
$selldate=trim($selldate);
$price0=trim($price0);
    $zhanghu = intval($zhanghu);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$beizhu= trim($beizhu);
	$img = trim($img);
$LastLogin = $_SESSION['eptime_username'];
$LastTime = date("Y-m-d H:i:s",time());

	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}
	if($price0-$price<0){
		LYG::ShowMsg('收/还款金额不能大于原金额');
	}

$beizhu1= $beizhu." 收/还款 ￥".$price."元 时间[".$selldate."]；";
$price1=$price0-$price;
    $data = array(
        'price'		=>$price1,
		'beizhu'	=>$beizhu1,
    );
	$result	= $con->savemin("money_pay",$data,"id={$dataid}");
	if($result!==false){
LYG::writeLog("[".$_SESSION['eptime_username']."]收/还款单号[".$moneyID."]");

$beizhu="[收/还款".$moneyID."] ￥".$price."元";
$moneyID1 = c_newOrderNo("D");
$id_login = $_SESSION['eptime_id'];
$login = $_SESSION['eptime_username'];
$addtime = date("Y-m-d H:i:s",time());
if($check_enable){
	$isok = 1;
		}
		else{$isok = 0;}

	$data = array(
		'type'		=>$type,
  'id_bigclass'		=>$id_bigclass,
 'id_smallclass'	=>$id_smallclass,
        'price'		=>$price,
        'selldate'	=>$selldate,
		'zhanghu'	=>$zhanghu,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'moneyID'	=>$moneyID1,
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);

$aok = $con->add("money",$data);
if ($type==0){$eok = $con->Update("update #__zhanghu set amount=amount+{$price} where id={$zhanghu}");}
elseif($type==1){$eok = $con->Update("update #__zhanghu set amount=amount-{$price} where id={$zhanghu}");}
LYG::writeLog("[".$_SESSION['eptime_username']."]收还款成功，同时产生[".$moneyID."]");
		LYG::ShowMsg('收还款成功','money_pay_list.php');
	}else{
		LYG::ShowMsg('流水没有更改');
	}
	
	die();
	
}


	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
label{margin-right:10px;}
</style>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
		<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
		<script src="kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>


<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
<input type='hidden' name='id' value="<?php echo $dataid;?>" />
<table cellpadding="3" cellspacing="0" class="table-add">
		<tr>
			<td align="right" height='36'></td>
			<td>
<?php if($info['type']==0){ echo "收款";}elseif($info['type']==1){ echo "还款";}?>
			</td>
		</tr>
		<input type='hidden' name='type' value="<?php echo $info['type'];?>" />
		<input type='hidden' name='moneyID' value="<?php echo $info['moneyID'];?>" />
		<input type="hidden" name="id_bigclass" value="<?php echo $info['id_bigclass'];?>">
		 <input type="hidden" name="id_smallclass" value="<?php echo $info['id_smallclass'];?>">
		 <input type="hidden" name="price0" value="<?php echo $info['price'];?>">
		<tr>
			<td align="right" height='36'>类型：</td>
			<td>
			<?php echo c_bigclassname($info['id_bigclass']);?>-><?php echo c_smallclassname($info['id_smallclass']);?>
			
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>金额：</td>
			<td>
			<input class='inp' type='number' name='price' placeholder="0.00" step="0.01" value="<?php echo $info['price'];?>"/>
			<span>数字，请保留小数点后两位</span>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>时间：</td>
			<td>
			
			<input type='text' class='inp' name='selldate' value='<?php echo date("Y-m-d",time()) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
			<span>点击选择</span>
			</td>
		</tr>


		<tr>
			<td align="right" height='36'>资金账户：</td>
			<td align="left" width='*'>
				<select name="zhanghu" class="select">
				<?php
				foreach(c_classinfo("zhanghu") as $k=>$v){
        		if(intval($info['zhanghu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>

		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_wanglai'];?>：</td>
			<td align="left" width='*'>
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
        		if(intval($info['wanglai'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_yuangong'];?>：</td>
			<td align="left" width='*'>
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
        		if(intval($info['yuangong'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_xiangmu'];?>：</td>
			<td align="left" width='*'>
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
        		if(intval($info['xiangmu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>

	<tr>
		<td align="right" height='36'>备注：</td>
		<td align="left"><input type='text' class='inp' name='beizhu' placeholder='' value="<?php echo $info['beizhu'];?>"/></td>
	</tr>


	<tr>
    	<td align="right" height='50'>　</td>
        <td align="left">
        	<input class='sub' type='submit' value='提交'/>　<input class='reset' type='reset' value='重置'></td>
    </tr>


    
</table>
</form>

</body>
</html>