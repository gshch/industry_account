<?php

require_once(dirname(__FILE__).'/include/common.php');
//是否启用审核
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


$_k = array();
$_v = array();

$_c = array();//分页条件
$_s = array();//搜索条件

if(isset($_GET['px']) && trim($_GET['px'])<>''){
    $_c[]="px=".intval($_GET['px']);
    $_s['px'] = intval($_GET['px']);
}

if(isset($_GET['type']) && trim($_GET['type'])<>''){
    $_k[]="#__money.type=?";
    $_v[]=intval($_GET['type']);
    $_c[]="type=".intval($_GET['type']);
    $_s['type'] = intval($_GET['type']);
}

if(!empty($_GET['id_bigclass']) && intval($_GET['id_bigclass'])>0){
    $_k[]="#__money.id_bigclass=?";
    $_v[]=intval($_GET['id_bigclass']);
    $_c[]="id_bigclass=".intval($_GET['id_bigclass']);
    $_s['id_bigclass'] = intval($_GET['id_bigclass']);
}

if(!empty($_GET['id_smallclass']) && intval($_GET['id_smallclass'])>0){
    $_k[]="#__money.id_smallclass=?";
    $_v[]=intval($_GET['id_smallclass']);
    $_c[]="id_smallclass=".intval($_GET['id_smallclass']);
    $_s['id_smallclass'] = intval($_GET['id_smallclass']);
}

if(!empty($_GET['zhanghu']) && intval($_GET['zhanghu'])>0){
    $_k[]="#__money.zhanghu=?";
    $_v[]=intval($_GET['zhanghu']);
    $_c[]="zhanghu=".intval($_GET['zhanghu']);
    $_s['zhanghu'] = intval($_GET['zhanghu']);
}
if(!empty($_GET['wanglai']) && intval($_GET['wanglai'])>0){
    $_k[]="#__money.wanglai=?";
    $_v[]=intval($_GET['wanglai']);
    $_c[]="wanglai=".intval($_GET['wanglai']);
    $_s['wanglai'] = intval($_GET['wanglai']);
}

if(!empty($_GET['yuangong']) && intval($_GET['yuangong'])>0){
    $_k[]="#__money.yuangong=?";
    $_v[]=intval($_GET['yuangong']);
    $_c[]="yuangong=".intval($_GET['yuangong']);
    $_s['yuangong'] = intval($_GET['yuangong']);
}
if(!empty($_GET['xiangmu']) && intval($_GET['xiangmu'])>0){
    $_k[]="#__money.xiangmu=?";
    $_v[]=intval($_GET['xiangmu']);
    $_c[]="xiangmu=".intval($_GET['xiangmu']);
    $_s['xiangmu'] = intval($_GET['xiangmu']);
}

if(!empty($_GET['id_login']) && intval($_GET['id_login'])>0){
    $_k[]="#__money.id_login=?";
    $_v[]=intval($_GET['id_login']);
    $_c[]="id_login=".intval($_GET['id_login']);
    $_s['id_login'] = intval($_GET['id_login']);
}

if($_SESSION['eptime_adminPower']==2){

    $_k[]="#__money.id_login=?";
    $_v[]=intval($_SESSION['eptime_id']);
    $_c[]="id_login=".intval($_SESSION['eptime_id']);
    $_s['id_login'] = intval($_SESSION['eptime_id']);

	}
else{

if(!empty($_GET['id_login']) && intval($_GET['id_login'])>0){
    $_k[]="#__money.id_login=?";
    $_v[]=intval($_GET['id_login']);
    $_c[]="id_login=".intval($_GET['id_login']);
    $_s['id_login'] = intval($_GET['id_login']);
}
	}
if(isset($_GET['isok']) && trim($_GET['isok'])<>''){
    $_k[]="#__money.isok=?";
    $_v[]=intval($_GET['isok']);
    $_c[]="isok=".intval($_GET['isok']);
    $_s['isok'] = intval($_GET['isok']);
}

if(!empty($_GET['keywords']) && trim($_GET['keywords'])!=''){
    $_k[]="(#__money.beizhu like '%".trim($_GET['keywords'])."%' or #__money.moneyID like '%".trim($_GET['keywords'])."%' )";
	$_v[]=trim($_GET['keywords']);
    $_c[]="keywords=".trim($_GET['keywords']);
    $_s['keywords'] = trim($_GET['keywords']);
}

if(!empty($_GET['time0']) && !empty($_GET['time1']) && 
    preg_match("/^\d{4}\-\d{2}\-\d{2}$/",$_GET['time0']) && preg_match("/^\d{4}\-\d{2}\-\d{2}$/",$_GET['time1'])
){
    $time0 = $_GET['time0'];
    $time1 = $_GET['time1'];
    $t0 = $time0." 00:00:00";
    $t1 = $time1." 23:59:59";
    if($t0!==false && $t1!==false && $t1>$t0){

        $_k[]="#__money.selldate>=?";
        $_v[]=$t0;
        $_c[]="time0=$time0";
        $_s['time0'] = $time0;

        $_k[]="#__money.selldate<=?";
        $_v[]=$t1;
        $_c[]="time1=$time1";
        $_s['time1'] = $time1;
    }
}

$_k = implode(' and ',$_k);
if($_k!=''){
    $_k = " where ".$_k;
}

$field = array(
	"#__money.*",
	"#__zhanghu.name",
	"#__admin.Realname"
);
$field = implode(",",$field);
$left = "left join #__zhanghu on #__zhanghu.id=#__money.zhanghu ";
$left.= "left join #__admin on #__admin.ID=#__money.id_login ";

$action="search";
if(!empty($_GET['action']) && $_GET['action']=='output'){
	require_once(dirname(__FILE__).'/include/PHPExcel/PHPExcel.php');
	$sql = "select {$field} from #__money {$left} {$_k} order by #__money.id desc,#__money.isok desc";
	$data = $con->select($sql,$_v);
	require_once("output.php");
	die();
}
$sqla = "select {$field} from #__money {$left} {$_k} order by #__money.id desc,#__money.isok desc";
$dataa = $con->select($sqla,$_v);
$nowprice1=0;
$nowprice2=0;
foreach($dataa as $k=>$v){
if($v['type']==0){$nowprice1=$nowprice1+$v['price'];}
if($v['type']==1){$nowprice2=$nowprice2+$v['price'];}
                        }

if($webconfig['eptime_pagesize']){$pagesize=$webconfig['eptime_pagesize'];}else{$pagesize = 20;}

$datacount=$con->RowsCount("select count(#__money.id) from #__money {$left} {$_k}",$_v);

$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(!empty($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;


if(!empty($_GET['px']) && intval($_GET['px'])>0){
	if($_GET['px']==1){$sql = "select {$field} from #__money {$left} {$_k} order by #__money.type asc,#__money.price desc limit {$start_id},{$pagesize}";}
    if($_GET['px']==2){$sql = "select {$field} from #__money {$left} {$_k} order by #__money.type asc,#__money.price asc limit {$start_id},{$pagesize}";}
	if($_GET['px']==3){$sql = "select {$field} from #__money {$left} {$_k} order by #__money.type desc,#__money.price desc limit {$start_id},{$pagesize}";}
	if($_GET['px']==4){$sql = "select {$field} from #__money {$left} {$_k} order by #__money.type desc,#__money.price asc limit {$start_id},{$pagesize}";}
	if($_GET['px']==5){$sql = "select {$field} from #__money {$left} {$_k} order by #__money.selldate desc limit {$start_id},{$pagesize}";}
	if($_GET['px']==6){$sql = "select {$field} from #__money {$left} {$_k} order by #__money.selldate asc limit {$start_id},{$pagesize}";}
}else{
$sql = "select {$field} from #__money {$left} {$_k} order by #__money.selldate desc,#__money.isok desc limit {$start_id},{$pagesize}";
}

$data =$con->select($sql,$_v);

$fenye = LYG::getPageHtml($page,$datacount,$pagesize,$_c);



$bigclassinfo = array();
if(array_key_exists('type',$_s)){
    $bigclassinfo = $con->select("select * from #__money_bigclass where type = ?",array($_s['type']));
}

$smallclassinfo = array();
if(array_key_exists('id_bigclass',$_s)){
    $smallclassinfo = $con->select("select * from #__money_smallclass where id_bigclass = ?",array($_s['id_bigclass']));
}

$admininfo = $con->select("select * from #__admin");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>收支流水</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(function(){
    $("#type").change(function(){
        var id = parseInt($(this).val());

            $.ajax({
                type:'get',
                url:'json.php?act=getbigclass&type='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有一级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].bigclass+"</option>";
                    }
                    $("#id_bigclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });

    $("#id_bigclass").change(function(){
        var id = parseInt($(this).val());
        if(id==''){
            $("#id_smallclass").html("<option value=''>所有二级分类</option>");
        }else{
            $.ajax({
                type:'get',
                url:'json.php?act=getsmallclass&id_bigclass='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有二级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].smallclass+"</option>";
                    }
                    $("#id_smallclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });
        }
    });

	//全选
	document.getElementById("ids").onclick = function(){
		var items = document.getElementsByName("id");
		for(var i=0; i<items.length; i++){
			items[i].checked = this.checked;
		}
	}
	//批量删除
	document.getElementById("delselected").onclick = function(){
		var items = document.getElementsByName("id");
		var ids = [];
		for(var i=0; i<items.length; i++){
			if(items[i].checked){
				ids.push(items[i].value);
			}
		}
		if(ids.length<1){
			return;
		}
		if(!confirm("确定删除选中的 "+(ids.length)+" 条记录吗？")){
			return;
		}
		location.href = "money_del.php?id="+ids.join(",");
	}
	
});
function setmethod(tag){
	$("#actionmethod").val(tag);
	if(tag=="output"){
		$("#searchform").attr("target","_blank");
	}else{
		$("#searchform").removeAttr("target");
	}
}
</script>
<SCRIPT language=javascript> 
function openScript(url, width, height) {
        var Win = window.open(url,"openScript",'width=' + width + ',height=' + 
 
height + ',resizable=0,scrollbars=no,menubar=no,status=no' );
}
</SCRIPT>
<script type="text/javascript">
function setEnable(obj,id){
    $.post("json.php",{"act":"setmoneyenable","id":id,"enable":obj.checked?"0":"1"},function(e){
        location.reload();
    },"Json");
}
</script>
</head>

<body class="content">
<div class="searchform">
    <form method="get" id="searchform" target="_self">
    <input type="hidden" id="actionmethod" name="action" value="search">
	<input type="hidden" id="actionmethod" name="px" value="<?php 
					if(array_key_exists("px",$_s)){
						echo $_s['px'];
					}
				?>">
	<table>
		<tr>
		<td width="*" align="right"></td>
			<td width="100">
			<select name="type" class="select bai" id="type">
<option value=''>所有类型</option>
			<?php 
			foreach($c_type as $k=>$v){
					if(array_key_exists('type', $_s) && intval($_s['type'])===intval($k)){
						echo "<option value='{$k}' selected='selected'>{$v}</option>";
					}else{
						echo "<option value='{$k}'>{$v}</option>";
					}
			}?>
			</select>			
			
			</td>

			<td width="100">
				<select name="id_bigclass" class="select bai" id="id_bigclass">
					<option value=''>所有一级分类</option><?php
					foreach($bigclassinfo as $k=>$v){
						if(array_key_exists('id_bigclass', $_s) && intval($_s['id_bigclass'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['bigclass']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['bigclass']}</option>";    
						}
					}
					?>
				</select>
			</td>

			<td width="100">
				<select name="id_smallclass" class="select bai" id="id_smallclass">
					<option value=''>所有二级分类</option><?php
					foreach($smallclassinfo as $k=>$v){
						if(array_key_exists('id_smallclass', $_s) && intval($_s['id_smallclass'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['smallclass']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['smallclass']}</option>";    
						}
					}
					?>
				</select>
			</td>
			<td width="100">
			<select name="zhanghu" class="select bai" id="zhanghu"><option value='0'>所有账户</option><?php
			foreach (c_classinfo("zhanghu") as $k => $v) {
				if(array_key_exists('zhanghu', $_s) && intval($_s['zhanghu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
				}else{
					echo "<option value='{$v['id']}'>{$v['name']}</option>";    
				}                    
			}
			?></select></td>


			<td width="100">
				<select name="wanglai" class="select bai" id="wanglai">
					<option value='0'>所有<?php echo $webconfig['system_wanglai'];?></option><?php
					foreach(c_classinfo("wanglai") as $k=>$v){
						if(array_key_exists('wanglai', $_s) && intval($_s['wanglai'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
				</select>
			</td>

			<td width="100">
				<select name="yuangong" class="select bai" id="yuangong">
					<option value='0'>所有<?php echo $webconfig['system_yuangong'];?></option><?php
					foreach(c_classinfo("yuangong") as $k=>$v){
						if(array_key_exists('yuangong', $_s) && intval($_s['yuangong'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
				</select>
			</td>

			<td width="100">
				<select name="xiangmu" class="select bai" id="xiangmu">
					<option value='0'>所有<?php echo $webconfig['system_xiangmu'];?></option><?php
					foreach(c_classinfo("xiangmu") as $k=>$v){
						if(array_key_exists('xiangmu', $_s) && intval($_s['xiangmu'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
				</select>
			</td>
			<td width="100">
			<select name="id_login" class="select bai" id="id_login"><option value='0'>所有记账人</option><?php
			foreach ($admininfo as $k => $v) {
				if(array_key_exists('id_login', $_s) && intval($_s['id_login'])===intval($v['ID'])){
					echo "<option value='{$v['ID']}' selected='selected'>{$v['Realname']}</option>";
				}else{
					echo "<option value='{$v['ID']}'>{$v['Realname']}</option>";    
				}                    
			}
			?></select></td>
			<?php
			if($check_enable){
			?>
			<td width="100">
				<select name="isok" class="select bai" id="isok">
					<option value=''>全部状态</option>
					<option value='0' <?php if(array_key_exists('isok', $_s) && intval($_s['isok'])===0){echo "selected=selected";}?>>已审</option>
					<option value='1' <?php if(array_key_exists('isok', $_s) && intval($_s['isok'])===1){echo "selected=selected";}?>>未审</option>
				</select>
			</td>
			<?php } ?>
		</tr>
</table>
<table>
		<tr>
			
			<td width="*" align="right"></td>
			<td width="80" align="right">日期:</td>
			<td width="100">
				<input type="text" name="time0" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php 
					if(array_key_exists("time0",$_s)){
						echo $_s['time0'];
					}
				?>">
			</td>
			<td width="20" align="center">至</td>
			<td width="100">
				<input type="text" name="time1" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php 
					if(array_key_exists("time1",$_s)){
						echo $_s['time1'];
					}
				?>">
			</td>
			
			<td width="60" align="right">关键词:</td>
			<td width="180">
				<input type="text" name="keywords" class="text" placeholder='单号或者备注' value="<?php 
					if(array_key_exists("keywords",$_s)){
						echo $_s['keywords'];
					}
				?>">
			</td>


			<td width="80" align="right">
<input type="submit" onclick="setmethod('search');" value="查询" class="sub">
			</td>
			<td width="80" align="right">
<input type="submit" onclick="setmethod('output');" value="导出" class="reset" title="根据当前查询条件导出">
			</td>
		</tr>

	</table>
    </form>
		<table>
		<tr>
		<td width="*" align="right"></td>
			<td width="80%" align="right">
			盈亏：<b><?php echo round($nowprice1-$nowprice2,2);?></b> 元，流入合计：<b><?php echo $nowprice1;?></b> 元，流出合计：<b><?php echo $nowprice2;?></b> 元&nbsp;
			</td>
			</tr>
	</table>
</div>

<?php
function replace_var($url,$string,$new_value)
{
        while(substr($url,0,1)=="&")
        {
                $url=substr($url,1);
        }
        if($url!="")
        {
                $url_array=explode("&",$url);
                $new_url=$_SERVER['PHP_SELF']."?";
                $string_len=strlen($string)+1;
                $i=0;
                while($url_array[$i])
                {
                         if(substr($url_array[$i],0,$string_len)==$string."=")
                         {
                               $url_array[$i]=$string."=".$new_value;
                        }
                        if($i>0) $url_array[$i]="&".$url_array[$i];
                        $new_url=$new_url.$url_array[$i];
                        $i++;
                }
        }
        else $new_url=$_SERVER['PHP_SELF'];
        return $new_url;
}
?>

<table class="table-list">
	<thead>
    	<tr>
            <th></th>
			<th>单号</th>
            <th>时间
<?php if($_GET['px']=='' && $_GET['action']==''){?><a href="?px=5"><img src="style/images/px.gif"><?php }?>
<?php if($_GET['px']=='' && $_GET['action']=='search' ){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",5);?>"><img src="style/images/px.gif"><?php }?>
<?php if($_GET['px']=='5'){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",6);?>"><img src="style/images/px1.gif"><?php }?>
<?php if($_GET['px']=='6'){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",5);?>"><img src="style/images/px1.gif"><?php }?>
			</a></th>
            <th>[类型]大类->小类</th>
            <th>流入
<?php if($_GET['px']=='' && $_GET['action']==''){?><a href="?px=1"><img src="style/images/px.gif"><?php }?>
<?php if($_GET['px']=='' && $_GET['action']=='search'){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",1);?>"><img src="style/images/px.gif"><?php }?>
<?php if($_GET['px']==1){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",2);?>"><img src="style/images/px1.gif"><?php }?>
<?php if($_GET['px']==2){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",1);?>"><img src="style/images/px1.gif"><?php }?>			
			</a></th>
            <th>流出
<?php if($_GET['px']=='' && $_GET['action']==''){?><a href="?px=3"><img src="style/images/px.gif"><?php }?>
<?php if($_GET['px']=='' && $_GET['action']=='search' ){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",3);?>"><img src="style/images/px.gif"><?php }?>
<?php if($_GET['px']==3){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",4);?>"><img src="style/images/px1.gif"><?php }?>
<?php if($_GET['px']==4){?><a href="<?php echo replace_var($_SERVER['QUERY_STRING'],"px",3);?>"><img src="style/images/px1.gif"><?php }?>			
			</a></th>
            <th>所属帐户</th>
                        <th>备注</th>
            <th>记账人</th>
			<th>
 
            <th>详情</th>
            <th>-</th>
			<th>-</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list'>
        	<td>
<?php if ($check_enable && $v['isok']==0){echo "";}else{echo "<input type='checkbox' name='id' value='{$v['id']}'>";}?><span></span>
			</td>
        	<td><?php echo $v['moneyID'];?></td>
		
			<td style='width:200px;'><?php echo substr($v['selldate'],0,10);?></td>
<td>[<?php echo $c_type["{$v['type']}"];?>]<?php echo c_bigclassname($v['id_bigclass']);?>-><?php echo c_smallclassname($v['id_smallclass']);?></td>
            <td style='color:red;'><?php if ($v['type']==0){echo $v['price'];}?></td>
			<td style='color:green;'><?php if ($v['type']==1){echo $v['price'];}?></td>
            <td><?php echo $v['name'];?></td>

            <td><div  title="<?php echo $v['beizhu'];?>" style="width:300px;text-overflow:ellipsis; "><?php echo $v['beizhu'];?></div></td>
			<td><?php echo c_adminname($v['id_login']);?></td>
<td>
<?php if($check_enable){ 
	if ($_SESSION['eptime_adminPower']==2){?>
<?php echo $v['isok']=="0"?" 已审":"未审";?>
   <?php }else{ ?>
<input type="checkbox" class="enable" <?php echo "onclick=\"setEnable(this,{$v['id']});\"";if ($_SESSION['eptime_adminPower']==0){ echo $v['isok']=="0"?" checked='checked'":"";}?>/>
<?php echo $v['isok']=="0"?" 已审":"未审";?>
<?php }
	} ?>
	
<?php 
//附件显示开始
if ($v['img']!=""){
$count = count(explode(',', $v['img']));
$kk=0;
foreach( explode(',', $v['img']) as $k1=>$v1)
{$kk=$kk+1;
if($count<>$kk){echo '<a href="'.$v1.'" target="blank">'.$kk.'</a> ';}
}		
			}
//附件显示结束			
			?>	
</td>
            <td><a href='javascript:openScript("money_show.php?id=<?php echo $v['id'];?>",500,640)'>详情</a></td>
            <td class="ctr">
				<a class="edit" href="money_edit.php?id=<?php echo $v['id'];?>">
<?php if (c_bigclassisok($v['id_bigclass'])==100000){echo "";}else{echo "<i class=fa fa-pencil-square-o></i><span>编辑</span>";}?>
				</a>
</td>
<td>
<a onclick="return confirm('删除后不可恢复，确定删除吗？');" class="del" href="<?php if($webconfig['modi_sms_open']){echo "yz_del.php?id={$v['id']}&yz=m";}else{echo "money_del.php?id={$v['id']}";}?>">
<?php if ($check_enable && $v['isok']==0){echo "";}else{echo "<i class=fa fa-close></i><span>删除</span>";}?></a>				
			</td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
    		<?php
    		if($check_enable){
    			echo "<td colspan='17' style='padding-left:3px;'>";
    		}else{
    			echo "<td colspan='16' style='padding-left:3px;'>";
    		}
			echo $fenye;
			?>
			</td>
        </tr>
    </tfoot>
    
</table>




</body>
</html>
