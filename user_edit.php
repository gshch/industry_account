<?php

require_once(dirname(__FILE__).'/include/common.php');
$webconfig = lyg::readArr("web");

if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$data_id = intval($_REQUEST['id']);
$info=  $con->find("select * from #__admin where ID=$data_id");
if(empty($info)){lyg::showmsg('参数错误');}


if(!empty($_POST)){
	//参数校验
	extract($_POST);

	$zhanghu = intval($zhanghu);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$AdminPower = intval($AdminPower);

	if(empty($UserName) || trim($UserName)==''){
		LYG::ShowMsg('用户名不能为空');
	}
	$UserName= trim($UserName);
	$Realname= trim($Realname);

$mobile= trim($mobile);
if(empty($mobile) || !preg_match('/^1[3-9]\d{9}$/',$mobile)){LYG::ShowMsg('手机号码格式错误');}

	$img = trim($img);
	$flag = implode('|',$flag);
	

	$ex = $con->rowscount("select count(*) from #__admin where UserName=? and ID<>?",array(
		$UserName,$data_id
	));
	if($ex>0){
		lyg::showmsg("同名用户已存在");
	}

	$ex1 = $con->rowscount("select count(*) from #__admin where mobile=? and ID<>?",array(
		$mobile,$data_id
	));
	if($ex1>0){
		lyg::showmsg("手机号码已存在");
	}

	
	$data = array(
		$UserName,
		$zhanghu,
		$wanglai,
		$yuangong,
		$xiangmu,
		$Realname,
		$mobile,
		$img,
		$flag,
		$AdminPower,
		$data_id,
	);
	
	$eok = $con->Update("update #__admin set UserName=?,zhanghu=?,wanglai=?,yuangong=?,xiangmu=?,Realname=?,mobile=?,img=?,flag=?,AdminPower=? where ID=? limit 1",$data);

	if($eok!==false){
		LYG::ShowMsg('操作成功','user_list.php');
	}else{
		LYG::ShowMsg('操作失败，请重试');
	}
	
	die();
}

	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑用户</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
		<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
		<script src="kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<input type='hidden' name='id' value='<?php echo $data_id;?>'>
	<table cellpadding="3" cellspacing="0" class="table-add">


		<tr>
			<td align="right" width='100' height='36'>用户名：</td>
			<td align="left" width='*'>
				<input type='text' class='inp' name='UserName' placeholder='' value='<?php echo $info['UserName'];?>'/>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>真实姓名：</td>
			<td>
				<input type="text" name="Realname" class="inp" placeholder="" value='<?php echo $info['Realname'];?>'/>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>手机号码：</td>
			<td>
				<input type="text" name="mobile" id="dianhua" class="inp" placeholder="" value='<?php echo $info['mobile'];?>'/>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>头像图片：</td>
			<td>
				<input type="text" name="img" class="inp" placeholder="" value='<?php echo $info['img'];?>' id="url3"> <input type="button" id="image3" value="选择图片" /> 建议为正方形png图

			</td>
		</tr>

		<tr>
			<td align="right" height='36'>默认资金账户：</td>
			<td align="left" width='*'>
				<select name="zhanghu" class="select">
				<?php
				foreach(c_classinfo("zhanghu") as $k=>$v){
					if(intval($v['id'])===intval($info['zhanghu'])){
						echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
					}else{
						echo "<option value='{$v['id']}'>{$v['name']}</option>";
					}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>默认<?php echo $webconfig['system_wanglai'];?>：</td>
			<td align="left" width='*'>
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
					if(intval($v['id'])===intval($info['wanglai'])){
						echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
					}else{
						echo "<option value='{$v['id']}'>{$v['name']}</option>";
					}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>默认<?php echo $webconfig['system_yuangong'];?>：</td>
			<td align="left" width='*'>
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
					if(intval($v['id'])===intval($info['yuangong'])){
						echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
					}else{
						echo "<option value='{$v['id']}'>{$v['name']}</option>";
					}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>默认<?php echo $webconfig['system_xiangmu'];?>：</td>
			<td align="left" width='*'>
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
					if(intval($v['id'])===intval($info['xiangmu'])){
						echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
					}else{
						echo "<option value='{$v['id']}'>{$v['name']}</option>";
					}
				}
				?>
				</select>
			</td>
		</tr>

		<tr>
			<td align="right" height='36'>用户级别：</td>
			<td>
			<select id="target" name="AdminPower" class="select">
			<?php 
			foreach($c_adminpower as $k=>$v){
					if(intval($k)===intval($info['AdminPower'])){
						echo "<option value='{$k}' selected='selected'>{$v}</option>";
					}else{
						echo "<option value='{$k}'>{$v}</option>";
					}
			}?>
			</select>

			</td>
		</tr>

		<tr>
			<td align="right" height='36'>权限：</td>
			<td>

			<?php 
			$i = 1;
			foreach($c_flag as $k=>$v){
				if($i%4==0){
if (strpos(trim($info['flag']), trim($k)) === false){echo "<input type='checkbox' name='flag[]' value='{$k}' >{$v}&nbsp;&nbsp;&nbsp;&nbsp;<br><br>";}
else{echo "<input type='checkbox' name='flag[]' value='{$k}' checked>{$v}&nbsp;&nbsp;&nbsp;&nbsp;<br><br>";}				
				
				}
                else{
if (strpos(trim($info['flag']), trim($k)) === false){echo "<input type='checkbox' name='flag[]' value='{$k}' >{$v}&nbsp;&nbsp;&nbsp;&nbsp;";}
else{echo "<input type='checkbox' name='flag[]' value='{$k}' checked>{$v}&nbsp;&nbsp;&nbsp;&nbsp;";}
				
				}
				$i++;
			}?>


			</td>
		</tr>

		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='edit' type='submit' value='修改'/></td>
		</tr>

	</table>
</form>

</body>
</html>