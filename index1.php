<?php

if(!file_exists("install/install_lock.txt")){
	header("location:install/");
}

session_start();
require_once(dirname(__FILE__)."/include/init.php");
require_once(dirname(__FILE__)."/include/mysql.php");
require_once(dirname(__FILE__)."/include/function.php");
$webconfig = lyg::readArr("web");


if($_GET['a']<>""){
$moblie=$_GET['phonum'];
		if(empty($moblie)){
			LYG::ShowMsg('请输入手机号');
		}
$send_code=random(6,1);
$_SESSION['eptime_login_code']=$send_code;
        $updatehosturl = $webconfig['sms_api'].'?eptime='.$webconfig['sms_eptimecode'].'&mobile='.$moblie.'&sendcode='.$send_code;
        $updatenowinfo = file_get_contents($updatehosturl);
}


if(!empty($_POST)){
	
	$post = $_POST;

		if(empty($post['phonum'])){
			LYG::ShowMsg('请输入手机号');
		}
		if(empty($_SESSION['eptime_login_code'])){
			LYG::ShowMsg('非法操作');
		}
		if(empty($post['code'])){
			LYG::ShowMsg('请输入验证码');
		}
		if(strtolower($post['code']) != $_SESSION['eptime_login_code']){
			LYG::ShowMsg('验证码错误');
		}
		$_SESSION['eptime_login_code'] = null;
		unset($_SESSION['eptime_login_code']);

	
	$mobile	=$post['phonum'];
	$data	=$con->find("select * from #__admin where Working=1 and mobile=$mobile");

	if(!empty($data)){
		$_SESSION['eptime_id']=$data['ID'];
		$_SESSION['eptime_username']=$data['UserName'];
		$_SESSION['eptime_mobile']=$data['mobile'];
		$_SESSION['eptime_adminPower']=$data['AdminPower'];
		$_SESSION['eptime_l_zhanghu']=$data['zhanghu'];
		$_SESSION['eptime_l_xiangmu']=$data['xiangmu'];
		$_SESSION['eptime_l_wanglai']=$data['wanglai'];
		$_SESSION['eptime_l_yuangong']=$data['yuangong'];
		$_SESSION['eptime_flag']=$data['flag'];
		$_SESSION['eptime_img']=$data['img'];
		LYG::writeLog("[".$_SESSION['eptime_username']."]登陆后台成功！");
		lyg::jump("admin.php"); 
        
	}else{
		LYG::ShowMsg('无此账号');
	}
	exit();
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $webconfig['system_title'];?></title>
<link href="style/css/login.css" type="text/css" rel="stylesheet" />
<style type="text/css">
body{
background:url(<?php echo $webconfig['index_pic'];?>) no-repeat center top; background-size:cover; position:relative;
}
</style>
<script src="js/jquery.min.js"></script>
</head>

<body>
<script>
    time =60;  //短信倒计时时间
    function send(obj) {
        var timer =  setInterval(function () {
            time--;
            $(obj).text(time+'秒之后重发');
            $(obj).removeClass('btn-info');
            $(obj).addClass('btn-success')
            $(obj).prop('disabled', true);
 
            if(time==0){
 
                //清除定时器
                clearInterval(timer)
                $(obj).removeClass('btn-success');
                $(obj).addClass('btn-info');
                $(obj).prop('disabled', false);
 
                $(obj).text('获取验证码');
                time=60;
 
            }
        },1000);
        //获取手机号
        var phonum = $('#phonum').val();
            $.post('?a=sendSms&phonum='+phonum,{phonum:phonum},function () {
            },'json')
    }
</script>
	<div class="loginbox">
		<div class="logintitle">					
		<span>快捷登录</span>
		<span><a href="index.php">账号登录</a></span>
        </div>
		<div class="logintitle-min"></div>
		<div class="loginbody">
			<form method="post">
				<div class="oneline">
					<input type="text" name="phonum"  id="phonum" autocomplete="off" placeholder="请输入手机号" value="">
				</div>
				<div class="oneline">
					<div class="code-text"><input type="text" name="code" autocomplete="off" placeholder="请输入手机验证码"></div>
					<div class="code-img"><button type="button" onclick="send(this)">获取验证码</button></div>
				</div>
				<div class="loginbtn">
				 <input type="submit" value="登录">
				</div>
			</form>		
		</div>
	</div>
</body>
</html>