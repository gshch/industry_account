<?php

return array (
  'system_title' => '企业记账系统',
  'system_wanglai' => '往来人',
  'system_yuangong' => '经手人',
  'system_xiangmu' => '分公司',
  'order_interval' => 604800,
  'login_code_open' => '0',
  'system_check' => '0',
  'backrestore_ext' => '.sql',
  'login_sms_open' => '0',
  'sms_api' => 'admin',
  'sms_eptimecode' => 'admin',
  'modi_sms_open' => '0',
  'eptime_pagesize' => '20',
  'sms_eptimename' => 'admin',
  'mobile_pic' => 'images/1.jpg',
  'index_pic' => 'style/images/login-bg.jpg',
  'wx_appid' => 'admin',
  'wx_secret' => 'admin',
);