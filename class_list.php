<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 

$class=trim($_GET['class']);
if(empty($c_class["{$class}"])){
	LYG::ShowMsg('参数错误');
}

$_c[]="class=".trim($_GET['class']);

if($webconfig['eptime_pagesize']){$pagesize=$webconfig['eptime_pagesize'];}else{$pagesize = 20;}


//总记录数
$datacount=$con->RowsCount("select count(*) from #__{$class}");
//总页数
$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(isset($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;
//查询数据
$sql='select * from #__'.$class.' order by id desc limit '.$start_id.','.$pagesize;
$data	=$con->select($sql,'');
//得到分页HTML
$fenye=LYG::getPageHtml($page,$datacount,$pagesize,$_c);
if(trim($_GET['shenhe'])=="ok"){$eok = $con->Update("update #__".$class." set isok=1 where id={$_GET['id']}");echo "<script>location.href='class_list.php?class={$class}';</script>";}
if(trim($_GET['shenhe'])=="kk"){$eok = $con->Update("update #__".$class." set isok=0 where id={$_GET['id']}");echo "<script>location.href='class_list.php?class={$class}';</script>";}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $c_class["{$class}"];?>管理</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body class="content">


<div class="list-menu no-pdt">
	<ul>
		<li><a href="class_add.php?class=<?php echo $class;?>">添加<?php echo $c_class["{$class}"];?></a></li>
	</ul>
</div>

<table cellpadding="3" cellspacing="0">
	<thead>
    	<tr>
            <th>ID</th>
            <th><?php echo $c_class["{$class}"];?>名称</th>
<?php if ($class=="wanglai"){?>
<th>电话</th>
<th>传真</th>
<th>Email</th>
<th>地址</th>
<?php }?>

<?php if ($class=="yuangong"){?>
<th>电话</th>
<th>生日</th>
<th>部门</th>
<?php }?>
			<th>备注</th>
			<th>状态</th>
            <th>-</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list'>
        	<td align="center"><?php  echo $v['id'];?></td>
        	<td align="center"><?php echo $v['name'];?></td>
<?php if ($class=="wanglai"){?>
<th align="center"><?php echo $v['tel'];?></th>
<th align="center"><?php echo $v['fax'];?></th>
<th align="center"><?php echo $v['email'];?></th>
<th align="center"><?php echo $v['address'];?></th>
<?php }?>

<?php if ($class=="yuangong"){?>
<th align="center"><?php echo $v['tel'];?></th>
<th align="center"><?php echo substr($v['shengri'],0,10);?></th>
<th align="center"><?php echo $v['address'];?></th>
<?php }?>
			<td align="center"><?php echo $v['beizhu'];?></td>
			<td align="center"><?php if($v['isok']==1){echo "<a href=?shenhe=kk&class=".$class."&id={$v['id']}><font color=red>正常</font></a>";}else{echo "<a href=?shenhe=ok&class=".$class."&id={$v['id']}><font color=grey>已锁定</a></a>";}?></td>
            <td align="center">
				<a class="edit" href="class_edit.php?id=<?php echo $v['id'];?>&class=<?php echo $class;?>"><i class="fa fa-pencil-square-o"></i><span>编辑</span></a>
				<a onclick="return confirm('确定删除吗？');" class="del" href="class_del.php?id=<?php echo $v['id'];?>&class=<?php echo $class;?>"><i class="fa fa-close"></i><span>删除</span></a>
			</td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="10" style="padding-left:30px;">
			<?php echo $fenye ;?>
			</td>
        </tr>
    </tfoot>
</table>


</body>
</html>