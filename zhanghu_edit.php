<?php

require_once(dirname(__FILE__).'/include/common.php');


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$data_id = intval($_REQUEST['id']);
$info=  $con->find("select * from #__zhanghu where id=$data_id");
if(empty($info)){lyg::showmsg('参数错误');}





if(!empty($_POST)){
	//参数校验
	extract($_POST);

	$type = intval($type);
	$beizhu = trim($beizhu);

	if(empty($name) || trim($name)==''){
		LYG::ShowMsg('账户名不能为空');
	}
	$name= trim($name);
	
	$ex = $con->rowscount("select count(*) from #__zhanghu where type=? and name=? and id<>?",array(
		$type,$name,$data_id
	));
	if($ex>0){
		lyg::showmsg("同名账户已存在");
	}
	
	$data = array(
		$name,
		$type,
		floatval($amount),
		$beizhu,
		$data_id,
	);
	
	$eok = $con->Update("update #__zhanghu set name=?,type=?,amount=?,beizhu=? where id=? limit 1",$data);

	if($eok!==false){
		LYG::ShowMsg('操作成功','zhanghu_list.php');
	}else{
		LYG::ShowMsg('操作失败，请重试');
	}
	
	die();
}

$classinfo = $con->select("select * from #__zhanghu_class");
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑账户</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<input type='hidden' name='id' value='<?php echo $data_id;?>'>
	<table cellpadding="3" cellspacing="0" class="table-add">

		<tr>
			<td align="right" width='100' height='36'>账户名称：</td>
			<td align="left" width='*'>
				<input type='text' class='inp' name='name' value='<?php echo $info['name'];?>'/>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>账户类型：</td>
			<td align="left" width='*'>
				<select name="type" class="select">
				<?php
				foreach($classinfo as $k=>$v){
					if(intval($v['id'])===intval($info['type'])){
						echo "<option value='{$v['id']}' selected='selected'>{$v['zhanghuclass']}</option>";
					}else{
						echo "<option value='{$v['id']}'>{$v['zhanghuclass']}</option>";
					}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>当前余额：</td>
			<td>
				<input type="text" name="amount" class="inp" placeholder="0.00" value="<?php echo floatval($info['amount'])?>">
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>备注：</td>
			<td>
				<input type="text" name="beizhu" class="inp" placeholder="" value="<?php echo $info['beizhu']?>">
			</td>
		</tr>
		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='edit' type='submit' value='修改'/></td>
		</tr>

	</table>
</form>

</body>
</html>