set names 'utf8';

DROP TABLE IF EXISTS `eptime_admin`;
CREATE TABLE `eptime_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `AdminPower` int(11) DEFAULT '0',
  `Working` int(11) DEFAULT '0',
  `LastLoginTime` datetime DEFAULT NULL,
  `LastLoginIP` varchar(255) DEFAULT NULL,
  `LoginTimes` int(11) DEFAULT '0',
  `AddDate` datetime DEFAULT NULL,
  `M_Random` varchar(255) DEFAULT NULL,
  `Realname` varchar(255) DEFAULT NULL,
  `zhanghu` int(11) DEFAULT '0',
  `wanglai` int(11) DEFAULT '0',
  `yuangong` int(11) DEFAULT '0',
  `xiangmu` int(11) DEFAULT '0',
  `flag` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_admin`(`ID`,`UserName`,`mobile`,`Password`,`AdminPower`,`Working`,`LastLoginTime`,`LastLoginIP`,`LoginTimes`,`AddDate`,`M_Random`,`Realname`,`zhanghu`,`wanglai`,`yuangong`,`xiangmu`,`flag`,`img`,`openid`) VALUES('1','admin','','ec057d2c5440bafd028c622fb032f7df','0','1','2019-01-01 00:00:00','127.0.0.1','0','2019-01-01 00:00:00','a8edfb4efe3612449a1d04883051d9fb','系统管理员','1','1','1','1','moneyfig|moneymfig|moneydfig|payfig|paymfig|paydfig|pay1fig|pay2fig|zhangfig|zhangzfig|zhanglfig|excelfig|yearrfig|monthfig|yearfig|chengyuanfig|shouzhifig|totalfig|MaiQi','style/images/user.png','');

DROP TABLE IF EXISTS `eptime_log`;
CREATE TABLE `eptime_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `content` longtext,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `eptime_money`;
CREATE TABLE `eptime_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beizhu` varchar(255) DEFAULT NULL,
  `id_login` int(11) DEFAULT '0',
  `login` varchar(50) DEFAULT NULL,
  `selldate` datetime DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `id_bigclass` int(11) DEFAULT '0',
  `id_smallclass` int(11) DEFAULT '0',
  `price` double DEFAULT '0',
  `isok` tinyint(1) DEFAULT '0',
  `addtime` datetime DEFAULT NULL,
  `zhanghu` int(11) DEFAULT '0',
  `wanglai` int(11) DEFAULT '0',
  `zhanghu1` int(11) DEFAULT '0',
  `LastLogin` varchar(255) DEFAULT NULL,
  `LastTime` datetime DEFAULT NULL,
  `yuangong` int(11) DEFAULT '0',
  `xiangmu` int(11) DEFAULT '0',
  `moneyID` varchar(50) DEFAULT NULL,
  `shenhetime` datetime DEFAULT NULL,
  `shenhe` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `moneyID` (`moneyID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `eptime_money_bigclass`;
CREATE TABLE `eptime_money_bigclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bigclass` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `isok` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('1','销售收入','0','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('2','服务收入','0','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('3','其他收入','0','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('4','借入款项','0','1');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('5','采购支出','1','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('6','工人工资','1','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('7','其他支出','1','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('8','借出款项','1','1');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('10','账户互转','0','0');
INSERT INTO `eptime_money_bigclass`(`id`,`bigclass`,`type`,`isok`) VALUES('11','账户互转','1','0');

DROP TABLE IF EXISTS `eptime_money_pay`;
CREATE TABLE `eptime_money_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beizhu` varchar(255) DEFAULT NULL,
  `id_login` int(11) DEFAULT '0',
  `login` varchar(50) DEFAULT NULL,
  `selldate` datetime DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `id_bigclass` int(11) DEFAULT '0',
  `id_smallclass` int(11) DEFAULT '0',
  `price` double DEFAULT '0',
  `isok` tinyint(1) DEFAULT '0',
  `addtime` datetime DEFAULT NULL,
  `zhanghu` int(11) DEFAULT '0',
  `wanglai` int(11) DEFAULT '0',
  `zhanghu1` int(11) DEFAULT '0',
  `LastLogin` varchar(255) DEFAULT NULL,
  `LastTime` datetime DEFAULT NULL,
  `yuangong` int(11) DEFAULT '0',
  `xiangmu` int(11) DEFAULT '0',
  `moneyID` varchar(50) DEFAULT NULL,
  `isclass` longtext,
  `Lastdate` datetime DEFAULT NULL,
  `payid` varchar(50) DEFAULT NULL,
  `price1` double DEFAULT '0',
  `shenhe` varchar(255) DEFAULT NULL,
  `shenhetime` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `eptime_money_smallclass`;
CREATE TABLE `eptime_money_smallclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smallclass` varchar(255) DEFAULT NULL,
  `id_bigclass` int(11) DEFAULT '0',
  `isok` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('1','设备销售','1','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('2','软件销售','1','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('3','硬件维修','2','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('4','软件服务','2','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('5','其他收入','3','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('6','银行贷款','4','1');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('7','其他借入','4','1');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('8','设备采购','5','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('9','办公用品','5','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('10','业务部工资','6','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('11','财务部工资','6','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('12','其他支出','7','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('13','差旅费','8','1');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('14','其他借出','8','1');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('16','转入','10','0');
INSERT INTO `eptime_money_smallclass`(`id`,`smallclass`,`id_bigclass`,`isok`) VALUES('17','转出','11','0');

DROP TABLE IF EXISTS `eptime_wanglai`;
CREATE TABLE `eptime_wanglai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `beizhu` longtext,
  `isok` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_wanglai`(`id`,`name`,`tel`,`fax`,`email`,`address`,`addtime`,`beizhu`,`isok`) VALUES('1','无','','','','','2019-01-01 00:00:00','','1');
INSERT INTO `eptime_wanglai`(`id`,`name`,`tel`,`fax`,`email`,`address`,`addtime`,`beizhu`,`isok`) VALUES('2','先启科技','','','','','2019-01-01 00:00:00','','1');
INSERT INTO `eptime_wanglai`(`id`,`name`,`tel`,`fax`,`email`,`address`,`addtime`,`beizhu`,`isok`) VALUES('3','张小姐','03512780621','03512780621','admin@xqkj.com.cn','̫ԭ','2019-01-01 00:00:00','','1');

DROP TABLE IF EXISTS `eptime_xiangmu`;
CREATE TABLE `eptime_xiangmu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `beizhu` longtext,
  `date` datetime DEFAULT NULL,
  `isok` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_xiangmu`(`id`,`name`,`beizhu`,`date`,`isok`) VALUES('1','无','','2019-01-01 00:00:00','1');
INSERT INTO `eptime_xiangmu`(`id`,`name`,`beizhu`,`date`,`isok`) VALUES('2','太原铁路局标段','','2019-01-01 00:00:00','1');

DROP TABLE IF EXISTS `eptime_yuangong`;
CREATE TABLE `eptime_yuangong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `shengri` datetime DEFAULT NULL,
  `beizhu` longtext,
  `isok` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_yuangong`(`id`,`name`,`tel`,`fax`,`email`,`address`,`addtime`,`shengri`,`beizhu`,`isok`) VALUES('1','李胜利','13327512993','','','','2019-01-01 00:00:00','2019-01-01 00:00:00','','1');
INSERT INTO `eptime_yuangong`(`id`,`name`,`tel`,`fax`,`email`,`address`,`addtime`,`shengri`,`beizhu`,`isok`) VALUES('2','张晓龙','13327512993','','','','2019-01-01 00:00:00','2019-01-01 00:00:00','','0');
INSERT INTO `eptime_yuangong`(`id`,`name`,`tel`,`fax`,`email`,`address`,`addtime`,`shengri`,`beizhu`,`isok`) VALUES('3','管理员','','','','','2019-01-01 00:00:00','2019-01-01 00:00:00','','1');

DROP TABLE IF EXISTS `eptime_zhanghu`;
CREATE TABLE `eptime_zhanghu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `amount` double DEFAULT '0',
  `beizhu` longtext,
  `date` datetime DEFAULT NULL,
  `amount0` double DEFAULT '0',
  `isok` int(11) DEFAULT '1',
  `sy` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_zhanghu`(`id`,`name`,`type`,`amount`,`beizhu`,`date`,`amount0`,`isok`,`sy`) VALUES('1','工商银行','2','10','','2019-01-01 00:00:01','0','1','0');
INSERT INTO `eptime_zhanghu`(`id`,`name`,`type`,`amount`,`beizhu`,`date`,`amount0`,`isok`,`sy`) VALUES('2','电子钱包','1','10','','2019-01-01 00:00:01','0','1','0');

DROP TABLE IF EXISTS `eptime_zhanghu_class`;
CREATE TABLE `eptime_zhanghu_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zhanghuclass` varchar(255) DEFAULT NULL,
  `px` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `eptime_zhanghu_class`(`id`,`zhanghuclass`,`px`) VALUES('1','现金','1');
INSERT INTO `eptime_zhanghu_class`(`id`,`zhanghuclass`,`px`) VALUES('2','存款','2');
INSERT INTO `eptime_zhanghu_class`(`id`,`zhanghuclass`,`px`) VALUES('3','理财','3');
