<?php
/**
 * 验证图片
 *
 */
session_start();

$config = array(
    'font_size'   => 16,
    'img_height'  => 30,
    'img_width'   => 100,
    'font_file'   => dirname(__FILE__).'/fonts/'.mt_rand(1,4).'.ttf',
);


//创建图片，并设置背景色
$im = @imagecreate($config['img_width'], $config['img_height']);
imagecolorallocate($im, 255,255,255);

//文字随机颜色
$fontColor[]  = imagecolorallocate($im, 0x15, 0x15, 0x15);
$fontColor[]  = imagecolorallocate($im, 0x95, 0x1e, 0x04);
$fontColor[]  = imagecolorallocate($im, 0x93, 0x14, 0xa9);
$fontColor[]  = imagecolorallocate($im, 0x12, 0x81, 0x0a);
$fontColor[]  = imagecolorallocate($im, 0x06, 0x3a, 0xd5);

//获取随机字符
$rndstring  = '';
$chars='ABCDEFGHIGKLMNPQRSTUVWXYZ123456789';
$rndstring='';
$length = rand(3,4);
$max = strlen($chars) - 1;
for($i = 0; $i < $length; $i++) {
	$rndstring .= $chars[mt_rand(0, $max)];
}

$rndcodelen = strlen($rndstring);

$_SESSION['eptime_login_code'] = strtolower($rndstring);

/*
//背景横线
$lineColor1 = imagecolorallocate($im, 0xda, 0xd9, 0xd1);
for($j=3; $j<=$config['img_height']-3; $j=$j+3){
	imageline($im, 2, $j, $config['img_width'] - 2, $j, $lineColor1);
}
//背景竖线
$lineColor2 = imagecolorallocate($im, 0xda,0xd9,0xd1);
for($j=2;$j<100;$j=$j+6){
	imageline($im, $j, 0, $j+8, $config['img_height'], $lineColor2);
}
*/
//输出文字
for($i=0;$i<$rndcodelen;$i++){
	$bc = mt_rand(0, 1);
	$c_fontColor = $fontColor[mt_rand(0,4)];
	$y_pos = $i==0 ? mt_rand(8,15) : $i*($config['font_size']+10);
	$c = mt_rand(0, 15);

	@imagettftext($im, $config['font_size'], $c, $y_pos, mt_rand(16,22), $c_fontColor, $config['font_file'], $rndstring[$i]);
}


//imagefilter ( $im, IMG_FILTER_NEGATE);

header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header("Expires:0\r\n");

if(function_exists("imagejpeg")){
	header("content-type:image/jpeg\r\n");
	imagejpeg($im);
}else{
	header("content-type:image/png\r\n");
	imagepng($im);
}
imagedestroy($im);
exit();
?>
