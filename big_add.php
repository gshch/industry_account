<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 
if(!empty($_POST)){
	//参数校验
	extract($_POST);

	$type = intval($type);
	
	if(empty($bigclass) || trim($bigclass)==''){
		LYG::ShowMsg('一级分类名不能为空');
	}
	$bigclass= trim($bigclass);

	if(empty($isok) || trim($isok)==''){
		$isok = 0;
	}
	else{$isok = 1;}

	$isok = intval($isok);
	$ex = $con->rowscount("select count(*) from #__money_bigclass where type=? and bigclass=?",array(
		$type,$bigclass
	));
	if($ex>0){
		lyg::showmsg("同名一级分类已存在");
	}
	
	$data = array(
		'bigclass'		=>$bigclass,
		'type'	=>$type,
		'isok'		=>$isok
	);
	
	$aok = $con->add("money_bigclass",$data);

	if($aok){
		LYG::ShowMsg('添加成功','big_list.php?type='.$type.'');
	}else{
		LYG::ShowMsg('添加失败，请重试');
	}
	
	die();
}


	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加一级分类</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<table cellpadding="3" cellspacing="0" class="table-add">
		<tr>
			<td align="right" height='36'>类型：</td>
			<td align="left" width='*'>
				<select name="type" class="select">
			<?php 
			foreach($c_type as $k=>$v){
				echo "<option value='{$k}'>{$v}</option>";
			}?>
			</select>
			</td>
		</tr>
		<tr>
			<td align="right" width='100' height='36'>一级分类名称：</td>
			<td align="left" width='*'>
				<input type='text' class='inp' name='bigclass' placeholder=''/>
			</td>
		</tr>

		<tr>
			<td align="right" height='36'>备注：</td>
			<td>
				<input type='checkbox' name='isok'> <font color="#ff0000"><?php echo $c_isok["0"];?> <?php echo $c_isok["1"];?></font>
				
			</td>
		</tr>
		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='sub' type='submit' value='添加'/></td>
		</tr>

	</table>
</form>

</body>
</html>