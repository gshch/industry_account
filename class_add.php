<?php
require_once(dirname(__FILE__).'/include/common.php');

$class=trim($_GET['class']);
if(empty($c_class["{$class}"])){
	LYG::ShowMsg('参数错误');
}

if(!empty($_POST)){
	//参数校验
	extract($_POST);

	if(empty($name) || trim($name)==''){
		LYG::ShowMsg('名称不能为空');
	}
	$name= trim($name);
    $beizhu= trim($beizhu);
    
	$ex = $con->rowscount("select count(*) from #__{$class} where name=?",array($name));
	if($ex>0){
		lyg::showmsg("已存在");
	}
	
if ($class=="xiangmu"){
		$data = array(
		'name'	=>$name,
		'beizhu'	=>$beizhu
	);
}
if ($class=="wanglai"){
		$data = array(
		'name'	=>$name,
		'beizhu'	=>$beizhu,
		'tel'	=>$tel,
		'fax'	=>$fax,
		'email'	=>$email,
		'address'	=>$address
	);
}
if ($class=="yuangong"){
		$data = array(
		'name'	=>$name,
		'beizhu'	=>$beizhu,
		'tel'	=>$tel,
		'address'	=>$address,
		'shengri'	=>$shengri
	);
}

	


	$aok = $con->add("{$class}",$data);

	if($aok){
		LYG::ShowMsg('添加成功','class_list.php?class='.$class.'');
	}else{
		LYG::ShowMsg('添加失败，请重试');
	}
	
	die();
}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加<?php echo $c_class["{$class}"];?></title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<table cellpadding="3" cellspacing="0" class="table-add">
		<tbody>
			<tr>
				<td align="right" width='100' height='36'><?php echo $c_class["{$class}"];?>名称：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='name' placeholder=''/>
				</td>
			</tr>


<?php if ($class=="wanglai"){?>
			<tr>
				<td align="right" width='100' height='36'>电话：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='tel' placeholder=''/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>传真：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='fax' placeholder=''/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>Email：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='email' placeholder=''/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>地址：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='address' placeholder=''/>
				</td>
			</tr>
<?php }?>

<?php if ($class=="yuangong"){?>
			<tr>
				<td align="right" width='100' height='36'>电话：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='tel' placeholder=''/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>生日：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='shengri' placeholder="0000-00-00" onclick="WdatePicker();" />
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>部门：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='address' placeholder=''/>
				</td>
			</tr>
<?php }?>




			<tr>
				<td align="right" width='100' height='36'>备注：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='beizhu' placeholder=''/>
				</td>
			</tr>
			<tr>
				<td align="right" height='50'>　</td>
				<td align="left"><input class='sub' type='submit' value='添加'/></td>
			</tr>
		</tbody>
	</table>
</form>

</body>
</html>