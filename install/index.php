<?php
//系统安装

//禁止重复安装
if(file_exists("install_lock.txt")){
	exit('locked');
}

$is_writable = is_writable(dirname(__FILE__)."/../") && is_writable(dirname(__FILE__));

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统安装::企业记账系统</title>
<style type="text/css">
*{ margin:0; padding:0; outline:none; box-sizing:border-box;}
body{ font-size:16px; background:#33878c; font-family:Arial,'Microsoft Yahei'; text-align:center;}
#box{ width: 600px; border: 1px solid #ffffff; margin:60px auto 0; text-align: left; border-radius: 4px;}
h1{ font-size:20px; color:#ffffff; font-weight: normal; line-height: 1em; padding: 20px; }
h2{ font-size: 16px; line-height: 1em;padding: 10px 20px; color: yellow; font-weight: normal;  border-bottom: 1px dashed #ffffff;border-top: 1px solid #ffffff; }
h5{ font-size: 18px; color: yellow }
table{ width: 100%; border: 0; color: #ffffff;}
td{ padding:8px 3px; }
input.text{ width: 100%; border: 0; background: #ffffff;line-height: 1em; padding: 6px 4px; border-radius: 3px; font-size: 16px;}
#do{ display: inline-block; color:#ffffff; background: #009e1b; line-height: 1em;padding: 10px 50px; border-radius: 4px; font-size: 14px; cursor: default; user-select: none}
#do:active{ background: #077f1c }
#msg{padding:0 20px 20px;}
#msg p{ color: #ffffff; line-height: 1.5em;}
#msg p a{ color: #c5ba33 }
.copy{ color:#999999; padding: 20px; font-size: 14px;  }
.copy a{ color: #999999 }
</style>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript">
var install_ing = false;
$(function(){
	$("#do").click(function(){
		if(install_ing){
			return false;
		}
		var data = {
			"host":$("#host").val(),
			"user":$("#user").val(),
			"pwd":$("#pwd").val(),
			"port":parseInt($("#port").val()),
			"database":$("#database").val(),
			"houtai":$("#houtai").val()
		}
		if(data['host'] == ""){
			alert('请输入数据库服务器地址');
			$("#host").focus();
			return;
		}
		if(data['user'] == ""){
			alert('请输入连接用户名');
			$("#user").focus();
			return;
		}
		if(data['pwd'] == ""){
			alert('请输入连接密码');
			$("#pwd").focus();
			return;
		}
		if(isNaN(data['port'])){
			alert('请输入正确的连接端口号');
			$("#port").focus();
			return;
		}
		if(data['database'] == ""){
			alert('请输入数据库名');
			$("#database").focus();
			return;
		}
		var reg = /^[0-9a-zA-Z_]+$/;
		if(!reg.test(data['manage'])){
			alert('请输入正确的后台目录名，由数字、字母、下划线组成');
			$("#houtai").focus();
			return;
		}
		if(!document.getElementById("agree").checked){
			alert('请同意确认需要进行安装');
			return;
		}
		install_ing = true;
		$("#msg").html("");
		appendMsg("准备安装，请不要刷新浏览器");
		appendMsg("正在校验数据库信息...");
		checkmysql(data);
	});
});
function checkmysql(data){
	var info = $.extend({},data);
	info['act'] = "checkmysql";
	$.post("install_action.php",info,function(e){
		if(e.flag=="1"){
			setTimeout(function(){
				install(data);
			},1000);
		}else{
			appendMsg(e.msg);
			install_ing = false;
		}
	},"json");
}
function install(data){
	appendMsg("数据库信息校验成功，正在导入数据...");
	var info = $.extend({},data);
	info['act'] = "install";
	$.post("install_action.php",info,function(e){
		if(e.flag == "1"){
			appendMsg("数据导入成功，正在保存配置...");
			setTimeout(function(){
				appendMsg("配置保存成功。");
				appendMsg("安装完成，登录账号：<span style='color:yellow'>admin</span>、密码：<span style='color:yellow'>admin</span>，登录后请及时修改密码！");
				appendMsg('<a href="../">点击此处登陆>>></a>');
				install_ing = false;
			},1000);
		}else{
			appendMsg(e.msg);
			install_ing = false;
		}
	},"json");
}
function appendMsg(msg){
	$("#msg").append("<p>"+msg+"</p>");
}

</script>
</head>

<body>


<div id="box">
	<h1>欢迎使用先启科技在线记账管理系统PHP版 - 系统安装</h1>
	<h2>请在下方输入正确的MySQL数据库连接信息</h2>
	<div style="padding: 10px 16px;">
		<table>
			<tr>
				<td width="180">数据库服务器地址：</td>
				<td><input type="text" class="text" id="host" placeholder="常用：localhost" value="localhost"></td>
				<td width="40" align="left" style="color:Red">*</td>
			</tr>
			<tr>
				<td>连接用户名：</td>
				<td><input type="text" class="text" id="user" placeholder="请输入正确的数据库连接用户名"></td>
				<td align="left" style="color:Red">*</td>
			</tr>
			<tr>
				<td>连接密码：</td>
				<td><input type="text" class="text" id="pwd" placeholder="请输入正确的数据库连接密码"></td>
				<td align="left" style="color:Red">*</td>
			</tr>
			<tr>
				<td>连接端口号：</td>
				<td><input type="text" class="text" id="port" placeholder="常用：3306" value="3306"></td>
				<td align="left" style="color:Red">*</td>
			</tr>
			<tr>
				<td>数据库名：</td>
				<td><input type="text" class="text" id="database" placeholder="请输入正确的数据库名称"></td>
				<td align="left" style="color:Red">*</td>
			</tr>
		</table>
	</div>
	<div style="padding: 20px; border-top: 1px dashed #ffffff; color:#ffffff;">
		<div style="padding-bottom: 20px;">
			<input type="checkbox" id="agree" style="vertical-align: middle;"><label for="agree" style="vertical-align: middle; user-select: none;color:yellow">确认需要进行安装</label>
		</div>
		<?php
		if($is_writable){
			echo <<<SEVSTUDIO
			<span id="do">执行安装</span>
SEVSTUDIO;
		}else{
			echo <<<SEVSTUDIO
			<h5>网站没有写入权限，无法进行安装，请检查。</span>
SEVSTUDIO;
		}
		?>		
	</div>

	<div id="msg">
	</div>
</div>

<div class="copy">技术支持：<a href="http://www.eptime.cn" target="_blank">环保时代网</a>，联系QQ：<a href="http://wpa.qq.com/msgrd?v=3&uin=86514465&site=qq&menu=yes" target="_blank">86514465</a></div>


</body>
</html>