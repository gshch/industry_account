--
-- 表的结构 `eptime_admin`
--

CREATE TABLE IF NOT EXISTS `eptime_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `AdminPower` int(11) DEFAULT '0',
  `Working` int(11) DEFAULT '0',
  `LastLoginTime` datetime DEFAULT NULL,
  `LastLoginIP` varchar(255) DEFAULT NULL,
  `LoginTimes` int(11) DEFAULT '0',
  `AddDate` datetime DEFAULT NULL,
  `M_Random` varchar(255) DEFAULT NULL,
  `Realname` varchar(255) DEFAULT NULL,
  `zhanghu` int(11) DEFAULT '0',
  `wanglai` int(11) DEFAULT '0',
  `yuangong` int(11) DEFAULT '0',
  `xiangmu` int(11) DEFAULT '0',
  `flag` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_admin`
--

INSERT INTO `eptime_admin` (`ID`, `UserName`, `Password`, `AdminPower`, `Working`, `LastLoginTime`, `LastLoginIP`, `LoginTimes`, `AddDate`, `M_Random`, `Realname`, `zhanghu`, `wanglai`, `yuangong`, `xiangmu`, `flag`, `img`) VALUES
(1, 'admin', 'ec057d2c5440bafd028c622fb032f7df', 0, 1, '2019-01-01 00:00:00', '127.0.0.1', 0, '2019-01-01 00:00:00', 'a8edfb4efe3612449a1d04883051d9fb', '系统管理员', 1, 1, 1, 1, 'moneyfig|moneymfig|moneydfig|payfig|paymfig|paydfig|pay1fig|pay2fig|zhangfig|zhangzfig|zhanglfig|excelfig|yearrfig|monthfig|yearfig|chengyuanfig|shouzhifig|totalfig|MaiQi', 'style/images/user.png');

-- --------------------------------------------------------

--
-- 表的结构 `eptime_log`
--

CREATE TABLE IF NOT EXISTS `eptime_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `content` longtext,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `eptime_money`
--

CREATE TABLE IF NOT EXISTS `eptime_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beizhu` varchar(255) DEFAULT NULL,
  `id_login` int(11) DEFAULT '0',
  `login` varchar(50) DEFAULT NULL,
  `selldate` datetime DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `id_bigclass` int(11) DEFAULT '0',
  `id_smallclass` int(11) DEFAULT '0',
  `price` double DEFAULT '0',
  `isok` tinyint(1) DEFAULT '0',
  `addtime` datetime DEFAULT NULL,
  `zhanghu` int(11) DEFAULT '0',
  `wanglai` int(11) DEFAULT '0',
  `zhanghu1` int(11) DEFAULT '0',
  `LastLogin` varchar(255) DEFAULT NULL,
  `LastTime` datetime DEFAULT NULL,
  `yuangong` int(11) DEFAULT '0',
  `xiangmu` int(11) DEFAULT '0',
  `moneyID` varchar(50) DEFAULT NULL,
  `shenhetime` datetime DEFAULT NULL,
  `shenhe` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `moneyID` (`moneyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `eptime_money_bigclass`
--

CREATE TABLE IF NOT EXISTS `eptime_money_bigclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bigclass` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `isok` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_money_bigclass`
--

INSERT INTO `eptime_money_bigclass` (`id`, `bigclass`, `type`, `isok`) VALUES
(1, '销售收入', 0, 0),
(2, '服务收入', 0, 0),
(3, '其他收入', 0, 0),
(4, '借入款项', 0, 1),
(5, '采购支出', 1, 0),
(6, '工人工资', 1, 0),
(7, '其他支出', 1, 0),(10,'账户互转', 0, 0),(11,'账户互转', 1, 0),
(8, '借出款项', 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `eptime_money_pay`
--

CREATE TABLE IF NOT EXISTS `eptime_money_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beizhu` varchar(255) DEFAULT NULL,
  `id_login` int(11) DEFAULT '0',
  `login` varchar(50) DEFAULT NULL,
  `selldate` datetime DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `id_bigclass` int(11) DEFAULT '0',
  `id_smallclass` int(11) DEFAULT '0',
  `price` double DEFAULT '0',
  `isok` tinyint(1) DEFAULT '0',
  `addtime` datetime DEFAULT NULL,
  `zhanghu` int(11) DEFAULT '0',
  `wanglai` int(11) DEFAULT '0',
  `zhanghu1` int(11) DEFAULT '0',
  `LastLogin` varchar(255) DEFAULT NULL,
  `LastTime` datetime DEFAULT NULL,
  `yuangong` int(11) DEFAULT '0',
  `xiangmu` int(11) DEFAULT '0',
  `moneyID` varchar(50) DEFAULT NULL,
  `isclass` longtext,
  `Lastdate` datetime DEFAULT NULL,
  `payid` varchar(50) DEFAULT NULL,
  `price1` double DEFAULT '0',
  `shenhe` varchar(255) DEFAULT NULL,
  `shenhetime` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `eptime_money_smallclass`
--

CREATE TABLE IF NOT EXISTS `eptime_money_smallclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smallclass` varchar(255) DEFAULT NULL,
  `id_bigclass` int(11) DEFAULT '0',
  `isok` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_money_smallclass`
--

INSERT INTO `eptime_money_smallclass` (`id`, `smallclass`, `id_bigclass`, `isok`) VALUES
(1, '设备销售', 1, 0),
(2, '软件销售', 1, 0),
(3, '硬件维修', 2, 0),
(4, '软件服务', 2, 0),
(5, '其他收入', 3, 0),
(6, '银行贷款', 4, 1),
(7, '其他借入', 4, 1),
(8, '设备采购', 5, 0),
(9, '办公用品', 5, 0),
(10, '业务部工资', 6, 0),
(11, '财务部工资', 6, 0),
(12, '其他支出', 7, 0),
(13, '差旅费', 8, 1),(16,'转入', 10, 0),(17,'转出', 11, 0),
(14, '其他借出', 8, 1);

-- --------------------------------------------------------

--
-- 表的结构 `eptime_wanglai`
--

CREATE TABLE IF NOT EXISTS `eptime_wanglai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `beizhu` longtext,
  `isok` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_wanglai`
--

INSERT INTO `eptime_wanglai` (`id`, `name`, `tel`, `fax`, `email`, `address`, `addtime`, `beizhu`, `isok`) VALUES
(1, '无', '', '', '', '', '2019-01-01 00:00:00', '', 1),
(2, '先启科技', '', '', '', '', '2019-01-01 00:00:00', '', 1),
(3, '张小姐', '03512780621', '03512780621', 'admin@xqkj.com.cn', '̫ԭ', '2019-01-01 00:00:00', '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `eptime_xiangmu`
--

CREATE TABLE IF NOT EXISTS `eptime_xiangmu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `beizhu` longtext,
  `date` datetime DEFAULT NULL,
  `isok` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_xiangmu`
--

INSERT INTO `eptime_xiangmu` (`id`, `name`, `beizhu`, `date`, `isok`) VALUES
(1, '无', '', '2019-01-01 00:00:00', 1),
(2, '太原铁路局标段', '', '2019-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- 表的结构 `eptime_yuangong`
--

CREATE TABLE IF NOT EXISTS `eptime_yuangong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `shengri` datetime DEFAULT NULL,
  `beizhu` longtext,
  `isok` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_yuangong`
--

INSERT INTO `eptime_yuangong` (`id`, `name`, `tel`, `fax`, `email`, `address`, `addtime`, `shengri`, `beizhu`, `isok`) VALUES
(1, '李胜利', '13327512993', NULL, '', '', '2019-01-01 00:00:00', '2019-01-01 00:00:00', '', 1),
(2, '张晓龙', '13327512993', NULL, '', '', '2019-01-01 00:00:00', '2019-01-01 00:00:00', '', 0),
(3, '无', '', NULL, '', '', '2019-01-01 00:00:00', '2019-01-01 00:00:00', '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `eptime_zhanghu`
--

CREATE TABLE IF NOT EXISTS `eptime_zhanghu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `amount` double DEFAULT '0',
  `beizhu` longtext,
  `date` datetime DEFAULT NULL,
  `amount0` double DEFAULT '0',
  `isok` int(11) DEFAULT '1',
  `sy` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_zhanghu`
--

INSERT INTO `eptime_zhanghu` (`id`, `name`, `type`, `amount`, `beizhu`, `date`, `amount0`, `isok`) VALUES
(1, '工商银行', 2, 0, '', '2019-01-01 00:00:01', 0, 1),
(2, '电子钱包', 1, 0, '', '2019-01-01 00:00:01', 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `eptime_zhanghu_class`
--

CREATE TABLE IF NOT EXISTS `eptime_zhanghu_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zhanghuclass` varchar(255) DEFAULT NULL,
  `px` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `eptime_zhanghu_class`
--

INSERT INTO `eptime_zhanghu_class` (`id`, `zhanghuclass`, `px`) VALUES
(1, '现金', 1),
(2, '存款', 2),
(3, '理财', 3);