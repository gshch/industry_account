<?php
require_once(dirname(__FILE__).'/include/common.php');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>后台首页</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.tongji1{ float: left; width: 98%; padding:20px 0 0;font-size: 14px;}
.tongji1 h2{ font-size: 15px; line-height: 1em;padding: 10px 0; border-bottom: 1px solid #ccc }
.tongji{ float: left; width: 49%; padding:20px 0 0;font-size: 14px;}
.tongji h2{ font-size: 15px; line-height: 1em;padding: 10px 0; border-bottom: 1px solid #ccc }
</style>
</head>

<body class="content">
<?php

$_week1 =strtotime(date("Y-m-d 00:00:00",strtotime("last Sunday +1 day")));
$_month1 =strtotime(date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),1,date("Y"))));
$_year1 =strtotime(date("Y-01-01 00:00:00")); 
function _total($pay,$type,$time1){
	global $con;
	if($pay==1){
		//统计收
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} ";}
	
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
	}else if($pay ==2){
		//支应收应付

    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as s2 from #__money_pay where type={$type} and UNIX_TIMESTAMP(selldate)>={$time1} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as s2 from #__money_pay where type={$type} and UNIX_TIMESTAMP(selldate)>={$time1} ";}
		
		$s2 = $con->find($sql);
		if(empty($s2['s2'])){ $s2['s2'] = 0;}
		return round($s2['s2'],2);
	}
}
?>

<div class="tongji1">
<h2>收支构成图表 <font color=blue><a href="?time1=<?php echo $_week1;?>&tt=1">本周</a></font> | <font color=blue><a href="?time1=<?php echo $_month1;?>&tt=2">本月</a></font> | <font color=blue><a href="?time1=<?php echo $_year1;?>&tt=3">本年</a></font>
</h2>
</div>


<div class="tongji">
	<table>
			<tr>
				<td>
	<iframe src="tongji/index.php?id=1&time1=<?php echo $_GET['time1'];?>&tt=<?php echo $_GET['tt'];?>" id="iframepage" name="iframepage" frameBorder=0 scrolling=no width="500" height="400" onLoad="iFrameHeight()" ></iframe> 
</td>
			</tr>
	</table>
</div>

<div class="tongji" style="float: right">
	<table>
			<tr>
				<td>
<iframe src="tongji/index.php?id=2&time1=<?php echo $_GET['time1'];?>&tt=<?php echo $_GET['tt'];?>" id="iframepage" name="iframepage" frameBorder=0 scrolling=no width="500" height="400" onLoad="iFrameHeight()" ></iframe> 
			
				
				</td>
			</tr>
	</table>
</div>


<div class="tongji">
	<h2>收支表<font color=red></font></h2>
	<table>
		<tr>
			<td><b></b></td>
			<td><b>本周</b></td>
			<td><b>本月</b></td>
			<td><b>本年</b></td>
		</tr>
			<tr>
				<td><img src="style/images/s.png"> 收入</td>
				<td><span style="color:red"><?php echo _total("1","0",$_week1);?></span></td>
				<td><span style="color:red"><?php echo _total('1','0',$_month1);?></span></td>
				<td><span style="color:red"><?php echo _total('1','0',$_year1);?></span></td>
			</tr>
			<tr>
				<td><img src="style/images/z.png"> 支出</td>
				<td><span style="color:#229D89"><?php echo _total('1','1',$_week1);?></span></td>
				<td><span style="color:#229D89"><?php echo _total('1','1',$_month1);?></span></td>
				<td><span style="color:#229D89"><?php echo _total('1','1',$_year1);?></span></td>
			</tr>
	</table>
</div>



<div class="tongji" style="float: right">
	<h2>应收应付表</h2>
	<table>
		<tr>
			<td><b></b></td>
			<td><b>本周</b></td>
			<td><b>本月</b></td>
			<td><b>本年</b></td>
		</tr>
			<tr>
				<td><img src="style/images/s.png"> 应收</td>
				<td><span style="color:red"><?php echo _total('2','0',$_week1);?></span></td>
				<td><span style="color:red"><?php echo _total('2','0',$_month1);?></span></td>
				<td><span style="color:red"><?php echo _total('2','0',$_year1);?></span></td>
			</tr>
			<tr>
				<td><img src="style/images/z.png"> 应付</td>
				<td><span style="color:#229D89"><?php echo _total('2','1',$_week1);?></span></td>
				<td><span style="color:#229D89"><?php echo _total('2','1',$_month1);?></span></td>
				<td><span style="color:#229D89"><?php echo _total('2','1',$_year1);?></span></td>
			</tr>
	</table>
</div>

<div class="tongji1"><h2>账户信息表</h2>
<?php
$sql = "select #__zhanghu.*,#__zhanghu_class.zhanghuclass from #__zhanghu left join #__zhanghu_class on #__zhanghu_class.id = #__zhanghu.type  order by  #__zhanghu.id desc";
$data = $con->select($sql,$_v);
?>

	<table>
		<tr>
			<td><b>ID</b></td>
			<td><b>类型</b></td>
			<td><b>账户</b></td>
			<td><b>期初金额</b></td>
			<td><b>当前余额</b></td>
			<td><b>盈余</b></td>
		</tr>	
	<?php $amount=0; $amount1=0;foreach($data as $k=>$v){?>
<tr>
<td><?php echo $v['id'];?></td>
<td>[<?php echo $v['zhanghuclass'];?>]</td>
<td><?php echo $v['name'];?></td>
<td><?php echo round($v['amount0'],2);?>元</td>
<td><?php echo round($v['amount'],2);?>元</td>
<td><?php echo round($v['amount']-$v['amount0'],2);?>元</td>
</tr>
	<?php $amount=$amount+$v['amount'];$amount1=$amount1+$v['amount0'];}?>
<tr><td><b>合计</b></td><td><b></b></td><td><b></b></td><td><b><?php echo round($amount1,2);?></b>元</td><td><b><?php echo round($amount,2);?></b>元</td><td><b><?php echo round($amount-$amount1,2);?></b>元</td></tr>
	</table>
</div>
</body>
</html>
