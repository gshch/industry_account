<?php

require_once(dirname(__FILE__).'/include/common.php');
if (strpos($_SESSION['eptime_flag'], 'zhanglfig') === false) {LYG::ShowMsg('您没有权限！');} 
//是否启用审核
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


$_k = array();
$_v = array();

$_c = array();//分页条件
$_s = array();//搜索条件

if(!empty($_GET['zhanghu']) && intval($_GET['zhanghu'])>0){
    $_k[]="#__money_pay.zhanghu=?";
    $_v[]=intval($_GET['zhanghu']);
    $_c[]="zhanghu=".intval($_GET['zhanghu']);
    $_s['zhanghu'] = intval($_GET['zhanghu']);
}



if(!empty($_GET['id_login']) && intval($_GET['id_login'])>0){
    $_k[]="#__money_pay.id_login=?";
    $_v[]=intval($_GET['id_login']);
    $_c[]="id_login=".intval($_GET['id_login']);
    $_s['id_login'] = intval($_GET['id_login']);
}
if(!empty($_GET['isok']) && intval($_GET['isok'])>=0){
    $_k[]="#__money_pay.isok=?";
    $_v[]=intval($_GET['isok']);
    $_c[]="isok=".intval($_GET['isok']);
    $_s['isok'] = intval($_GET['isok']);
}
if(!empty($_GET['moneyID']) && trim($_GET['moneyID'])!=''){
    $moneyID = trim($_GET['moneyID']);
    $_k[]="#__money_pay.moneyID=?";
    $_v[]=$moneyID;
    $_c[]="moneyID=$moneyID";
    $_s['moneyID'] = $moneyID;
}

if(!empty($_GET['time0']) && !empty($_GET['time1']) && 
    preg_match("/^\d{4}\-\d{2}\-\d{2}$/",$_GET['time0']) && preg_match("/^\d{4}\-\d{2}\-\d{2}$/",$_GET['time1'])
){
    $time0 = $_GET['time0'];
    $time1 = $_GET['time1'];
    $t0 = $time0." 00:00:00";
    $t1 = $time1." 23:59:59";
    if($t0!==false && $t1!==false && $t1>$t0){

        $_k[]="#__money_pay.selldate>=?";
        $_v[]=$t0;
        $_c[]="time0=$t0";
        $_s['time0'] = $time0;

        $_k[]="#__money_pay.selldate<=?";
        $_v[]=$t1;
        $_c[]="time1=$t1";
        $_s['time1'] = $time1;
    }
}

$_k = implode(' and ',$_k);
if($_k!=''){
    $_k = " where #__money_pay.type=3 and ".$_k;
}else{$_k = " where #__money_pay.type=3 ";}

$field = array(
	"#__money_pay.*",
	"#__zhanghu.name",
	"#__admin.Realname"
);
$field = implode(",",$field);
$left = "left join #__zhanghu on #__zhanghu.id=#__money_pay.zhanghu ";
$left.= "left join #__admin on #__admin.ID=#__money_pay.id_login ";

$action="search";
if(!empty($_GET['action']) && $_GET['action']=='output'){
	require_once(dirname(__FILE__).'/include/PHPExcel/PHPExcel.php');
	$sql = "select {$field} from #__money_pay {$left} {$_k} order by #__money_pay.id desc,#__money_pay.isok desc";
	$data = $con->select($sql,$_v);
	require_once("output.php");
	die();
}


$pagesize = 20;



$datacount=$con->RowsCount("select count(#__money_pay.id) from #__money_pay {$left} {$_k}",$_v);

$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(!empty($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;

$sql = "select {$field} from #__money_pay {$left} {$_k} order by #__money_pay.id desc,#__money_pay.isok desc limit {$start_id},{$pagesize}";
$data =$con->select($sql,$_v);

$fenye = LYG::getPageHtml($page,$datacount,$pagesize,$_c);



$bigclassinfo = array();
if(array_key_exists('type',$_s)){
    $bigclassinfo = $con->select("select * from #__money_bigclass where type = ?",array($_s['type']));
}

$smallclassinfo = array();
if(array_key_exists('id_bigclass',$_s)){
    $smallclassinfo = $con->select("select * from #__money_smallclass where id_bigclass = ?",array($_s['id_bigclass']));
}

$admininfo = $con->select("select * from #__admin");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>流水</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(function(){
    $("#type").change(function(){
        var id = parseInt($(this).val());

            $.ajax({
                type:'get',
                url:'json.php?act=getbigclass&type='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有一级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].bigclass+"</option>";
                    }
                    $("#id_bigclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });

    $("#id_bigclass").change(function(){
        var id = parseInt($(this).val());
        if(id==''){
            $("#id_smallclass").html("<option value=''>所有二级分类</option>");
        }else{
            $.ajax({
                type:'get',
                url:'json.php?act=getsmallclass&id_bigclass='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有二级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].smallclass+"</option>";
                    }
                    $("#id_smallclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });
        }
    });

	
});
function setmethod(tag){
	$("#actionmethod").val(tag);
	if(tag=="output"){
		$("#searchform").attr("target","_blank");
	}else{
		$("#searchform").removeAttr("target");
	}
}
</script>
</head>

<body class="content">
<div class="searchform">
    <form method="get" id="searchform" target="_self">
    <input type="hidden" id="actionmethod" name="action" value="search">

<table>
		<tr>
			
			<td width="*" align="right"></td>
			<td width="100">
			<select name="zhanghu" class="select bai" id="zhanghu"><option value='0'>所有账户</option><?php
			foreach (c_classinfo("zhanghu") as $k => $v) {
				if(array_key_exists('zhanghu', $_s) && intval($_s['zhanghu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
				}else{
					echo "<option value='{$v['id']}'>{$v['name']}</option>";    
				}                    
			}
			?></select></td>
			<td width="100">
			<select name="id_login" class="select bai" id="id_login"><option value='0'>所有记账人</option><?php
			foreach ($admininfo as $k => $v) {
				if(array_key_exists('id_login', $_s) && intval($_s['id_login'])===intval($v['ID'])){
					echo "<option value='{$v['ID']}' selected='selected'>{$v['Realname']}</option>";
				}else{
					echo "<option value='{$v['ID']}'>{$v['Realname']}</option>";    
				}                    
			}
			?></select></td>
			<?php
			if($check_enable){
			?>
			<td width="100">
				<select name="isok" class="select bai" id="isok">
					<option value=''>全部状态</option>
					<option value='0' <?php if(array_key_exists('isok', $_s) && intval($_s['isok'])===0){echo "selected=selected";}?>>已审</option>
					<option value='1' <?php if(array_key_exists('isok', $_s) && intval($_s['isok'])===1){echo "selected=selected";}?>>未审</option>
				</select>
			</td>
			<?php } ?>
			<td width="80" align="right">日期:</td>
			<td width="100">
				<input type="text" name="time0" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php 
					if(array_key_exists("time0",$_s)){
						echo $_s['time0'];
					}
				?>">
			</td>
			<td width="20" align="center">至</td>
			<td width="100">
				<input type="text" name="time1" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php 
					if(array_key_exists("time1",$_s)){
						echo $_s['time1'];
					}
				?>">
			</td>

			<td width="40" align="right">单号:</td>
			<td width="180">
				<input type="text" name="moneyID" class="text" value="<?php 
					if(array_key_exists("moneyID",$_s)){
						echo $_s['moneyID'];
					}
				?>">
			</td>
			
			<td width="80" align="right">
<input type="submit" onclick="setmethod('search');" value="查询" class="sub">
			</td>
			<td width="80" align="right">
<input type="submit" onclick="setmethod('output');" value="导出" class="reset" title="根据当前查询条件导出">
			</td>
		</tr>

	</table>
    </form>
</div>



<table class="table-list">
	<thead>
    	<tr>
			<th>单号</th>
            <th>时间</th>
            <th>金额</th>
			<th>账户</th>
            <th>备注</th>
            <th>记账人</th>
			<th>
<?php if($check_enable){ ?>审核状态<?php } ?></th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list'>
        	<td><?php echo $v['moneyID'];?></td>
			<td><?php echo substr($v['selldate'],0,10);?></td>
            <td><?php echo $v['price'];?></td>
			<td><?php echo c_zhangname($v['zhanghu']);?>--><?php echo c_zhangname($v['zhanghu1']);?></td>
            <td><?php echo $v['beizhu'];?></td>
			<td><?php echo c_adminname($v['id_login']);?></td>
<td><?php if($check_enable){ ?><?php if ($v['isok']==0){echo '已审';}?><?php if ($v['isok']==1){echo '未审';}?><?php } ?>
</td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
    		<?php
    		if($check_enable){
    			echo "<td colspan='14' style='padding-left:3px;'>";
    		}else{
    			echo "<td colspan='12' style='padding-left:3px;'>";
    		}
			echo $fenye;
			?>
			</td>
        </tr>
    </tfoot>
    
</table>




</body>
</html>