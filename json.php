<?php


error_reporting(0);
require_once(dirname(__FILE__).'/include/common.php');

$action = empty($_REQUEST['act'])?"":$_REQUEST['act'];

if($action =="getgoods"){
	//根据网站获取商品列表
	$class_id = intval($_GET['class_id']);
	$data = $con->select("select * from #__goods where class_id=$class_id");
	$res = array();
	foreach($data as $k=>$v){
		$res[] = array(
			'id'	=>$v['id'],
			'data'	=>$v['goods'],
		);
	}
	echo json_encode($res);
	die();
}else if($action =="getbigclass"){
	//根据类型获取一级分类
	$type = intval($_GET['type']);
	$data = $con->select("select * from #__money_bigclass where type=$type");
	$res = array();
	foreach($data as $k=>$v){
		$res[] = array(
			'id'	=>$v['id'],
			'bigclass'	=>$v['bigclass'],
			'isok'	=>$v['isok']?'（应收/应付）':'',
		);
	}
	echo json_encode($res);
	die();
}else if($action =="getsmallclass"){
	//根据类型获取er级分类
	$id_bigclass = intval($_GET['id_bigclass']);
	$data = $con->select("select * from #__money_smallclass where id_bigclass=$id_bigclass");
	$res = array();
	foreach($data as $k=>$v){
		$res[] = array(
			'id'	=>$v['id'],
			'smallclass'	=>$v['smallclass'],
			'isok'	=>$v['isok']?'（应收/应付）':'',
		);
	}
	echo json_encode($res);
	die();
}else if($action=="setordernotice"){
	//保存新订单提示开关
	$notice = !isset($_POST['notice'])?true:(intval($_POST['notice'])===1?true:false);
	$arr = array(
		"notice"=>$notice
	);
	lyg::writeArr("notice",$arr);
}else if($action=="setemailtype"){
	//设置发件方式
	$type = empty($_POST['type']) || $_POST['type']=="local"?"local":"server";
	$email = lyg::readArr("mail");
	$email['type'] = $type;
	lyg::writeArr("mail",$email);
	echo json_encode(array()); exit();
}else if($action=="setemailver"){
	//设置server_ver
	$email = lyg::readArr("mail");
	$email['server_ver'] = $email['server_ver']+0.1;
	$email['server_ver'] = sprintf("%01.1f",$email['server_ver']);
	lyg::writeArr("mail",$email);
	echo 'ok';
    die;
}else if($action=="setservermail"){
	//设置邮件服务器发件方式
	$data = array(
		"server_mail"=>empty($_POST['server_mail'])?"":trim($_POST['server_mail']),
		"server_key"=>empty($_POST['server_key'])?"":trim($_POST['server_key']),
		"server_url"=>empty($_POST['server_url'])?"":trim($_POST['server_url']),
		"server_ver"=>empty($_POST['server_ver'])?"":trim($_POST['server_ver']),
		
	);
	
	if($data['server_mail']==""){
		ajaxreturn("126邮箱设置错误");
	}
	if($data['server_key']==""){
		ajaxreturn("客户端授权码设置错误");
	}
	if($data['server_url']==""){
		ajaxreturn("Gid设置错误");
	}
	if($data['server_ver']==""){
		ajaxreturn("Ukey设置错误");
	}

	
	$email = lyg::readArr("mail");
	$email['type'] = 'server';
	foreach($data as $k=>$v){
		$email["{$k}"] = $v;
	}
	lyg::writeArr("mail",$email);
	ajaxreturn("保存成功","1");
	
}else if($action=="setwebconfig"){
	//网页设置
	if(empty($_POST['key'])){ajaxreturn('参数错误');}
	if(!isset($_POST['val'])){ajaxreturn('设置错误');}
	$key = trim($_POST['key']);
	$val = trim($_POST['val']);
	
	if($key=="order_interval"){
		$val = intval($val);
	}else if($key == 'backrestore_ext'){
		if($val=='.php'){
			ajaxreturn('数据备份文件后缀名不允许为php');
		}
		//数据库备份文件后缀名，.开头+数字或字母
		if(!preg_match('/^\.[0-9a-zA-Z]{3,}$/',$val)){
			ajaxreturn('数据备份文件后缀名设置错误');
		}
	}
	
	$webconfig = lyg::readArr("web");
	$webconfig["{$key}"] = $val;
	lyg::writeArr("web",$webconfig);
	ajaxreturn($val,"1");
}else if($action == "addcode"){
	//新增调用代码
	$data = array(
		"title"=>trim($_POST['title']),
		"class_id"=>intval($_POST['class_id']),
		"themes"=>trim($_POST['themes']),
		"width"=>trim($_POST['width']),
		"height"=>trim($_POST['height']),
		"target"=>trim($_POST['target']),
		"goods_list_style"=>intval($_POST['goods_list_style']),
	);
	$no = md5(implode("",$data).microtime());
	$data['maodouu'] = $no;
	$newid = $con->add("code",$data);
	if($newid){
		ajaxreturn($no,"1");
	}else{
		ajaxreturn('failed');
	}
}else if($action == "setadminenable"){
	//更改调用代码的可用状态
	$id = intval($_POST['id']);
	$enable = isset($_POST['enable'])?intval($_POST['enable'])%2:0;
	$data = array(
		"Working"=>$enable,
	);
	$res = $con->savemin("admin",$data,"id={$id}");
	ajaxreturn('success',"1");
}else if($action == "setmoneyenable"){
	//更改审核状态
	$id = intval($_POST['id']);
	$enable = isset($_POST['enable'])?intval($_POST['enable'])%2:0;
	$data = array(
		"isok"=>$enable,
	);
	$res = $con->savemin("money",$data,"id={$id}");
	ajaxreturn('success',"1");
}else if($action == "setpayenable"){
	//更改审核状态
	$id = intval($_POST['id']);
	$enable = isset($_POST['enable'])?intval($_POST['enable'])%2:0;
	$data = array(
		"isok"=>$enable,
	);
	$res = $con->savemin("money_pay",$data,"id={$id}");
	ajaxreturn('success',"1");
}else if($action == "editcode"){
	//编辑调用代码
	if(empty($_POST['id'])){
		ajaxreturn('参数错误');
	}
	$id = intval($_POST['id']);
	$code = $con->find("select * from #__code where id=$id");
	
	$data = array(
		"title"=>trim($_POST['title']),
		"class_id"=>intval($_POST['class_id']),
		"themes"=>trim($_POST['themes']),
		"width"=>trim($_POST['width']),
		"height"=>trim($_POST['height']),
		"target"=>trim($_POST['target']),
		"goods_list_style"=>intval($_POST['goods_list_style']),
	);
	$res = $con->savemin("code",$data,"id={$id}");
	ajaxreturn($code['maodouu'],"1");
	
}else if($action == "notice"){
	//获取最新订单编号，用于新订单提醒
	
	$data=$con->find('select id from #__order order by id desc');
	if($data){
		ajaxreturn($data['id'],"1");
	}else{
		ajaxreturn("null");
	}	
}else if($action == "addiptoblacklist"){
	//将单个IP加入黑名单
	$ip = $_POST['ip'];
	$ex = $con->rowscount("select count(id) from #__blacklist where ip=?",array($ip));
	if(empty($ex)){
		$con->add("blacklist",array("ip"=>$ip));
	}
	ajaxreturn("SUCCESS","1");

}else if($action == "goodssort"){
	//更新商品排序
	$id = intval($_POST['id']);
	$sort = intval($_POST['sort']);
	$con->update("update #__goods set sort={$sort} where id={$id}");
	ajaxreturn("SUCCESS","1");
	
}else if($action == "addexpress"){
	//添加账户类型
	$name = trim($_POST['name']);
	if($name == ""){
		ajaxreturn("请输入账户类型名称");
	}
	$newid = $con->add("zhanghu_class",array("zhanghuclass"=>$name));
	if($newid){
		ajaxreturn("SUCCESS","1");
	}else{
		ajaxreturn("ERROR");
	}
	
}else if($action == "editexpress"){
	//编辑账户类型
	$id = intval($_POST['id']);
	
	$newname = trim($_POST['name']);
	if($newname == ""){
		ajaxreturn("请输入账户类型名称");
	}
	$newid = $con->update("update #__zhanghu_class set zhanghuclass=? where id={$id}",array($newname));
	if($newid!==false){
		ajaxreturn("SUCCESS","1");
	}else{
		ajaxreturn("ERROR");
	}
	
}else if($action == "expresshasorder"){
	//判断账户类型信息是否再一些订单中有使用
	$id = intval($_POST['id']);
	$c = $con->rowscount("select count(id) from #__order where express_id={$id}");
	if($c){
		ajaxreturn("YES","1");
	}else{
		ajaxreturn("no");
	}	
}else if($action == "delexpress"){
	//删除账户类型信息
	$id = intval($_POST['id']);
	$con->excute("delete from #__zhanghu_class where id={$id} limit 1");
	ajaxreturn("SUCCESS","1");
}else if($action == "enableexpress"){
	$enable = intval($_POST['enable'])%2;
	lyg::writeArr("expressopen",array("enable"=>$enable));
	ajaxreturn("SUCCESS","1");
}

function ajaxreturn($info,$flag="0"){
	$data = array(
		"flag"=>$flag,
		"info"=>$info,
	);
	echo json_encode($data); exit();
}