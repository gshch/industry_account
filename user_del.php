<?php

require_once(dirname(__FILE__).'/include/common.php');

if(empty($_GET['id']) || intval($_GET['id'])<1){
	LYG::ShowMsg('参数错误');
}
$id = intval($_GET['id']);

//判断用户下是否有数据
$ddcount = $con->rowscount("select count(*) from #__money where id_login=$id");
if($ddcount>0){
	LYG::ShowMsg('该用户有s数据，暂不能删除');
}
$ddcount1 = $con->rowscount("select count(*) from #__money_pay where id_login=$id");
if($ddcount1>0){
	LYG::ShowMsg('该用户有y数据，暂不能删除');
}
$debug = lyg::readArr("debug");
if($debug['debug']){
	//至少保留一个用户
	$gsCount = $con->rowsCount('select count(*) from #__admin');
	if($gsCount < 2){
		LYG::ShowMsg('至少需要一个用户');
	}
}

$sql="delete from #__admin where ID=$id limit 1";
$data =$con->Excute($sql);
if($data){
	lyg::jump('user_list.php');
}else{
	LYG::ShowMsg('删除失败');
}	
