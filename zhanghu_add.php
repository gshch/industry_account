<?php

require_once(dirname(__FILE__).'/include/common.php');

if(!empty($_POST)){
	//参数校验
	extract($_POST);

	$type = intval($type);
	$beizhu = trim($beizhu);
	
	if(empty($name) || trim($name)==''){
		LYG::ShowMsg('账户名不能为空');
	}
	$name= trim($name);
	
	$ex = $con->rowscount("select count(*) from #__zhanghu where type=? and name=?",array(
		$type,$name
	));
	if($ex>0){
		lyg::showmsg("同名账户已存在");
	}
	
	$data = array(
		'name'		=>$name,
		'type'	=>$type,
		'amount'		=>floatval($amount),
		'amount0'		=>floatval($amount),
		'beizhu'		=>$beizhu
	);
	
	$aok = $con->add("zhanghu",$data);

	if($aok){
		LYG::ShowMsg('添加成功','zhanghu_list.php');
	}else{
		LYG::ShowMsg('添加失败，请重试');
	}
	
	die();
}

$classinfo = $con->select("select * from #__zhanghu_class");
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加账户</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<table cellpadding="3" cellspacing="0" class="table-add">

		<tr>
			<td align="right" width='100' height='36'>账户名称：</td>
			<td align="left" width='*'>
				<input type='text' class='inp' name='name' placeholder=''/>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>账户类型：</td>
			<td align="left" width='*'>
				<select name="type" class="select">
				<?php
				foreach($classinfo as $k=>$v){
					echo "<option value='{$v['id']}'>{$v['zhanghuclass']}</option>";
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>当前余额：</td>
			<td>
				<input type="text" name="amount" class="inp" placeholder="0.00">
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>备注：</td>
			<td>
				<input type="text" name="beizhu" class="inp" placeholder="">
			</td>
		</tr>
		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='sub' type='submit' value='添加'/></td>
		</tr>

	</table>
</form>

</body>
</html>