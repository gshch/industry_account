<?php

require_once(dirname(__FILE__).'/include/common.php');

$class=trim($_GET['class']);
if(empty($c_class["{$class}"])){
	LYG::ShowMsg('参数错误');
}

if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$id = intval($_REQUEST['id']);
$info=  $con->find("select * from #__{$class} where id=$id");
if(empty($info)){lyg::showmsg('参数错误');}


if(!empty($_POST)){
	//参数校验
	extract($_POST);
	
	if(empty($name) || trim($name)==''){
		LYG::ShowMsg('名称不能为空');
	}
	$name= trim($name);
	$beizhu= trim($beizhu);
	$ex = $con->rowscount("select count(*) from #__{$class} where name=? and id<>$id",array($name));
	if($ex>0){
		lyg::showmsg("已存在");
	}
	

if ($class=="xiangmu"){
$eok = $con->update("update #__{$class} set name=?,beizhu=? where id=$id limit 1",array($name,$beizhu));
}
if ($class=="wanglai"){
$eok = $con->update("update #__{$class} set name=?,beizhu=?,tel=?,address=?,fax=?,email=? where id=$id limit 1",array($name,$beizhu,$tel,$address,$fax,$email));
}
if ($class=="yuangong"){
$eok = $con->update("update #__{$class} set name=?,beizhu=?,tel=?,address=?,shengri=? where id=$id limit 1",array($name,$beizhu,$tel,$address,$shengri));
}


	

	if($eok!==false){
		LYG::ShowMsg('操作成功','class_list.php?class='.$class.'');
	}else{
		LYG::ShowMsg('操作失败，请重试');
	}
	die();
}
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>编辑<?php echo $c_class["{$class}"];?></title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<input type='hidden' name='id' value='<?php echo $info['id'];?>'>
	<table cellpadding="3" cellspacing="0" class="table-add">
		<tbody>
			<tr>
				<td align="right" width='100' height='36'><?php echo $c_class["{$class}"];?>名称：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='name' value='<?php echo $info['name'];?>'/>
				</td>
			</tr>

<?php if ($class=="wanglai"){?>
			<tr>
				<td align="right" width='100' height='36'>电话：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='tel' value='<?php echo $info['tel'];?>'/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>传真：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='fax' value='<?php echo $info['fax'];?>'/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>Email：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='email' value='<?php echo $info['email'];?>'/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>地址：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='address' value='<?php echo $info['address'];?>'/>
				</td>
			</tr>
<?php }?>

<?php if ($class=="yuangong"){?>
			<tr>
				<td align="right" width='100' height='36'>电话：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='tel' value='<?php echo $info['tel'];?>'/>
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>生日：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='shengri' value='<?php echo substr($info['shengri'],0,10);?>' onclick="WdatePicker();" />
				</td>
			</tr>
			<tr>
				<td align="right" width='100' height='36'>部门：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='address' value='<?php echo $info['address'];?>'/>
				</td>
			</tr>
<?php }?>

			<tr>
				<td align="right" width='100' height='36'>备注：</td>
				<td align="left" width='*'>
					<input type='text' class='inp' name='beizhu' value='<?php echo $info['beizhu'];?>'/>
				</td>
			</tr>
			<tr>
				<td align="right" height='50'>　</td>
				<td align="left"><input class='sub' type='submit' value='修改'/></td>
			</tr>
		</tbody>
	</table>
</form>

</body>
</html>