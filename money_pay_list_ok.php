<?php

require_once(dirname(__FILE__).'/include/common.php');
//是否启用审核
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


$_k = array();
$_v = array();

$_c = array();//分页条件
$_s = array();//搜索条件

if(isset($_GET['type']) && trim($_GET['type'])<>''){
    $_k[]="#__money_pay.type=?";
    $_v[]=intval($_GET['type']);
    $_c[]="type=".intval($_GET['type']);
    $_s['type'] = intval($_GET['type']);
}

if(!empty($_GET['id_bigclass']) && intval($_GET['id_bigclass'])>0){
    $_k[]="#__money_pay.id_bigclass=?";
    $_v[]=intval($_GET['id_bigclass']);
    $_c[]="id_bigclass=".intval($_GET['id_bigclass']);
    $_s['id_bigclass'] = intval($_GET['id_bigclass']);
}

if(!empty($_GET['id_smallclass']) && intval($_GET['id_smallclass'])>0){
    $_k[]="#__money_pay.id_smallclass=?";
    $_v[]=intval($_GET['id_smallclass']);
    $_c[]="id_smallclass=".intval($_GET['id_smallclass']);
    $_s['id_smallclass'] = intval($_GET['id_smallclass']);
}

if(!empty($_GET['zhanghu']) && intval($_GET['zhanghu'])>0){
    $_k[]="#__money_pay.zhanghu=?";
    $_v[]=intval($_GET['zhanghu']);
    $_c[]="zhanghu=".intval($_GET['zhanghu']);
    $_s['zhanghu'] = intval($_GET['zhanghu']);
}
if(!empty($_GET['wanglai']) && intval($_GET['wanglai'])>0){
    $_k[]="#__money_pay.wanglai=?";
    $_v[]=intval($_GET['wanglai']);
    $_c[]="wanglai=".intval($_GET['wanglai']);
    $_s['wanglai'] = intval($_GET['wanglai']);
}

if(!empty($_GET['yuangong']) && intval($_GET['yuangong'])>0){
    $_k[]="#__money_pay.yuangong=?";
    $_v[]=intval($_GET['yuangong']);
    $_c[]="yuangong=".intval($_GET['yuangong']);
    $_s['yuangong'] = intval($_GET['yuangong']);
}
if(!empty($_GET['xiangmu']) && intval($_GET['xiangmu'])>0){
    $_k[]="#__money_pay.xiangmu=?";
    $_v[]=intval($_GET['xiangmu']);
    $_c[]="xiangmu=".intval($_GET['xiangmu']);
    $_s['xiangmu'] = intval($_GET['xiangmu']);
}

if($_SESSION['eptime_adminPower']==2){

    $_k[]="#__money_pay.id_login=?";
    $_v[]=intval($_SESSION['eptime_id']);
    $_c[]="id_login=".intval($_SESSION['eptime_id']);
    $_s['id_login'] = intval($_SESSION['eptime_id']);

	}
else{

if(!empty($_GET['id_login']) && intval($_GET['id_login'])>0){
    $_k[]="#__money_pay.id_login=?";
    $_v[]=intval($_GET['id_login']);
    $_c[]="id_login=".intval($_GET['id_login']);
    $_s['id_login'] = intval($_GET['id_login']);
}
	}
if(isset($_GET['isok']) && trim($_GET['isok'])<>''){
    $_k[]="#__money_pay.isok=?";
    $_v[]=intval($_GET['isok']);
    $_c[]="isok=".intval($_GET['isok']);
    $_s['isok'] = intval($_GET['isok']);
}
if(!empty($_GET['moneyID']) && trim($_GET['moneyID'])!=''){
    $moneyID = trim($_GET['moneyID']);
    $_k[]="#__money_pay.moneyID=?";
    $_v[]=$moneyID;
    $_c[]="moneyID=$moneyID";
    $_s['moneyID'] = $moneyID;
}

if(!empty($_GET['time0']) && !empty($_GET['time1']) && 
    preg_match("/^\d{4}\-\d{2}\-\d{2}$/",$_GET['time0']) && preg_match("/^\d{4}\-\d{2}\-\d{2}$/",$_GET['time1'])
){
    $time0 = $_GET['time0'];
    $time1 = $_GET['time1'];
    $t0 = $time0." 00:00:00";
    $t1 = $time1." 23:59:59";
    if($t0!==false && $t1!==false && $t1>$t0){

        $_k[]="#__money_pay.selldate>=?";
        $_v[]=$t0;
        $_c[]="time0=$time0";
        $_s['time0'] = $time0;

        $_k[]="#__money_pay.selldate<=?";
        $_v[]=$t1;
        $_c[]="time1=$time1";
        $_s['time1'] = $time1;
    }
}

$_k = implode(' and ',$_k);
if($_k!=''){
    $_k = " where #__money_pay.price=0 and #__money_pay.type!=2 and ".$_k;
}else{$_k = " where #__money_pay.price=0 and #__money_pay.type!=2 ";}

$field = array(
	"#__money_pay.*",
	"#__zhanghu.name",
	"#__admin.Realname"
);
$field = implode(",",$field);
$left = "left join #__zhanghu on #__zhanghu.id=#__money_pay.zhanghu ";
$left.= "left join #__admin on #__admin.ID=#__money_pay.id_login ";

$action="search";
if(!empty($_GET['action']) && $_GET['action']=='output'){
	require_once(dirname(__FILE__).'/include/PHPExcel/PHPExcel.php');
	$sql = "select {$field} from #__money_pay {$left} {$_k} order by #__money_pay.id desc,#__money_pay.isok desc";
	$data = $con->select($sql,$_v);
	require_once("output.php");
	die();
}


if($webconfig['eptime_pagesize']){$pagesize=$webconfig['eptime_pagesize'];}else{$pagesize = 20;}



$datacount=$con->RowsCount("select count(#__money_pay.id) from #__money_pay {$left} {$_k}",$_v);

$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(!empty($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;

$sql = "select {$field} from #__money_pay {$left} {$_k} order by #__money_pay.id desc,#__money_pay.isok desc limit {$start_id},{$pagesize}";
$data =$con->select($sql,$_v);

$fenye = LYG::getPageHtml($page,$datacount,$pagesize,$_c);



$bigclassinfo = array();
if(array_key_exists('type',$_s)){
    $bigclassinfo = $con->select("select * from #__money_bigclass where type = ?",array($_s['type']));
}

$smallclassinfo = array();
if(array_key_exists('id_bigclass',$_s)){
    $smallclassinfo = $con->select("select * from #__money_smallclass where id_bigclass = ?",array($_s['id_bigclass']));
}

$admininfo = $con->select("select * from #__admin");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>流水</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(function(){
    $("#type").change(function(){
        var id = parseInt($(this).val());

            $.ajax({
                type:'get',
                url:'json.php?act=getbigclass&type='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有一级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].bigclass+"</option>";
                    }
                    $("#id_bigclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });

    $("#id_bigclass").change(function(){
        var id = parseInt($(this).val());
        if(id==''){
            $("#id_smallclass").html("<option value=''>所有二级分类</option>");
        }else{
            $.ajax({
                type:'get',
                url:'json.php?act=getsmallclass&id_bigclass='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有二级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].smallclass+"</option>";
                    }
                    $("#id_smallclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });
        }
    });

	
});
function setmethod(tag){
	$("#actionmethod").val(tag);
	if(tag=="output"){
		$("#searchform").attr("target","_blank");
	}else{
		$("#searchform").removeAttr("target");
	}
}
</script>
<SCRIPT language=javascript> 
function openScript(url, width, height) {
        var Win = window.open(url,"openScript",'width=' + width + ',height=' + 
 
height + ',resizable=0,scrollbars=no,menubar=no,status=no' );
}
</SCRIPT>
<script type="text/javascript">
function setEnable(obj,id){
    $.post("json.php",{"act":"setpayenable","id":id,"enable":obj.checked?"0":"1"},function(e){
        location.reload();
    },"Json");
}
</script>
</head>

<body class="content">
<div class="searchform">
    <form method="get" id="searchform" target="_self">
    <input type="hidden" id="actionmethod" name="action" value="search">
	<table>
		<tr>
		<td width="*" align="right"></td>
			<td width="100">
			<select name="type" class="select bai" id="type">
<option value=''>所有类型</option>
			<?php 
			foreach($c_type2 as $k=>$v){
					if(array_key_exists('type', $_s) && intval($_s['type'])===intval($k)){
						echo "<option value='{$k}' selected='selected'>{$v}</option>";
					}else{
						echo "<option value='{$k}'>{$v}</option>";
					}
			}?>
			</select>			
			
			</td>

			<td width="100">
				<select name="id_bigclass" class="select bai" id="id_bigclass">
					<option value=''>所有一级分类</option><?php
					foreach($bigclassinfo as $k=>$v){
						if(array_key_exists('id_bigclass', $_s) && intval($_s['id_bigclass'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['bigclass']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['bigclass']}</option>";    
						}
					}
					?>
				</select>
			</td>

			<td width="100">
				<select name="id_smallclass" class="select bai" id="id_smallclass">
					<option value=''>所有二级分类</option><?php
					foreach($smallclassinfo as $k=>$v){
						if(array_key_exists('id_smallclass', $_s) && intval($_s['id_smallclass'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['smallclass']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['smallclass']}</option>";    
						}
					}
					?>
				</select>
			</td>
			<td width="100">
			<select name="zhanghu" class="select bai" id="zhanghu"><option value='0'>所有账户</option><?php
			foreach (c_classinfo("zhanghu") as $k => $v) {
				if(array_key_exists('zhanghu', $_s) && intval($_s['zhanghu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
				}else{
					echo "<option value='{$v['id']}'>{$v['name']}</option>";    
				}                    
			}
			?></select></td>


			<td width="100">
				<select name="wanglai" class="select bai" id="wanglai">
					<option value='0'>所有<?php echo $webconfig['system_wanglai'];?></option><?php
					foreach(c_classinfo("wanglai") as $k=>$v){
						if(array_key_exists('wanglai', $_s) && intval($_s['wanglai'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
				</select>
			</td>

			<td width="100">
				<select name="yuangong" class="select bai" id="yuangong">
					<option value='0'>所有<?php echo $webconfig['system_yuangong'];?></option><?php
					foreach(c_classinfo("yuangong") as $k=>$v){
						if(array_key_exists('yuangong', $_s) && intval($_s['yuangong'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
				</select>
			</td>

			<td width="100">
				<select name="xiangmu" class="select bai" id="xiangmu">
					<option value='0'>所有<?php echo $webconfig['system_xiangmu'];?></option><?php
					foreach(c_classinfo("xiangmu") as $k=>$v){
						if(array_key_exists('xiangmu', $_s) && intval($_s['xiangmu'])===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
				</select>
			</td>
			<td width="100">
			<select name="id_login" class="select bai" id="id_login"><option value='0'>所有记账人</option><?php
			foreach ($admininfo as $k => $v) {
				if(array_key_exists('id_login', $_s) && intval($_s['id_login'])===intval($v['ID'])){
					echo "<option value='{$v['ID']}' selected='selected'>{$v['Realname']}</option>";
				}else{
					echo "<option value='{$v['ID']}'>{$v['Realname']}</option>";    
				}                    
			}
			?></select></td>
			<?php
			if($check_enable){
			?>
			<td width="100">
				<select name="isok" class="select bai" id="isok">
					<option value=''>全部状态</option>
					<option value='0' <?php if(array_key_exists('isok', $_s) && intval($_s['isok'])===0){echo "selected=selected";}?>>已审</option>
					<option value='1' <?php if(array_key_exists('isok', $_s) && intval($_s['isok'])===1){echo "selected=selected";}?>>未审</option>
				</select>
			</td>
			<?php } ?>
		</tr>
</table>
<table>
		<tr>
			
			<td width="*" align="right"></td>
			<td width="80" align="right">日期:</td>
			<td width="100">
				<input type="text" name="time0" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php 
					if(array_key_exists("time0",$_s)){
						echo $_s['time0'];
					}
				?>">
			</td>
			<td width="20" align="center">至</td>
			<td width="100">
				<input type="text" name="time1" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php 
					if(array_key_exists("time1",$_s)){
						echo $_s['time1'];
					}
				?>">
			</td>

			<td width="40" align="right">单号:</td>
			<td width="180">
				<input type="text" name="moneyID" class="text" value="<?php 
					if(array_key_exists("moneyID",$_s)){
						echo $_s['moneyID'];
					}
				?>">
			</td>
			
			<td width="80" align="right">
<input type="submit" onclick="setmethod('search');" value="查询" class="sub">
			</td>
			<td width="80" align="right">
<input type="submit" onclick="setmethod('output');" value="导出" class="reset" title="根据当前查询条件导出">
			</td>
		</tr>

	</table>
    </form>
</div>



<table class="table-list">
	<thead>
    	<tr>
			<th>单号</th>
            <th>时间</th>
            <th>[类型]大类->小类</th>
            <th>已收</th>
            <th>已付</th>
            <th>期限</th>
            <th><?php echo $webconfig['system_wanglai'];?></th>
            <th><?php echo $webconfig['system_yuangong'];?></th>
            <th><?php echo $webconfig['system_xiangmu'];?></th>
            <th>备注</th>
            <th>记账人</th>
			<th>
<?php if($check_enable){ ?>状态 | <?php } ?>附件</th>
<th>详情</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($data as $k=>$v){?>
    	<tr class='list'>
        	<td><?php echo $v['moneyID'];?></td>
			<td><?php echo substr($v['selldate'],0,10);?></td>
<td>[<?php echo $c_type2["{$v['type']}"];?>]<?php echo c_bigclassname($v['id_bigclass']);?>-><?php echo c_smallclassname($v['id_smallclass']);?></td>
            <td><?php if ($v['type']==0){echo $v['price1'];}?></td>
			<td><?php if ($v['type']==1){echo $v['price1'];}?></td>
            <td><?php echo substr($v['Lastdate'],0,10);?></td>
<td align="center"><?php echo c_classname("wanglai",$v['wanglai']);?></td>
<td align="center"><?php echo c_classname("yuangong",$v['yuangong']);?></td>
<td align="center"><?php echo c_classname("xiangmu",$v['xiangmu']);?></td>
            <td><?php echo GBsubstr($v['beizhu'],0,18);?></td>
			<td><?php echo c_adminname($v['id_login']);?></td>
<td>
<?php if($check_enable){ 
	if ($_SESSION['eptime_adminPower']==2){?>
<?php echo $v['isok']=="0"?" 已审":"未审";?>
   <?php }else{ ?>
<input type="checkbox" class="enable" <?php echo "onclick=\"setEnable(this,{$v['id']});\""; echo $v['isok']=="0"?" checked='checked'":"";?>/>
<?php echo $v['isok']=="0"?" 已审":"未审";?>
<?php }
	} ?>
<?php 
//附件显示开始
if ($v['img']!=""){
$count = count(explode(',', $v['img']));
$kk=0;
foreach( explode(',', $v['img']) as $k1=>$v1)
{$kk=$kk+1;
if($count<>$kk){echo '<a href="'.$v1.'" target="blank">'.$kk.'</a> ';}
}		
			}
//附件显示结束			
			?>
</td>
<td><a href='javascript:openScript("money_pay_show_ok.php?id=<?php echo $v['id'];?>",500,640)'>详情</a></td>
        </tr>
	<?php }?>
    </tbody>
    <tfoot>
    	<tr>
    		<?php
    		if($check_enable){
    			echo "<td colspan='14' style='padding-left:3px;'>";
    		}else{
    			echo "<td colspan='13' style='padding-left:3px;'>";
    		}
			echo $fenye;
			?>
			</td>
        </tr>
    </tfoot>
    
</table>




</body>
</html>