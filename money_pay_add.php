<?php

require_once(dirname(__FILE__).'/include/common.php');
if (strpos($_SESSION['eptime_flag'], 'payfig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;
if(!empty($_POST)){
	//参数校验
	extract($_POST);

$type=intval($type);
$id_bigclass=intval($id_bigclass);
$id_smallclass=intval($id_smallclass);
$price=trim($price);
$selldate=trim($selldate);
$Lastdate=trim($Lastdate);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$beizhu= trim($beizhu);
	$img = trim($img);

$moneyID = c_newOrderNo("Y");
$id_login = $_SESSION['eptime_id'];
$login = $_SESSION['eptime_username'];
if($check_enable){
	$isok = 1;
		}
		else{$isok = 0;}
$addtime = date("Y-m-d H:i:s",time());


	if(trim($type)==''){
		LYG::ShowMsg('类型不能为空');
	}
	if(empty($id_bigclass) || trim($id_bigclass)==''){
		LYG::ShowMsg('一级分类不能为空');
	}
	if(empty($id_smallclass) || trim($id_smallclass)==''){
		LYG::ShowMsg('二级分类不能为空');
	}
	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}
	$ex = $con->rowscount("select count(*) from #__money_pay where moneyID=?",array($moneyID));
	if($ex>0){
		lyg::showmsg("单据号出错，请重新提交！");
	}
	
	$data = array(
		'type'		=>$type,
  'id_bigclass'		=>$id_bigclass,
 'id_smallclass'	=>$id_smallclass,
        'price'		=>$price,
		'price1'	=>$price,
        'selldate'	=>$selldate,
		'Lastdate'	=>$Lastdate,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'img'		=>$img,
		'moneyID'	=>$moneyID,
		'id_login'	=>$id_login,
        'login'		=>$login,
		'isok'		=>$isok,
		'addtime'	=>$addtime
	);
	
	$aok = $con->add("money_pay",$data);

	if($aok){
LYG::writeLog("[".$_SESSION['eptime_username']."]添加单号[".$moneyID."]");
		LYG::ShowMsg('添加成功','money_pay_list.php');
	}else{
		LYG::ShowMsg('添加失败，请重试');
	}
	
	die();
}




?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>应收应付记账</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(function(){
    $("#type").change(function(){
        var id = parseInt($(this).val());

            $.ajax({
                type:'get',
                url:'json.php?act=getbigclass&type='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>请选择</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].bigclass+"</option>";
                    }
                    $("#id_bigclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });

    $("#id_bigclass").change(function(){
        var id = parseInt($(this).val());
            $.ajax({
                type:'get',
                url:'json.php?act=getsmallclass&id_bigclass='+id,
                dataType:'json',
                success:function(data){
                    var html="";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].smallclass+"</option>";
                    }
                    $("#id_smallclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });
	
});
function setmethod(tag){
	$("#actionmethod").val(tag);
	if(tag=="output"){
		$("#searchform").attr("target","_blank");
	}else{
		$("#searchform").removeAttr("target");
	}
}
</script>


		<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
		<script src="kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>

<body class="content">

<SCRIPT language=javascript>
function check()
{
	
if (document.form3.type.value=="")
{
alert("类型必须选择！");
return false;
}
if (document.form3.id_bigclass.value=="")
{
alert("一级分类必须填写，如没有，请先添加！");
return false;
}
if (document.form3.id_smallclass.value=="")
{
alert("二级分类必须填写，如没有，请先添加！");
return false;
}
if (document.form3.price.value=="")
{
alert("金额必须填写！");
return false;
}

if (document.form3.wanglai.value=="")
{
alert("<?php echo $webconfig['system_wanglai'];?>必须填写，请选择！如没有，请先添加！");
return false;
}
if (document.form3.yuangong.value=="")
{
alert("<?php echo $webconfig['system_yuangong'];?>必须填写，请选择！如没有，请先添加！");
return false;
}
if (document.form3.xiangmu.value=="")
{
alert("<?php echo $webconfig['system_xiangmu'];?>必须填写，请选择！如没有，请先添加！");
return false;
}

}
</script>

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post' name="form3">
	<table cellpadding="3" cellspacing="0" class="table-add">

		<tr>
			<td align="right" width='100' height='36'>类型：</td>
			<td align="left" width='*'>
			<select name="type" class="select" id="type">
			<option value=''>请选择</option>
			<?php 
			foreach($c_type1 as $k=>$v){
						echo "<option value='{$k}'>{$v}</option>";
			}?>
			</select>	
			</td>
		</tr>

		<tr>
			<td align="right" height='36'>一级分类：</td>
			<td>
				<select name="id_bigclass" class="select" id="id_bigclass">
				</select>
			</td>
		</tr>

		<tr>
			<td align="right" height='36'>二级分类：</td>
			<td>
				<select name="id_smallclass" class="select" id="id_smallclass">
				</select>
			</td>
		</tr>

		<tr>
			<td align="right" height='36'>金额：</td>
			<td>
			<input class='inp' type='number' name='price' placeholder="0.00" step="0.01" />
			<span>数字，请保留小数点后两位</span>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>时间：</td>
			<td>
			<input type='text' class='inp' name='selldate' value='<?php echo date("Y-m-d",time()) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
			<span>点击选择</span>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>期限：</td>
			<td>
			<input type='text' class='inp' name='Lastdate' value='<?php echo date("Y-m-d",strtotime('+7 days')) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
			<span>点击选择</span>
			</td>
		</tr>



		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_wanglai'];?>：</td>
			<td align="left" width='*'>
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
if(intval($_SESSION['eptime_l_wanglai'])===intval($v['id'])){echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";}
else{echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_yuangong'];?>：</td>
			<td align="left" width='*'>
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
if(intval($_SESSION['eptime_l_yuangong'])===intval($v['id'])){echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";}
else{echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_xiangmu'];?>：</td>
			<td align="left" width='*'>
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
if(intval($_SESSION['eptime_l_xiangmu'])===intval($v['id'])){echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";}
else{echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>

	<tr>
		<td align="right" height='36'>备注：</td>
		<td align="left"><textarea name="beizhu" cols="60" rows="3"  onKeyup="if(this.value.length>255){this.value=this.value.slice(0,255)}"></textarea></td>
	</tr>
	<tr>
		<td align="right" height='36'>附件：</td>
		<td align="left"><input type='text' class='inp' name='img' placeholder='' id="url3" readonly/> <input type="button" value="上传附件" id="open" class="layui-btn layui-btn-danger">
<div class="alert alert-info" style="width:92%" id="demo2"></div>
            <div class="item col-xs-12"><div class="f-fl item-ifo">
<link rel="stylesheet" href="layui/css/layui.css" media="all">
<script type="text/javascript" src="layui/layui.js"></script>
<script type="text/javascript">
layui.use('upload', function (){
  var upload = layui.upload,
    $ = layui.jquery;
var uploadInst = upload.render({
  elem : '#open',
  accept : 'images',//指定允许上传时校验的文件类型，可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
  multiple : 'true',
  url : 'up.php',
  data: {},
  before: function(obj){
      obj.preview(function(index, file, result){
        $('#demo2').append('<img src="'+ result +'" alt="'+ file.name +'" style="width:33%;">')
      });
    },
  done : function(res){
if(res['code']==1){
//alert(res['src']);
var imgpath=res['src'];
//alert(imgpath);
layer.msg('图片上传成功');
imgpath = imgpath + ','+$("#url3").val();
$("#url3").val(imgpath);
}else{
layer.msg('图片上传失败');
}
  },
  error : function(){   
    //请求异常
  }
});
});

</script>
            </div></div></td>
	</tr>

		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='sub' type='submit' value='添加' onClick="return check()"/></td>
		</tr>

	</table>
</form>

</body>
</html>