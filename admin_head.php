<?php

require_once(dirname(__FILE__).'/include/common.php');

$notice = lyg::readArr("notice");
$webconfig = lyg::readArr("web");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>头部</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
*{margin:0;padding:0;-webkit-font-smoothing: antialiased;}
body{background-color:#00ADC4;height:90px;overflow:hidden;}
p{display:inline-block; font-size:15px;}
p a{ color:#ffffff;}
p a.topnotice{position:relative;}
.sevcheckbox{
	overflow: hidden; display: inline-block; width: 50px; height: 20px; border: 1px solid #efefef;
	background: #fff;
	box-sizing: border-box; position: relative; font-family: "Microsoft yahei"; border-radius: 10px;
}
.sevcheckbox input[type=checkbox]{
	visibility: hidden;
}
.sevcheckbox label{
	width: 40%; z-index:2;
	position: absolute; 
	display: block;  height: 100%; top: 0; right: 60%; 
	box-sizing: border-box; 
	-webkit-transition:all ease .3s;
	-moz-transition:all ease .3s;
	transition:all ease .3s;
	margin-left:-50%; 
	background: #c7c7c7;
	border-radius:10px;
}
.sevcheckbox input[type=checkbox]:checked + label{
	right:0;
	background:#032D63;
}

.kaiguan{
	display:inline-block;font-weight:normal;color:#fff;padding:0 0 0 20px;display: inline-flex;
	-webkit-box-align: center;
	-moz-box-align: center;
	justify-content: center;
	align-items: center;
}
.kaiguan>label{padding-left:10px;}
</style>
<script type='text/javascript' src='js/jquery.min.js'></script>
<SCRIPT language=javascript> 
function openScript(url, width, height) {
        var Win = window.open(url,"openScript",'width=' + width + ',height=' + 
 
height + ',resizable=0,scrollbars=no,menubar=no,status=no' );
}
</SCRIPT>
<body>
<p style=" font-size:20px; font-weight:bold; line-height:1em; padding:35px 0 0 20px; color:#fff;"><?php echo $webconfig['system_title'];?><p>

<p style='padding:0 0 0 40px;'><a href='admin_default.php' class='topnotice' target='right'>首页</a></p>
<p style='padding:0 0 0 10px;'><a href='money_add.php' class='topnotice' target='right'>录上一笔</a></p>
<p style='padding:0 0 0 10px;'><a href='money_zhang.php' class='topnotice' target='right'>资金余额</a></p>
<p style='padding:0 0 0 10px;'><a href='money_list.php' class='topnotice' target='right'>资金流水</a></p>
<p style='padding:0 0 0 10px;'><a href='money_pay_list.php' class='topnotice' target='right'>应收应付</a></p>

<p style="float:right;margin-right:20px;font-size:14px;padding-top:30px;">
    <a style="margin-right:10px;"><img src="<?php echo $_SESSION['eptime_img'];?>" width="40" height="40" align="middle"></a>
	<a style="margin-right:20px;"><?php echo $_SESSION['eptime_username'];?></a>
	<a style="margin-right:10px;" href="mypwd.php" target="right">修改密码</a>
	<a href="exit.php" style="color:#fff;" target="_top" onclick="return confirm('确定退出吗？');">退出登录</a>
</p>


</body>
</html>
