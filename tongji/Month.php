<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");
if (strpos($_SESSION['eptime_flag'], 'monthfig') === false) {LYG::ShowMsg('您没有权限！');} 

if(empty($_GET['year'])){$_year=date("Y");}else{$_year=$_GET['year'];}

if(empty($_GET['month'])){$_month=date('n');}else{$_month=$_GET['month'];}

function total_d($type,$time){
	global $con;
$time1=$time." 00:00:00";
$time2 =$time." 23:59:59"; 
$time1 =strtotime($time1);
$time2 =strtotime($time2); 

    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}
		
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		<link href="../images/admin.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            title: {
                text: '<?php echo $_year;?>年<?php echo $_month;?>月收支曲线图',
                x: -20 //center
            },
            subtitle: {
                text: '日期: <?php echo date("Y-m-d");?>',
                x: -20
            },
            xAxis: {
                categories: [
<?php $days = date('t', strtotime("{$_year}-{$_month}-1"));
for ($x=1; $x<=$days; $x++){ ?>
'<?php echo $x;?>日', 
<?php } ?>
					]
            },
            yAxis: {
                title: {
                    text: 'RMB'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '￥'
            },
            legend: {
                layout: '',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: '收入',
                data: [
<?php
for ($x=1; $x<=$days; $x++){ ?>
<?php echo total_d("0",$_year."-".$_month."-".$x);?>, 
<?php } ?>
				]
            },{
                name: '支出',
                data: [
<?php
for ($x=1; $x<=$days; $x++){ ?>
<?php echo total_d("1",$_year."-".$_month."-".$x);?>, 
<?php } ?>
				]
            }]
        });
    });
    

		</script>
	</head>
	<body>
<br>

<form name="form2">
<?php if(empty($_GET['list'])){?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr> 
    <td width="50" height="30">&nbsp;</td>
	<td width="*" align="right">&nbsp;选择年月：
	  <select name="year" onChange="form2.submit()">
<?php 
for ($x=0; $x<=10; $x++) {
						if(intval($_year)===intval(date("Y", strtotime("-{$x} year")))){
							echo "<option value='".date("Y", strtotime("-{$x} year"))."' selected='selected'>".date("Y", strtotime("-{$x} year"))."</option>";
						}else{
							echo "<option value='".date("Y", strtotime("-{$x} year"))."'>".date("Y", strtotime("-{$x} year"))."</option>";   
						}
}
?>
      </select>
	  <select name="month" onChange="form2.submit()">
<?php 
for ($x=1; $x<=12; $x++) {
						if(intval($_month)===intval($x)){
							echo "<option value='".$x."' selected='selected'>".$x."月</option>";
						}else{
							echo "<option value='".$x."'>".$x."月</option>"; 
						}
}
?>
      </select>
&nbsp;<input type="submit" value=" 统计 " class="button">&nbsp;
	</td>
  </tr>
  </table>

<?php } else{?>
		<input type='hidden' name='list' value="Month" />
        <div class="hui-form-items">
        	<div class="hui-form-items-title">选择年：</div>
 	  <select name="year" onChange="form2.submit()">
<?php 
for ($x=0; $x<=10; $x++) {
						if(intval($_year)===intval(date("Y", strtotime("-{$x} year")))){
							echo "<option value='".date("Y", strtotime("-{$x} year"))."' selected='selected'>".date("Y", strtotime("-{$x} year"))."</option>";
						}else{
							echo "<option value='".date("Y", strtotime("-{$x} year"))."'>".date("Y", strtotime("-{$x} year"))."</option>";   
						}
}
?>
      </select>         
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">选择月：</div>
 	  <select name="month" onChange="form2.submit()">
<?php 
for ($x=1; $x<=12; $x++) {
						if(intval($_month)===intval($x)){
							echo "<option value='".$x."' selected='selected'>".$x."月</option>";
						}else{
							echo "<option value='".$x."'>".$x."月</option>"; 
						}
}
?>
      </select>         
        </div>
        <div style="padding:15px 8px;">
		<input type="submit" value=" 统计 " class="hui-button hui-button-large hui-primary" >
        </div>
<?php }?>
</form>  


<script src="../js/highcharts.js"></script>
<script src="../js/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

	</body>
</html>