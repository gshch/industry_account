<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");
if (strpos($_SESSION['eptime_flag'], 'moneyfig') === false) {LYG::ShowMsg('您没有权限！');} 



function total_b($bigclass,$time1,$time2){
	global $con;
		//统计收
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_bigclass={$bigclass} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where id_bigclass={$bigclass} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}
		
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}

function total_s($bigclass,$time1,$time2){
	global $con;
		//统计收
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_smallclass={$bigclass} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where id_smallclass={$bigclass} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}
		
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}

function total_a($type,$time1,$time2){
	global $con;
		//统计收
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}
		
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}



if(empty($_GET['year'])){$_year=date("Y");}else{$_year=$_GET['year'];}



if(empty($_GET['type'])){$_type="0";}else{$_type=$_GET['type'];}


if($_type==0){
$classes = $con->select("select * from #__money_bigclass where type=0 and isok=0");
$classes1 = $con->select("select * from #__money_bigclass where type=1 and isok=0");
}
if($_type==1){
$classes = $con->select("select * from #__money_smallclass where id_bigclass in (select id from #__money_bigclass where type=0 and isok=0)");
$classes1 = $con->select("select * from #__money_smallclass where id_bigclass in (select id from #__money_bigclass where type=1 and isok=0)");
}

?>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<!--[if !mso]>
<style>
v\:*         { behavior: url(#default#VML) }
o\:*         { behavior: url(#default#VML) }
.shape       { behavior: url(#default#VML) }
</style>
<![endif]-->
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../images/admin.css" rel="stylesheet" type="text/css">
<style>
body {
	background-color:#FFFFFF;
}
</style>
<style>
TD { FONT-SIZE: 9pt}
</style>
<script language=javascript>
function preview() { 
bdhtml=window.document.body.innerHTML;
sprnstr="<!--startprint-->"; 
eprnstr="<!--endprint-->"; 
prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17); 
prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
window.document.body.innerHTML=prnhtml; 
window.print();
window.document.body.innerHTML=bdhtml; 
         }
</script>
</HEAD>

<BODY topmargin=5 leftmargin=0 scroll=AUTO>

<script>
function CheckAll(form)  {
  for (var i=0;i<form.elements.length;i++)    {
    var e = form.elements[i];
    if (e.name != 'chkall')       e.checked = form.chkall.checked; 
   }
  }
</script>


<form name="form2">
<?php if(empty($_GET['list'])){?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr> 
    <td width="50" height="30">&nbsp;<img src="../style/images/print.jpg" align="absmiddle" style="cursor:hand;" onClick="preview();"></td>
	<td width="*" align="right">&nbsp;选择年份：
	  <select name="year" onChange="form2.submit()">
<?php 
for ($x=0; $x<=10; $x++) {
						if(intval($_year)===intval(date("Y", strtotime("-{$x} year")))){
							echo "<option value='".date("Y", strtotime("-{$x} year"))."' selected='selected'>".date("Y", strtotime("-{$x} year"))."</option>";
						}else{
							echo "<option value='".date("Y", strtotime("-{$x} year"))."'>".date("Y", strtotime("-{$x} year"))."</option>";   
						}
}
?>
      </select>

	  <select name="type" onChange="form2.submit()">
			<?php 
					if(intval($_type)===0){
						echo "<option value='0' selected='selected'>按一级分类</option>";
					}else{
						echo "<option value='0'>按一级分类</option>";
					}
					if(intval($_type)===1){
						echo "<option value='1' selected='selected'>按二级分类</option>";
					}else{
						echo "<option value='1'>按二级分类</option>";
					}
			?>
      </select>
&nbsp;<input type="submit" value=" 统计 " class="button">&nbsp;
	</td>
  </tr>
  </table>

<?php } else{?>
<input type='hidden' name='list' value="yearReport" />
        <div class="hui-form-items">
        	<div class="hui-form-items-title">选择年份：</div>
 	  <select name="year" onChange="form2.submit()">
<?php 
for ($x=0; $x<=10; $x++) {
						if(intval($_year)===intval(date("Y", strtotime("-{$x} year")))){
							echo "<option value='".date("Y", strtotime("-{$x} year"))."' selected='selected'>".date("Y", strtotime("-{$x} year"))."</option>";
						}else{
							echo "<option value='".date("Y", strtotime("-{$x} year"))."'>".date("Y", strtotime("-{$x} year"))."</option>";   
						}
}
?>
      </select>         
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">选择分类</div>
	  <select name="type" onChange="form2.submit()">
			<?php 
					if(intval($_type)===0){
						echo "<option value='0' selected='selected'>按一级分类</option>";
					}else{
						echo "<option value='0'>按一级分类</option>";
					}
					if(intval($_type)===1){
						echo "<option value='1' selected='selected'>按二级分类</option>";
					}else{
						echo "<option value='1'>按二级分类</option>";
					}
			?>
      </select>          
        </div>
        <div style="padding:15px 8px;">
		<input type="submit" value=" 统计 " class="hui-button hui-button-large hui-primary" >
        </div>
<?php }?>
</form>  

<br>
<!--startprint-->
<table style="width:95%" cellpadding="0" cellspacing="0" border="0" bgcolor="white" align="center">
<tr >
  <td align=center colspan="14" height=30><span style="font-family: 宋体; font-size: 20.0px; font-weight: bold;">
<?php echo $_year;?>年度收支统计表（按<?php if($_type==0){echo "一级分类";}if($_type==1){echo "二级分类";}?>）</span></td>
</tr>
<tr >
  <td align=right colspan="14" height=25>制表日期：<?php echo date("Y-m-d");?></td>
</tr>
<tr >
  <td align=center colspan="14" height=1 bgcolor="green"></td>
</tr>
<tr >
  <td align=center height=25>收支分类</td>
  <td align=right>1月</td>
  <td align=right>2月</td>
  <td align=right>3月</td>
  <td align=right>4月</td>
  <td align=right>5月</td>
  <td align=right>6月</td>
  <td align=right>7月</td>
  <td align=right>8月</td>
  <td align=right>9月</td>
  <td align=right>10月</td>
  <td align=right>11月</td>
  <td align=right>12月</td>
  <td align=right>全年</td>
</tr>
<tr>
  <td align=center colspan="14" height=1 bgcolor="green"></td>
</tr>
<tr>
  <td align=center colspan="14" height=20>收入</td>
</tr>


			<?php
			foreach ($classes as $k => $v) {  
			?>
<tr> 
<td align=center height=25>
<?php if($_type==0){?><a href="../money_list.php?action=search&type=0&id_bigclass=<?php echo $v['id'];?>&time0=<?php echo $_year;?>-01-01&time1=<?php echo $_year;?>-12-31" target="_blank"><?php echo $v['bigclass'];?></a><?php }?>
<?php if($_type==1){?><a href="../money_list.php?action=search&type=0&id_smallclass=<?php echo $v['id'];?>&time0=<?php echo $_year;?>-01-01&time1=<?php echo $_year;?>-12-31" target="_blank"><?php echo $v['smallclass'];?></a><?php }?>
</td>

<?php 
for ($x=1; $x<=12; $x++) {
$_year1=$_year."-".$x."-01";
$_year2=getthemonth($_year1);
$_year1=$_year1." 00:00:00";
$_year2 =$_year2." 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 

?>
<td align="right">
<?php if($_type==0){echo total_b($v['id'],$_year1,$_year2);}if($_type==1){echo total_s($v['id'],$_year1,$_year2);} ?>
</td>
<?php 
}

$_year1=$_year."-01-01 00:00:00";
$_year2 =$_year."-12-31 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
	<td align="right">
<?php if($_type==0){echo total_b($v['id'],$_year1,$_year2);}if($_type==1){echo total_s($v['id'],$_year1,$_year2);} ?>
</td>
</tr>
			<?php                
			}
			?>

<tr>
<td align=center height=25>收入小计</td>
<?php 
for ($x=1; $x<=12; $x++) {
$_year1=$_year."-".$x."-01";
$_year2=getthemonth($_year1);
$_year1=$_year1." 00:00:00";
$_year2 =$_year2." 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
<td align="right">
<?php echo total_a("0",$_year1,$_year2);?>
</td>
<?php 
}

$_year1=$_year."-01-01 00:00:00";
$_year2 =$_year."-12-31 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
	<td align="right">
<?php echo total_a("0",$_year1,$_year2); ?>
</td>
</tr>

<tr >
  <td align=center colspan="14" height=1 bgcolor="green"></td>
</tr>
<tr >
  <td align=center colspan="14" height=20>支出</td>
</tr>


			<?php
			foreach ($classes1 as $k => $v) {  
			?>
<tr> 
<td align=center height=25>
<?php if($_type==0){?><a href="../money_list.php?action=search&type=1&id_bigclass=<?php echo $v['id'];?>&time0=<?php echo $_year;?>-01-01&time1=<?php echo $_year;?>-12-31" target="_blank"><?php echo $v['bigclass'];?></a><?php }?>
<?php if($_type==1){?><a href="../money_list.php?action=search&type=1&id_smallclass=<?php echo $v['id'];?>&time0=<?php echo $_year;?>-01-01&time1=<?php echo $_year;?>-12-31" target="_blank"><?php echo $v['smallclass'];?></a><?php }?>
</td>

<?php 
for ($x=1; $x<=12; $x++) {
$_year1=$_year."-".$x."-01";
$_year2=getthemonth($_year1);
$_year1=$_year1." 00:00:00";
$_year2 =$_year2." 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 

?>
<td align="right">
<?php if($_type==0){echo total_b($v['id'],$_year1,$_year2);}if($_type==1){echo total_s($v['id'],$_year1,$_year2);} ?>
</td>
<?php 
}

$_year1=$_year."-01-01 00:00:00";
$_year2 =$_year."-12-31 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
	<td align="right">
<?php if($_type==0){echo total_b($v['id'],$_year1,$_year2);}if($_type==1){echo total_s($v['id'],$_year1,$_year2);} ?>
</td>
</tr>
			<?php                
			}
			?>
<tr >
  <td align=center height=25>支出小计</td>
<?php 
for ($x=1; $x<=12; $x++) {
$_year1=$_year."-".$x."-01";
$_year2=getthemonth($_year1);
$_year1=$_year1." 00:00:00";
$_year2 =$_year2." 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
<td align="right">
<font color=#ff0000><?php echo total_a("1",$_year1,$_year2);?></font>
</td>
<?php 
}

$_year1=$_year."-01-01 00:00:00";
$_year2 =$_year."-12-31 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
	<td align="right">
<font color="#ff0000"><?php echo total_a("1",$_year1,$_year2); ?></font>
</td>

</tr>
<tr >
  <td align=center colspan="14" height=1 bgcolor="green"></td>
</tr>

<tr >
  <td align=center height=25><strong>合计</strong></td>

<?php 
for ($x=1; $x<=12; $x++) {
$_year1=$_year."-".$x."-01";
$_year2=getthemonth($_year1);
$_year1=$_year1." 00:00:00";
$_year2 =$_year2." 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
<td align="right">
<font color=blue><?php echo total_a("0",$_year1,$_year2)-total_a("1",$_year1,$_year2);?></font>
</td>
<?php 
}

$_year1=$_year."-01-01 00:00:00";
$_year2 =$_year."-12-31 23:59:59"; 
$_year1 =strtotime($_year1);
$_year2 =strtotime($_year2); 
?>
	<td align="right">
<font color="blue"><b><?php echo total_a("0",$_year1,$_year2)-total_a("1",$_year1,$_year2); ?></b></font>
</td>

</tr>
</table>

<!--endprint-->

</body>
</html>