<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");
if (strpos($_SESSION['eptime_flag'], 'yearrfig') === false) {LYG::ShowMsg('您没有权限！');} 

if(empty($_GET['year'])){$_year=date("Y");}else{$_year=$_GET['year'];}

if(empty($_GET['yuangong'])){$_yuangong="0";}else{$_yuangong=$_GET['yuangong'];}

function total_y($type,$yuangong,$time){
	global $con;
$time1=$time."-01";
$time2=getthemonth($time1);
$time1=$time1." 00:00:00";
$time2 =$time2." 23:59:59"; 
$time1 =strtotime($time1);
$time2 =strtotime($time2); 
   if($yuangong==0){
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}
}
  else{
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and yuangong={$yuangong} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and yuangong={$yuangong} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}
}
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}


$classes = $con->select("select * from #__yuangong ");
?>
<?php if(empty($_GET['list'])){?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=uft-8">
		<title></title>
<?php }?>
		<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>

		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '<?php echo $_year;?>年度每月收支表(<?php if ($_yuangong==0){echo "全部";}else{echo c_classname("yuangong",$_yuangong);}?>)'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    '1月',
                    '2月',
                    '3月',
                    '4月',
                    '5月',
                    '6月',
                    '7月',
                    '8月',
                    '9月',
                    '10月',
                    '11月',
                    '12月'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'RMB'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f} ￥</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: '收入',
                data: [

<?php
for ($x=1; $x<=12; $x++){ 
	?>
<?php echo total_y("0",$_yuangong,$_year."-".$x);?>, 
<?php } ?>

				]
    
            },  {
                name: '支出',
                data: [

<?php
for ($x=1; $x<=12; $x++){ 
	?>
<?php echo total_y("1",$_yuangong,$_year."-".$x);?>, 
<?php } ?>

				]
    
            }]
        });
    });
    

		</script>
<?php if(empty($_GET['list'])){?>

	</head>
	<body>
<?php }?>
<form name="form2">
<?php if(empty($_GET['list'])){?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr> 
    <td width="50" height="30">&nbsp;</td>
	<td width="*" align="right">&nbsp;选择年份：
	  <select name="year" onChange="form2.submit()">
<?php 
for ($x=0; $x<=10; $x++) {
						if(intval($_year)===intval(date("Y", strtotime("-{$x} year")))){
							echo "<option value='".date("Y", strtotime("-{$x} year"))."' selected='selected'>".date("Y", strtotime("-{$x} year"))."</option>";
						}else{
							echo "<option value='".date("Y", strtotime("-{$x} year"))."'>".date("Y", strtotime("-{$x} year"))."</option>";   
						}
}
?>
      </select>


	  <select name="yuangong" onChange="form2.submit()">
        <option value="0">全部</option>
			<?php
			foreach ($classes as $k => $v) {  
						if(intval($_yuangong)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>"; 
						}               
			}
			?>
      </select>  
&nbsp;<input type="submit" value=" 统计 " class="button">
	</td>
  </tr>
  </table>

<?php } else{?>
<input type='hidden' name='list' value="year" />
        <div class="hui-form-items">
        	<div class="hui-form-items-title">选择年份</div>
	  <select name="year" onChange="form2.submit()">
<?php 
for ($x=0; $x<=10; $x++) {
						if(intval($_year)===intval(date("Y", strtotime("-{$x} year")))){
							echo "<option value='".date("Y", strtotime("-{$x} year"))."' selected='selected'>".date("Y", strtotime("-{$x} year"))."</option>";
						}else{
							echo "<option value='".date("Y", strtotime("-{$x} year"))."'>".date("Y", strtotime("-{$x} year"))."</option>";   
						}
}
?>
      </select>			
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?></div>
	  <select name="yuangong" onChange="form2.submit()">
        <option value="0">全部</option>
			<?php
			foreach ($classes as $k => $v) {  
						if(intval($_yuangong)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>"; 
						}               
			}
			?>
      </select> 			
        </div>
        <div style="padding:15px 8px;">
		<input type="submit" value=" 统计 " class="hui-button hui-button-large hui-primary" >
        </div>
<?php }?>
</form>  


<script src="../js/highcharts.js"></script>
<script src="../js/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php if(empty($_GET['list'])){?>
	</body>
</html>
<?php }?>