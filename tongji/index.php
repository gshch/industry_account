<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");

if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$data_id = intval($_REQUEST['id']);
if($_REQUEST['time1']){$_year1 =$_REQUEST['time1'];}else{$_year1 =strtotime(date("Y-m-d 00:00:00",strtotime("last Sunday +1 day")));}
if($_REQUEST['tt']==1){$_title ="本周";}elseif($_REQUEST['tt']==2){$_title ="本月";}elseif($_REQUEST['tt']==3){$_title ="本年";}elseif($_REQUEST['tt']==4){$_title ="时间段内";}else{$_title ="本周";}
function _total1($type,$time1){
	global $con;
		//统计收

    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_bigclass={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where id_bigclass={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} ";}

		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}
if($data_id==1){$sql = "select * from #__money_bigclass where type=0  order by  id desc";}
elseif($data_id==2){$sql = "select * from #__money_bigclass where type=1  order by  id desc";}
$data = $con->select($sql,$_v);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>

		<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
				enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: '<?php echo $_title;?><?php if($data_id==1){echo 收入;}elseif($data_id==2){echo 支出;}?>构成图表'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '占比',
            data: [
<?php foreach($data as $k=>$v){?>
['<?php echo $v["bigclass"];?>', <?php echo _total1($v['id'],$_year1);?>],
<?php }?>
            ]
        }]
    });
});
		</script>
	</head>
	<body>
<script src="../js/highcharts.js"></script>

<script src="../js/exporting.js"></script>
<div id="container" style="height: 400px"></div>
	</body>
</html>
