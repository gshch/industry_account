<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");
if (strpos($_SESSION['eptime_flag'], 'totalfig') === false) {LYG::ShowMsg('您没有权限！');} 


function total_a($type,$yuangong,$time1,$time2){
	global $con;
		//统计收
$time1=$time1." 00:00:00";
$time2 =$time2." 23:59:59"; 
$time1 =strtotime($time1);
$time2 =strtotime($time2); 
if ($yuangong==0){
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}


}
else{
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where type={$type} and yuangong={$yuangong} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and id_login={$_SESSION['eptime_id']}  ";}
	else{$sql = "select SUM(price) as sl from #__money where type={$type} and yuangong={$yuangong} and UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2}  ";}


	}
		
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}


if(empty($_GET['yuangong'])){$_yuangong="0";}else{$_yuangong=$_GET['yuangong'];}
if(empty($_GET['time0'])){$_time0=date('Y-m-01', strtotime(date("Y-m-d")));}else{$_time0=$_GET['time0'];}
if(empty($_GET['time1'])){$_time1=getthemonth(date('Y-m-d'));}else{$_time1=$_GET['time1'];}

if($_type==0){
$classes = $con->select("select * from #__money_bigclass where type=0 and isok=0");
$classes1 = $con->select("select * from #__money_bigclass where type=1 and isok=0");
}
if($_type==1){
$classes = $con->select("select * from #__money_smallclass where id_bigclass in (select id from #__money_bigclass where type=0 and isok=0)");
$classes1 = $con->select("select * from #__money_smallclass where id_bigclass in (select id from #__money_bigclass where type=1 and isok=0)");
}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
<link href="../images/admin.css" rel="stylesheet" type="text/css">
<script language=javascript src="../js/calendar1.js"></script>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
				enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: '收支汇总表(<?php if ($_yuangong==0){echo "全部";}else{echo c_classname("yuangong",$_yuangong);}?>)'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '占比',
            data: [
                ['收入',<?php echo total_a("0",$_yuangong,$_time0,$_time1);?>],
                ['支出',<?php echo total_a("1",$_yuangong,$_time0,$_time1);?>],
            ]
        }]
    });
});
		</script>
	</head>
	<body>

<form name="form2">

<?php if(empty($_GET['list'])){?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr> 
    <td height="30">&nbsp;</td>
	<td align="right">
	  <select name="yuangong" onChange="form2.submit()">
					<option value='0'>所有<?php echo $webconfig['system_yuangong'];?></option><?php
					foreach(c_classinfo("yuangong") as $k=>$v){
						if(intval($_yuangong)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
      </select>  
&nbsp;日期：
				<input type="text" name="time0" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time0;?>">
	  至
				<input type="text" name="time1" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time1;?>">
	  <input type="submit" value=" 统计 " class="button" onClick="return check()">&nbsp;
	</td>
  </tr>
  </table>
<?php } else{ ?>
		<input type='hidden' name='list' value="total" />
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?></div>

	  <select name="yuangong" onChange="form2.submit()">
					<option value='0'>所有<?php echo $webconfig['system_yuangong'];?></option><?php
					foreach(c_classinfo("yuangong") as $k=>$v){
						if(intval($_yuangong)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
      </select> 
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">开始日期</div>
			<input type="text" name="time0" class="hui-input hui-input-clear" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time0;?>">
           
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">结束日期</div>
			<input type="text" name="time1" class="hui-input hui-input-clear" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time1;?>">
           
        </div>

        <div style="padding:15px 8px;">
		<input type="submit" value=" 统计 " class="hui-button hui-button-large hui-primary" >
        </div>

<?php }?>
</form>  

  <table cellpadding="0" cellspacing="0" width="95%" align="center" >
    <tr height="30">
      <td>&nbsp;</td>
	  <td align="right">盈亏：<?php echo total_a("0",$_yuangong,$_time0,$_time1)-total_a("1",$_yuangong,$_time0,$_time1);?><b></b> 元, 收入合计：<b><?php echo total_a("0",$_yuangong,$_time0,$_time1);?></b> 元, 支出合计：<b><?php echo total_a("1",$_yuangong,$_time0,$_time1);?></b> 元&nbsp;</td>
    </tr>
  </table>
<script src="../js/highcharts.js"></script>
<script src="../js/highcharts-3d.js"></script>
<script src="../js/exporting.js"></script>
  <table cellpadding="0" cellspacing="0" width="95%" bgcolor="#EBEBEB" align="center" >
    <tr height="30" align="center">
      <td align="center">
<div id="container" style="height: 100%"></div>
    </td>
    </tr>
  </table>
	</body>
</html>