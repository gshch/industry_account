<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");
//if (strpos($_SESSION['eptime_flag'], 'kehufig') === false) {LYG::ShowMsg('您没有权限！');} 

function total_x($type,$wanglai,$id,$time1,$time2){
	global $con;
$time1=$time1." 00:00:00";
$time2 =$time2." 23:59:59"; 
$time1 =strtotime($time1);
$time2 =strtotime($time2); 
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and type={$type} and {$wanglai}= {$id} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where UNIX_TIMESTAMP(selldate) >= {$time1} and UNIX_TIMESTAMP(selldate) <= {$time2} and type={$type} and {$wanglai}= {$id}  ";}
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}
if(empty($_GET['time0'])){$_time0=date('Y-m-01', strtotime(date("Y-m-d")));}else{$_time0=$_GET['time0'];}
if(empty($_GET['time1'])){$_time1=getthemonth(date('Y-m-d'));}else{$_time1=$_GET['time1'];}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
<script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>

		<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '<?php echo $_time0;?>至<?php echo $_time1;?> <?php echo $webconfig['system_wanglai'];?>收支表'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
				?>
'<?php echo $v['name'];?>',
				<?php
				}
				?>
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'RMB'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f} ￥</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: '收入',
                data: [
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
				?>
<?php echo total_x("0","wanglai",$v['id'],$_time0,$_time1);?>,
				<?php
				}
				?>

				]
    
            },  {
                name: '支出',
                data: [

				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
				?>
<?php echo total_x("1","wanglai",$v['id'],$_time0,$_time1);?>,
				<?php
				}
				?>

				]
    
            }]
        });
    });
    

		</script>
	</head>
	<body>
<form name="form2">

<?php if(empty($_GET['list'])){?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr> 
    <td height="30">&nbsp;</td>
	<td align="right">
&nbsp;日期：
				<input type="text" name="time0" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time0;?>">
	  至
				<input type="text" name="time1" class="text" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time1;?>">
	  <input type="submit" value=" 统计 " class="button" onClick="return check()">&nbsp;
	</td>
  </tr>
  </table>
<?php } else{ ?>
		<input type='hidden' name='list' value="wanglai" />

        <div class="hui-form-items">
        	<div class="hui-form-items-title">开始日期</div>
			<input type="text" name="time0" class="hui-input hui-input-clear" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time0;?>">
           
        </div>

        <div class="hui-form-items">
        	<div class="hui-form-items-title">结束日期</div>
			<input type="text" name="time1" class="hui-input hui-input-clear" placeholder="0000-00-00" onclick="WdatePicker();" value="<?php echo $_time1;?>">
           
        </div>

        <div style="padding:15px 8px;">
		<input type="submit" value=" 统计 " class="hui-button hui-button-large hui-primary" >
        </div>

<?php }?>
</form>  




<script src="../js/highcharts.js"></script>
<script src="../js/exporting.js"></script>


  <table cellpadding="0" cellspacing="0" width="95%" bgcolor="#EBEBEB" align="center" >
    <tr height="30" align="center">
      <td align="center">
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </td>
    </tr>
  </table>
	</body>
</html>
