<?php
require_once(dirname(dirname(__FILE__)).'/include/common.php');
$webconfig = lyg::readArr("web");
if (strpos($_SESSION['eptime_flag'], 'shouzhifig') === false) {LYG::ShowMsg('您没有权限！');} 



function total_b($bigclass,$type,$yuangong){
	global $con;
		//统计收
if ($yuangong==0){
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_bigclass={$bigclass} and type = {$type} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where id_bigclass={$bigclass} and type = {$type} ";}
	}
else{
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_bigclass={$bigclass} and type = {$type} and yuangong = {$yuangong}  and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where id_bigclass={$bigclass} and type = {$type} and yuangong = {$yuangong}  ";}
	}
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}

function total_s($bigclass,$type,$yuangong){
	global $con;
		//统计收
if ($yuangong==0){
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_smallclass={$bigclass} and type = {$type} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where id_smallclass={$bigclass} and type = {$type} ";}	
	
	}
else{
    if($_SESSION['eptime_adminPower']==2){$sql = "select SUM(price) as sl from #__money where id_smallclass={$bigclass} and type = {$type} and yuangong = {$yuangong} and id_login={$_SESSION['eptime_id']} ";}
	else{$sql = "select SUM(price) as sl from #__money where id_smallclass={$bigclass} and type = {$type} and yuangong = {$yuangong}  ";}	
	
	}
		$sl = $con->find($sql);
		if(empty($sl['sl'])){ $sl['sl'] = 0;}
		return round($sl['sl'],2);
}





if(empty($_GET['type'])){$_type="0";}else{$_type=$_GET['type'];}
if(empty($_GET['yuangong'])){$_yuangong="0";}else{$_yuangong=$_GET['yuangong'];}
if(empty($_GET['bigclass'])){$_bigclass="0";}else{$_bigclass=$_GET['bigclass'];}


$bigclassinfo = $con->select("select * from #__money_bigclass where type = {$_type}");
$smallclassinfo = $con->select("select * from #__money_smallclass where id_bigclass = {$_bigclass}");


?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
<link href="../images/admin.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
				enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: '<?php echo $c_type["{$_type}"];?>构成图表(<?php if ($_yuangong==0){echo "全部";}else{echo c_classname("yuangong",$_yuangong);}?>)'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '占比',
            data: [
<?php if ($_bigclass==0)
{
foreach($bigclassinfo as $k=>$v){ ?>
['<?php echo $v['bigclass'];?>',<?php echo total_b($v['id'],$_type,$_yuangong);?>],
<?php
				}
}else{
foreach($smallclassinfo as $k=>$v){ ?>
['<?php echo $v['smallclass'];?>',<?php echo total_s($v['id'],$_type,$_yuangong);?>],
<?php }
}
	?>

            ]
        }]
    });
});
		</script>
	</head>
	<body>
<form name="form2">
<?php if(empty($_GET['list'])){?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr bgcolor="#EBEBEB"> 
    <td width="50" height="30">&nbsp;</td>
	<td width="*" align="right">
	  <select name="type" onChange="form2.submit()">
			<?php 
			foreach($c_type as $k=>$v){
					if(intval($_type)===intval($k)){
						echo "<option value='{$k}' selected='selected'>{$v}</option>";
					}else{
						echo "<option value='{$k}'>{$v}</option>";
					}
			}?>
      </select>

	  <select name="yuangong" onChange="form2.submit()">
					<option value='0'>所有<?php echo $webconfig['system_yuangong'];?></option><?php
					foreach(c_classinfo("yuangong") as $k=>$v){
						if(intval($_yuangong)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
      </select>    

	  <select name="bigclass" onChange="form2.submit()">
        <option value="0">按一级分类统计</option>
                    <?php
					foreach($bigclassinfo as $k=>$v){
						if(intval($_bigclass)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>对[{$v['bigclass']}]统计</option>";
						}else{
							echo "<option value='{$v['id']}'>对[{$v['bigclass']}]统计</option>";    
						}
					}
					?>
      </select>
	  <input type="submit" value=" 统计 " class="button" onClick="return check()">&nbsp; 
	</td>
  </tr>
  </table>
<?php } else{?>
		<input type='hidden' name='list' value="shouzhi" />
        <div class="hui-form-items">
        	<div class="hui-form-items-title">类型</div>
 	  <select name="type" onChange="form2.submit()">
			<?php 
			foreach($c_type as $k=>$v){
					if(intval($_type)===intval($k)){
						echo "<option value='{$k}' selected='selected'>{$v}</option>";
					}else{
						echo "<option value='{$k}'>{$v}</option>";
					}
			}?>
      </select>          
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title"><?php echo $webconfig['system_yuangong'];?></div>
 	  <select name="yuangong" onChange="form2.submit()">
					<option value='0'>所有<?php echo $webconfig['system_yuangong'];?></option><?php
					foreach(c_classinfo("yuangong") as $k=>$v){
						if(intval($_yuangong)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
						}else{
							echo "<option value='{$v['id']}'>{$v['name']}</option>";    
						}
					}
					?>
      </select>           
        </div>
        <div class="hui-form-items">
        	<div class="hui-form-items-title">结束日期</div>
	  <select name="bigclass" onChange="form2.submit()">
        <option value="0">按一级分类统计</option>
                    <?php
					foreach($bigclassinfo as $k=>$v){
						if(intval($_bigclass)===intval($v['id'])){
							echo "<option value='{$v['id']}' selected='selected'>对[{$v['bigclass']}]统计</option>";
						}else{
							echo "<option value='{$v['id']}'>对[{$v['bigclass']}]统计</option>";    
						}
					}
					?>
      </select>           
        </div>
        <div style="padding:15px 8px;">
		<input type="submit" value=" 统计 " class="hui-button hui-button-large hui-primary" >
        </div>
<?php }?>
</form>  

<script src="../js/highcharts.js"></script>
<script src="../js/exporting.js"></script>
<div id="container" style="height: 400px"></div>
	</body>
</html>
