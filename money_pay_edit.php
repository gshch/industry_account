<?php

require_once(dirname(__FILE__).'/include/common.php');
if (strpos($_SESSION['eptime_flag'], 'paymfig') === false) {LYG::ShowMsg('您没有权限！');} 
$webconfig = lyg::readArr("web");
$check_enable = intval($webconfig['system_check'])===1?true:false;


if(empty($_REQUEST['id']) || intval($_REQUEST['id'])<1){lyg::showmsg('参数错误');}
$dataid = intval($_REQUEST['id']);
$info=  $con->find("select * from #__money_pay where id=$dataid");
if(empty($info)){lyg::showmsg('参数错误');}

if(!empty($_POST)){
	
	extract($_POST);

$moneyID=intval($moneyID);
$price=trim($price);
$selldate=trim($selldate);
$Lastdate=trim($Lastdate);
	$wanglai = intval($wanglai);
	$yuangong = intval($yuangong);
	$xiangmu = intval($xiangmu);
	$beizhu= trim($beizhu);
	$img = trim($img);
$LastLogin = $_SESSION['eptime_username'];
$LastTime = date("Y-m-d H:i:s",time());

	if(empty($price) || trim($price)==''){
		LYG::ShowMsg('金额不能为空');
	}

    $data = array(
        'price'		=>$price,
        'selldate'	=>$selldate,
		'Lastdate'	=>$Lastdate,
		'wanglai'	=>$wanglai,
		'yuangong'	=>$yuangong,
		'xiangmu'	=>$xiangmu,
		'beizhu'	=>$beizhu,
		'img'		=>$img,
        'LastLogin'	=>$LastLogin,
		'LastTime'	=>$LastTime,
    );
	$result	= $con->savemin("money_pay",$data,"id={$dataid}");
	if($result!==false){
LYG::writeLog("[".$_SESSION['eptime_username']."]修改单号[".$moneyID."]");
		LYG::ShowMsg('修改成功','money_pay_list.php');
	}else{
		LYG::ShowMsg('流水没有更改');
	}
	
	die();
	
}


	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改流水</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
label{margin-right:10px;}
</style>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
		<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
		<script src="kindeditor/kindeditor-all.js" charset="UTF-8"></script>
		<script src="kindeditor/lang/zh-CN.js" charset="UTF-8"></script>
		<script>
			KindEditor.ready(function(K) {
				var editor = K.editor({
					allowFileManager : true
				});
				K('#image3').click(function() {
					editor.loadPlugin('image', function() {
						editor.plugin.imageDialog({
							showRemote : false,
							imageUrl : K('#url3').val(),
							clickFn : function(url, title, width, height, border, align) {
								K('#url3').val(url);
								editor.hideDialog();
							}
						});
					});
				});
			});
		</script>
</head>


<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='<?php if($webconfig['modi_sms_open']){echo "yz_money_pay_edit.php";}?>' method='post'>
<input type='hidden' name='id' value="<?php echo $dataid;?>" />
<table cellpadding="3" cellspacing="0" class="table-add">
		<tr>
			<td align="right" height='36'>单号：</td>
			<td>
			<?php echo $info['moneyID'];?>
			</td>
		</tr>
		<input type='hidden' name='moneyID' value="<?php echo $info['moneyID'];?>" />
		<tr>
			<td align="right" height='36'>类型：</td>
			<td>
			[<?php echo $c_type1["{$info['type']}"];?>]<?php echo c_bigclassname($info['id_bigclass']);?>-><?php echo c_smallclassname($info['id_smallclass']);?>
			
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>金额：</td>
			<td>
			<input class='inp' type='number' name='price' placeholder="0.00" step="0.01" value="<?php echo $info['price'];?>"/>
			<span>数字，请保留小数点后两位</span>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>时间：</td>
			<td>
			
			<input type='text' class='inp' name='selldate' value='<?php echo substr($info['selldate'],0,10) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
			<span>点击选择</span>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'>收还时间：</td>
			<td>
			
			<input type='text' class='inp' name='Lastdate' value='<?php echo substr($info['Lastdate'],0,10) ?>' placeholder="0000-00-00" onclick="WdatePicker();" />
			<span>点击选择</span>
			</td>
		</tr>



		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_wanglai'];?>：</td>
			<td align="left" width='*'>
			<select name="wanglai" class="select">
				<?php
				foreach(c_classinfo("wanglai") as $k=>$v){
        		if(intval($info['wanglai'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_yuangong'];?>：</td>
			<td align="left" width='*'>
			<select name="yuangong" class="select">
				<?php
				foreach(c_classinfo("yuangong") as $k=>$v){
        		if(intval($info['yuangong'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" height='36'><?php echo $webconfig['system_xiangmu'];?>：</td>
			<td align="left" width='*'>
			<select name="xiangmu" class="select">
				<?php
				foreach(c_classinfo("xiangmu") as $k=>$v){
        		if(intval($info['xiangmu'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['name']}</option>";
        		}else{
        			echo "<option value='{$v['id']}'>{$v['name']}</option>";}
				}
				?>
				</select>
			</td>
		</tr>

	<tr>
		<td align="right" height='36'>备注：</td>
		<td align="left"><textarea name="beizhu" cols="60" rows="3" onKeyup="if(this.value.length>255){this.value=this.value.slice(0,255)}"><?php echo $info['beizhu'];?></textarea></td>
	</tr>
	<tr>
		<td align="right" height='36'>附件：</td>
		<td align="left"><input type='text' class='inp' name='img' placeholder='' id="url3" value="<?php echo $info['img'];?>"/> <input type="button" value="上传附件" id="open" class="layui-btn layui-btn-danger">
<div class="alert alert-info" style="width:92%" id="demo2"></div>
            <div class="item col-xs-12"><div class="f-fl item-ifo">
<link rel="stylesheet" href="layui/css/layui.css" media="all">
<script type="text/javascript" src="layui/layui.js"></script>
<script type="text/javascript">
layui.use('upload', function (){
  var upload = layui.upload,
    $ = layui.jquery;
var uploadInst = upload.render({
  elem : '#open',
  accept : 'images',//指定允许上传时校验的文件类型，可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
  multiple : 'true',
  url : 'up.php',
  data: {},
  before: function(obj){
      obj.preview(function(index, file, result){
        $('#demo2').append('<img src="'+ result +'" alt="'+ file.name +'" style="width:33%;">')
      });
    },
  done : function(res){
if(res['code']==1){
//alert(res['src']);
var imgpath=res['src'];
//alert(imgpath);
layer.msg('图片上传成功');
imgpath = imgpath + ','+$("#url3").val();
$("#url3").val(imgpath);
}else{
layer.msg('图片上传失败');
}
  },
  error : function(){   
    //请求异常
  }
});
});

</script>
            </div></div></td>
	</tr>

	<tr>
    	<td align="right" height='50'>　</td>
        <td align="left">
        	<input class='sub' type='submit' value='修改'/>　<input class='reset' type='reset' value='重置'></td>
    </tr>


    
</table>
</form>

</body>
</html>