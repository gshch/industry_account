<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 

if(!empty($_POST)){
	//参数校验
	extract($_POST);

	$id_bigclass = intval($id_bigclass);
	
	if(empty($smallclass) || trim($smallclass)==''){
		LYG::ShowMsg('二级分类名不能为空');
	}
	$smallclass= trim($smallclass);

    $isok = c_bigclassisok($id_bigclass);

	$isok = intval($isok);
	$ex = $con->rowscount("select count(*) from #__money_smallclass where id_bigclass=? and smallclass=?",array(
		$id_bigclass,$smallclass
	));
	if($ex>0){
		lyg::showmsg("同名二级分类已存在");
	}
	
	$data = array(
		'smallclass'		=>$smallclass,
		'id_bigclass'	=>$id_bigclass,
		'isok'		=>$isok
	);
	
	$aok = $con->add("money_smallclass",$data);

	if($aok){
		LYG::writeLog("[".$_SESSION['eptime_username']."]添加二级分类[".$smallclass."]");
		LYG::ShowMsg('添加成功','small_list.php');
	}else{
		LYG::ShowMsg('添加失败，请重试');
	}
	
	die();
}

$classes = $con->select("select * from #__money_bigclass");
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加二级分类</title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

</head>

<body class="content">

<h5 class='back' onclick='history.go(-1);'><span>返回</span></h5>

<form action='' method='post'>
	<table cellpadding="3" cellspacing="0" class="table-add">
		<tr>
			<td align="right" height='36'>一级分类：</td>
			<td align="left" width='*'>
			<select name="id_bigclass" class="select bai" onchange="search(this);">
			<?php
			foreach ($classes as $k => $v) {
					echo "<option value='{$v['id']}'>[{$c_type[$v['type']]}]{$v['bigclass']}</option>";                    
			}
			?></select>
			</td>
		</tr>
		<tr>
			<td align="right" width='100' height='36'>二级分类名称：</td>
			<td align="left" width='*'>
				<input type='text' class='inp' name='smallclass' placeholder=''/>
			</td>
		</tr>
		<tr>
			<td align="right" height='50'>　</td>
			<td align="left"><input class='sub' type='submit' value='添加'/></td>
		</tr>

	</table>
</form>

</body>
</html>