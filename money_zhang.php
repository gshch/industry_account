<?php

require_once(dirname(__FILE__).'/include/common.php');
if (strpos($_SESSION['eptime_flag'], 'zhangfig') === false) {LYG::ShowMsg('您没有权限！');} 
$_k = array();
$_v = array();

$_c = array();//分页条件
$_s = array();//搜索条件

if(!empty($_GET['type']) && intval($_GET['type'])>0){
    $_k[]="#__zhanghu.type=?";
    $_v[]=intval($_GET['type']);
    $_c[]="type=".intval($_GET['type']);
    $_s['type'] = intval($_GET['type']);
}

$_k = implode(' and ',$_k);
if($_k!=''){
    $_k = " where ".$_k;
}



$pagesize = 20;

//总记录数
$datacount=$con->RowsCount("select count(*) from #__zhanghu {$_k}",$_v);
//总页数
$totalpages=LYG::getTotalPage($datacount,$pagesize);
$page=1;
if(isset($_GET['p']) && intval($_GET['p'])>0){
	$page=intval($_GET['p']);
	$page=$page>$totalpages?$totalpages:$page;
	if($page+1<=1){$page=1;}
}
$start_id=($page-1)*$pagesize;
//查询数据
$sql = "select #__zhanghu.*,#__zhanghu_class.zhanghuclass from #__zhanghu left join #__zhanghu_class on #__zhanghu_class.id = #__zhanghu.type {$_k} order by  #__zhanghu.id desc limit $start_id,$pagesize";
$data = $con->select($sql,$_v);

//得到分页HTML
$fenye=LYG::getPageHtml($page,$datacount,$pagesize);

$classes = $con->select("select * from #__zhanghu_class");

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style/css/css.css" rel="stylesheet" type="text/css" />
<link href="style/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
$(function(){
    $("#type").change(function(){
        var id = parseInt($(this).val());

            $.ajax({
                type:'get',
                url:'json.php?act=getbigclass&type='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有一级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].bigclass+"</option>";
                    }
                    $("#id_bigclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });

    });

    $("#id_bigclass").change(function(){
        var id = parseInt($(this).val());
        if(id==''){
            $("#id_smallclass").html("<option value=''>所有二级分类</option>");
        }else{
            $.ajax({
                type:'get',
                url:'json.php?act=getsmallclass&id_bigclass='+id,
                dataType:'json',
                success:function(data){
                    var html="<option value=''>所有二级分类</option>";
                    for(var i=0;i<data.length;i++){
                        html+="<option value='"+data[i].id+"'>"+data[i].smallclass+"</option>";
                    }
                    $("#id_smallclass").html(html);
                },
                complete:function(XMLHttpRequest,textStatus){},
                error:function(){}
            });
        }
    });

function setmethod(tag){
	$("#actionmethod").val(tag);
	if(tag=="output"){
		$("#searchform").attr("target","_blank");
	}else{
		$("#searchform").removeAttr("target");
	}
}
</script>
<script type="text/javascript">
function search(obj){
	document.searchform.submit();
}
$(function(){
	$("input.sort").blur(function(){
		
		var sort = parseInt($(this).val());
		if(isNaN(sort)){
			sort = 100;
		}
		if(sort == parseInt(this.defaultValue)){
			return;
		}
		
		var id = $(this).parent().parent().attr("data-id");
		
		$.post("json.php",{"act":"zhanghusort","id":id,"sort":sort},function(e){
			location.reload();
		},"json");
	});
});
</script>
</head>

<body class="content">


<div class="searchform">
    <form method="get" name="searchform">
	<table>
		<tr>
			<td width="80" align="left">账户类型</td>
			<td width="200">
			<select name="type" class="select bai" onchange="search(this);">
				<option value='0'>不限</option>
			<?php
			foreach ($classes as $k => $v) {
				if(array_key_exists('type', $_s) && intval($_s['type'])===intval($v['id'])){
					echo "<option value='{$v['id']}' selected='selected'>{$v['zhanghuclass']}</option>";
				}else{
					echo "<option value='{$v['id']}'>{$v['zhanghuclass']}</option>";    
				}                    
			}
			?></select>
			</td>
			<td width="*"></td>
		</tr>
	</table>
    </form>
</div>

<div class="list-menu">
	<ul>
		<li>只能选择一个账户进行操作</li>
	</ul>
</div>


<table cellpadding="3" cellspacing="0">
	<thead>
    	<tr>
			<th width="100">ID</th>
            <th>[类型]账户名称</th>
            <th>当前余额</th>
            <th>操作</th>
            <th>-</th>
        </tr>
    </thead>
    <tbody>
	<?php $amount=0; foreach($data as $k=>$v){?>
    	<tr class='list' data-id="<?php echo $v['id'];?>">
			<td align="center"><?php echo $v['id'];?></td>
        	<td align="center">[<?php echo $v['zhanghuclass'];?>]<?php echo $v['name'];?></td>
        	<td align="center"><?php echo round($v['amount'],2);?></td>
<td align="center"><a href="zhuan.php?id=<?php echo $v['id'];?>">转账(互转)</a> 
			<td align="center">
			<a href="money_list.php?zhanghu=<?php echo $v['id'];?>" target="_blank">收支流水对账</a>&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="money_zhang_list.php?zhanghu=<?php echo $v['id'];?>" target="_blank">转账流水对账</a>&nbsp;&nbsp;|&nbsp;&nbsp;
		
        </tr>
	<?php $amount=$amount+$v['amount'];}?>



    	<tr>
		    
            <th>

            </th>
			<th></th>
            <th>合计:</th>
            <th><?php echo round($amount,2);?></th>
            <th></th>
        </tr>


    </tbody>

    <tfoot>
    	<tr>
        	<td colspan="7" style="padding-left:30px;">
			<?php echo $fenye ;?>
			</td>
        </tr>
    </tfoot>
</table>


</body>
</html>
