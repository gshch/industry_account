<?php

require_once(dirname(__FILE__).'/include/common.php');
if ($_SESSION['eptime_adminPower']<>0) {LYG::ShowMsg('您没有权限！');} 

if(empty($_GET['id']) || intval($_GET['id'])<1){
	LYG::ShowMsg('参数错误');
}
$id = intval($_GET['id']);


//判断账户下是否有
$ddcount = $con->rowscount("select count(*) from #__money where id_bigclass=$id ");
if($ddcount>0){
	LYG::ShowMsg('该账户有数据，暂不能删除');
}

$debug = lyg::readArr("debug");
if($debug['debug']){
	//至少保留一个账户
	$gsCount = $con->rowsCount('select count(*) from #__money_bigclass');
	if($gsCount < 2){
		LYG::ShowMsg('至少需要一个账户');
	}
}

$sql="delete from #__money_bigclass where id=$id limit 1";
$data =$con->Excute($sql);
if($data){
	lyg::jump('big_list.php?type=');
}else{
	LYG::ShowMsg('删除失败');
}	
